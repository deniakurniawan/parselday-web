$(function () {
    $("#payment_method").change();
    $("#check_create_acc").change();
});

$('#payment_method').change(function(){
  if ($('#payment_method').val()==='1'){
    $('.payment_method_info').hide();
    $('#payment_method_info_bt').show();
  }
  else if ($('#payment_method').val()==='2'){
    $('.payment_method_info').hide();
    $('#payment_method_info_cp').show();
  }
  else if ($('#payment_method').val()==='3'){
    $('.payment_method_info').hide();
    $('#payment_method_info_vt').show();
  }

});

$('#check_create_acc').change(function(){
	
  if ($('#check_create_acc').is( ":checked" ) == true){
    $('#username').attr('required','true');
    $('#password').attr('required','true');
    $('#confirm_password').attr('required','true');

    $('#create_account').show();
  }
  else {
    $('#username').val('');
    $('#password').val('');
    $('#confirm_password').val('');

    $('#username').removeAttr('required');
    $('#password').removeAttr('required');
    $('#confirm_password').removeAttr('required');

    $('#create_account').hide();
  }
});

$('#src_alt_addr').change(function(){
  $.ajax({
    url: config.base_url+"hello/getAltAddress",
    type: "POST",
    data: {
        source: $('#src_alt_addr').val(),
    },
    dataType: "JSON",
    cache: false,
    success: function(data) {
      $('#first_name').val(data['first_name']);
      $('#last_name').val(data['last_name']);
      $('#phone').val(data['phone']);
      $('#email').val(data['email']);
      $('#address').val(data['address']);
      $('#city').val(data['city']);
      $('#country').val(data['country']);
      $('#province').val(data['province']);
      $('#zip').val(data['zip']);
		}
  });
});

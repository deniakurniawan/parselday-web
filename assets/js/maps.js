var origin_map;
var origin_marker;
var destination_map;
var destination_marker;

function initialize_from() {
  var geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-6.2297465,106.829518);
  var mapOptions = {
    scrollwheel: false,
    disableDefaultUI: true,
    zoom: 12,
    center: latlng,
    mapTypeId: 'roadmap',
    streetViewControl: false,
    panControl: false,
    zoomControl: true,
  }
  origin_map = new google.maps.Map(document.getElementById('senderdata'), mapOptions);

  origin_marker = new google.maps.Marker({
    position: latlng,
    map: origin_map,
    draggable:true,
    animation: google.maps.Animation.DROP,
  });

  google.maps.event.addListener(origin_marker, "dragend", function (e) {
    geocoder.geocode({'latLng': e.latLng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
       // console.log(results);
        if (results.length > 0) {
          if (!results[0].geometry) {
            update_address('origin');
            return;
          }
          update_address('origin',results[0]);
        } else {
          update_address('origin');
        }
      } else {
        update_address('origin');
      }
    })
  });

  var autocomplete = new google.maps.places.Autocomplete((document.getElementById('origin_search')),{types:['geocode']});
  google.maps.event.addListener(autocomplete, "place_changed", function() {
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      update_address('origin');
      return;
    }
    origin_map.setCenter(place.geometry.location);
    origin_map.setZoom(17);  // Why 17? Because it looks good.
    origin_marker.setPosition(place.geometry.location);
    update_address('origin',place);
  });

  google.maps.event.addDomListener(document.getElementById('origin_search'), 'keydown', function(e) {
    if (e.keyCode == 13)
      e.preventDefault();
  });
}

function initialize_to() {
  var geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-6.2297465,106.829518);
  var mapOptions = {
    scrollwheel: false,
    disableDefaultUI: true,
    zoom: 12,
    center: latlng,
    mapTypeId: 'roadmap',
    streetViewControl: false,
    panControl: false,
    zoomControl: true,
  }
  destination_map = new google.maps.Map(document.getElementById('recipientdata'), mapOptions);

  destination_marker = new google.maps.Marker({
    position: latlng,
    map: destination_map,
    draggable:true,
    animation: google.maps.Animation.DROP,
  });

  google.maps.event.addListener(destination_marker, "dragend", function (e) {
    geocoder.geocode({'latLng': e.latLng}, function(results, status) {
     // console.log(results)
      if (status == google.maps.GeocoderStatus.OK) {
        if (results.length > 0) {
          if (!results[0].geometry) {
            update_address('destination');
            return;
          }
          update_address('destination',results[0]);
        } else {
          update_address('destination');
        }
      } else {
        update_address('destination');
      }
    })
  });

var autocomplete = new google.maps.places.Autocomplete((document.getElementById('destination_search')),{types:['geocode']});
google.maps.event.addListener(autocomplete, "place_changed", function() {
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      update_address('destination');
      return;
    }
    destination_map.setCenter(place.geometry.location);
    destination_map.setZoom(17);  // Why 17? Because it looks good.
    destination_marker.setPosition(place.geometry.location);
    update_address('destination',place);
  });
  google.maps.event.addDomListener(document.getElementById('destination_search'), 'keydown', function(e) {
    if (e.keyCode == 13)
      e.preventDefault();
  });
}

function update_address(id) {
  $('#'+id+'_lat').val("");
  $('#'+id+'_lng').val("");
  $('#'+id+'_address').val("");
  $('#'+id+'_village').val("");
  $('#'+id+'_sub_district').val("");
  $('#'+id+'_city').val("");
  $('#'+id+'_country').val("");
  $('#'+id+'_zip').val("");
  $('#'+id+'_province').val("");
  $('#distance').val(0);

  alert('No results found');
}

function update_address(id, place) {

    if (!place.address_components) update_address(id);
    var address_components = place.address_components;
    var street_number = "", address = "", village = "", sub_district = "", city = "", country = "", zip = "", province = "";
    for (var i = 0; i < address_components.length; i++) {
      if (address_components[i].types[0] == "street_number")
        street_number += address_components[i].short_name;
      if (address_components[i].types[0] == "route")
        address += address_components[i].short_name;
      if (address_components[i].types[0] == "country") //country
        country += address_components[i].long_name;
      if (address_components[i].types[0] == "administrative_area_level_1") //province
        province += address_components[i].long_name;
      if (address_components[i].types[0] == "administrative_area_level_2") 
        city += address_components[i].long_name;
      if (address_components[i].types[0] == "administrative_area_level_3") //kecamatan
        sub_district += address_components[i].long_name;
      if (address_components[i].types[0] == "administrative_area_level_4") // kelurahan
        village += address_components[i].long_name;
      //if (address_components[i].types[0] == "administrative_area_level_5")
      if (address_components[i].types[0] == "postal_code") //kodepos
        zip = address_components[i].short_name;

    };
    $('#'+id+'_lat').val(place.geometry.location.lat());
    $('#'+id+'_lng').val(place.geometry.location.lng());
    if (street_number == "")
      $('#'+id+'_address').val(address);
    else
      $('#'+id+'_address').val(address+" No."+street_number);
    $('#'+id+'_village').val(village);
    $('#'+id+'_sub_district').val(sub_district);
    $('#'+id+'_city').val(city);
    $('#'+id+'_province').val(province);
    $('#'+id+'_country').val(country);
    $('#'+id+'_zip').val(zip);

    check_field();
    calculateDistances();
}

function calculateDistances() {
	  var service = new google.maps.DistanceMatrixService();
		service.getDistanceMatrix({
        origins: [new google.maps.LatLng($('#origin_lat').val(), $('#origin_lng').val())],
        destinations: [new google.maps.LatLng($('#destination_lat').val(), $('#destination_lng').val())],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: true,
        avoidTolls: true,
        avoidFerries: true,
    }, callback);
}

function callback(response, status) {
  if (status != google.maps.DistanceMatrixStatus.OK) {
    alert('Error was: ' + status);
  } else {
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;
    var results = response.rows[0].elements;

    if (results[0].status == "OK") {
      $('#distance').val(results[0].distance.value);
    } else {
      $('#distance').val(0);
    }
  }
}

function resetMap() {
  var myLatLng = new google.maps.LatLng(-6.2297465,106.829518);
  origin_marker.setPosition(myLatLng);
  origin_map.setCenter(myLatLng);
  origin_map.setZoom(12);
  destination_marker.setPosition(myLatLng);
  destination_map.setCenter(myLatLng);
  destination_map.setZoom(12);
}

google.maps.event.addDomListener(window, 'load', initialize_from);
google.maps.event.addDomListener(window, 'load', initialize_to);



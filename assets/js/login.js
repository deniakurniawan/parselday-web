$(function() {
    $("input,textarea").not("[type=submit]").jqBootstrapValidation({
    	preventSubmit: true,
        submitSuccess: function($form, event) {
            if ($form[0].id == 'loginForm') {
	            event.preventDefault(); // prevent default submit behaviour
	            $.ajax({
	                url: config.base_url+"hello/login",
	                type: "POST",
	                data: {
	                	login_email: $('input#login_email').val(),
	                	login_password: $('input#login_password').val(),
	                	login_remember: $('input#login_remember').val(),
	                	form: $('input#form').val(),
	                },
	                cache: false,
	                success: function(data) {
	                    // Success message
	                    alert('success');
	                },
	                error: function() {
	                	// Fail message
	                    alert('error');
	                },
	            });
            } else if ($form[0].id == 'registerForm') {
	            event.preventDefault(); // prevent default submit behaviour
            	$.ajax({
	                url: config.base_url+"hello/contact_us",
	                type: "POST",
	                data: {
	                    register_fullname: $("input#register_fullname").val(),
	                    register_username: $("input#register_username").val(),
	                    register_email: $("input#register_email").val(),
	                    register_phone: $("input#register_phone").val(),
	                    register_password: $("input#register_password").val(),
	                	form: $('input#form').val(),
	                },
	                cache: false,
	                success: function(data) {
	                	// Success message
	                    alert('success');
	                },
	                error: function() {
	                    // Fail message
	                    alert('error');
	                },
	            });
            }
        },
    });
}
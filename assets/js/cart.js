function emptyCart() {
    if (confirm('are you sure wants to empty your cart?')){
        $.ajax({
                url: config.base_url+"hello/emptyCart",
                type: "POST",
                
                cache: false,
                success: function() {
                    // Success message
                location.reload();
            }
        });
    }
}

function removeItems(itemNo, itemId) {
    if (confirm('are you sure wants to delete this item?')){
        $.ajax({
                url: config.base_url+"hello/removeItems",
                type: "POST",
                data: {
                    rowid: itemId,
                },
                dataType: "json",
                cache: false,
                success: function(data) {
                    // Success message
                    $('.cartid_'+itemNo).remove();
                    data['total_items'] > 0 ? $('.cart-total-items').html("<span class='cart-no'>"+data['total_items']+"</span>") : $('.cart-total-items').html("");
                    $('.cart-total-items2').html(data['total_items']);
                    $('.cart-total-amt').html(data['total']);
                    // $('#cart_summary').load(config.base_url+'hello/cart_summary #cartmodal');
                    // $('#cartmodal').modal('show');
            }
        });
    }
}

function completeCart() {
    document.location.href=config.base_url+"hello/cart";
}

function createNew(frontpage) {
//    if (frontpage == 1) {
//        var $anchor = $(this);
//        $('html, body').stop().animate({
//            scrollTop: $('#shipment').offset().top - 30
//        }, 1500, 'easeInOutExpo');
//        event.preventDefault();
//    } else {
    $('#cartmodal').modal('hide');
    $('html, body').stop().animate({
        scrollTop: $('#shipment').offset().top - 30
    }, 1500, 'easeInOutExpo');
    $('#origin_search').focus();
//    }
}

function checkOut() {
    document.location.href=config.base_url+"hello/confirmCart";
}
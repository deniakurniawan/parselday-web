/* Date Picker */
//$(function () {
//});

/* Form Validation */


// var coverage_area = ['Surabaya', 'Tangerang', 'Kota Tangerang', 'Kota Tangerang Selatan', 'Kota Jakarta Barat', 'Kota Jakarta Selatan', 'Kota Jakarta Pusat', 'Kota Jakarta Timur', 'Kota Jakarta Utara'];

$(function() {
    $('#datetimepicker1').datetimepicker({
    	format: 'ddd, DD MMM YYYY'
    });

    $("input,textarea").not("[type=submit]").jqBootstrapValidation({
    	preventSubmit: true,
        submitSuccess: function($form, event) {
            if ($form[0].id == 'createOrderForm') {
	            event.preventDefault(); // prevent default submit behaviour

				// if(coverage_area.indexOf($('input#origin_city').val()) < 0){
  					// alert("Your shipment is not available on our coverage area");
				// }
				// else
				// {
					// check_field();
					
		            $.ajax({
		                url: config.base_url+"hello/create_order",
		                type: "POST",
		                data: {
		                	shipment_date: $('input#shipment_date').val(),

		                	origin_contact: $('input#sendername').val(),
							origin_lat: $('input#origin_lat').val(),
							origin_lng: $('input#origin_lng').val(),
							origin_address: $('input#origin_address').val(),
							origin_village: $('input#origin_village').val(),
							origin_city: $('input#origin_city').val(),
							origin_country: $('input#origin_country').val(),
							origin_sub_district: $('input#origin_sub_district').val(),
							origin_province: $('input#origin_province').val(),
							origin_zip: $('input#origin_zip').val(),
							origin_name: $('input#origin_name').val(),
							origin_phone: $('input#origin_phone').val(),
							origin_email: $('input#origin_email').val(),
							
							//destination data
		                	destination_contact: $('input#recipientname').val(),
							destination_lat: $('input#destination_lat').val(),
							destination_lng: $('input#destination_lng').val(),
							destination_address: $('input#destination_address').val(),
							destination_village: $('input#destination_village').val(),
							destination_city: $('input#destination_city').val(),
							destination_country: $('input#destination_country').val(),
							destination_sub_district: $('input#destination_sub_district').val(),
							destination_province: $('input#destination_province').val(),
							destination_zip: $('input#destination_zip').val(),
							destination_name: $('input#destination_name').val(),
							destination_phone: $('input#destination_phone').val(),
							destination_email: $('input#destination_email').val(),

							//items data
							item_name: $('input#item_name').val(),
							item_lenght: $('input#item_lenght').val(),
							item_width: $('input#item_width').val(),
							item_height: $('input#item_height').val(),
							item_weight: $('input#item_weight').val(),
							notes: $('#note').val(),
							distance: $('input#distance').val(),
							cost: $('input#cost').val(),
		                },
		                cache: false,
		                dataType: "json",
		                success: function(data) {
		                    // Success message
		                    $('#createOrderForm').trigger("reset");
		                    data > 0 ? $('.cart-total-items').html("<span class='cart-no'>"+data+"</span>") : $('.cart-total-items').html("");
		                    $('.cart-total-items2').html(data);
		                    $('#cart_summary').load(config.base_url+'hello/cart_summary #cartmodal');
		                    $('#cost-view').html('Rp0,-');
		                    resetMap();
		                },
		                error: function(XMLHttpRequest, textStatus, errorThrown) {
		                	// Fail message
		                	alert('Error while adding shipment to cart.');
		                },
		            });
				// }
            } else if ($form[0].id == 'contactForm') {
	            event.preventDefault(); // prevent default submit behaviour
	            var firstName = $("input#name").val(); // For Success/Failure Message
	            
	            // Check for white space in name for Success/Fail message
	            if (firstName.indexOf(' ') >= 0) {
	                firstName = name.split(' ').slice(0, -1).join(' ');
	            }
            	$.ajax({
	                url: config.base_url+"hello/contact_us",
	                type: "POST",
	                data: {
	                    name: $("input#name").val(),
	                    phone: $("input#phone").val(),
	                    email: $("input#email").val(),
	                    message: $("textarea#message").val(),
	                },
	                cache: false,
	                success: function(data) {
	                	// Success message
	                    $('#success').html("<div class='alert alert-success'>");
	                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
	                        .append("</button>");
	                    $('#success > .alert-success')
	                        .append("<strong>Your message has been sent. </strong>");
	                    $('#success > .alert-success')
	                        .append('</div>');

	                    //clear all fields
	                    $('#contactForm').trigger("reset");
	                },
	                error: function() {
	                    // Fail message
	                    $('#success').html("<div class='alert alert-danger'>");
	                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
	                        .append("</button>");
	                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
	                    $('#success > .alert-danger').append('</div>');
	                    //clear all fields
	                    $('#contactForm').trigger("reset");
	                },
	            });
            }
        },
    });

	$('input,textarea').change(function(){
		check_field();
	})
});

$('#sendername').autocomplete({
	source:config.base_url+"hello/contact",
	type:"post",
    minLength: 0,
    select: function( event, ui ) {
		$('#origin_lat').val(ui.item ? ui.item.lat : '');
		$('#origin_lng').val(ui.item ? ui.item.lng : '');
		$('#origin_address').val(ui.item ? ui.item.address : '');
		$('#origin_village').val(ui.item ? ui.item.village : '');
		$('#origin_sub_district').val(ui.item ? ui.item.sub_district : '');
		$('#origin_city').val(ui.item ? ui.item.city : '');
		$('#origin_province').val(ui.item ? ui.item.province : '');
		$('#origin_country').val(ui.item ? ui.item.country : '');
		$('#origin_zip').val(ui.item ? ui.item.zip : '');
		$('#origin_name').val(ui.item ? ui.item.name : '');
		$('#origin_phone').val(ui.item ? ui.item.phone : '');
		$('#origin_email').val(ui.item ? ui.item.email : '');

		if ($('#origin_lat').val() != '' && $('#origin_lng').val() != '') {
			var myLatLng = new google.maps.LatLng($('#origin_lat').val(),$('#origin_lng').val());

			if (myLatLng != origin_marker.position) {
				origin_map.setCenter(myLatLng);
				origin_map.setZoom(17);
				origin_marker.setPosition(myLatLng);

				calculateDistances();
			}
		}
    }
}); 

$('#recipientname').autocomplete({
	source:config.base_url+"hello/contact",
	type:"post",
    minLength: 0,
    select: function( event, ui ) {
		$('#destination_lat').val(ui.item ? ui.item.lat : '');
		$('#destination_lng').val(ui.item ? ui.item.lng : '');
		$('#destination_address').val(ui.item ? ui.item.address : '');
		$('#destination_village').val(ui.item ? ui.item.village : '');
		$('#destination_sub_district').val(ui.item ? ui.item.sub_district : '');
		$('#destination_city').val(ui.item ? ui.item.city : '');
		$('#destination_province').val(ui.item ? ui.item.province : '');
		$('#destination_country').val(ui.item ? ui.item.country : '');
		$('#destination_zip').val(ui.item ? ui.item.zip : '');
		$('#destination_name').val(ui.item ? ui.item.name : '');
		$('#destination_phone').val(ui.item ? ui.item.phone : '');
		$('#destination_email').val(ui.item ? ui.item.email : '');

		if ($('#destination_lat').val() != '' && $('#destination_lng').val() != '') {
			var myLatLng = new google.maps.LatLng($('#destination_lat').val(),$('#destination_lng').val());

			if (myLatLng != destination_marker.position) {
				destination_map.setCenter(myLatLng);
				destination_map.setZoom(17);
				destination_marker.setPosition(myLatLng);
				
				calculateDistances()
			}
		}
    }
}); 


// Tanggal 15 AGUSTUS 2015 - 31 AGUSTUS 2015 PROMOSI (***************)
// Min 19 rb (10 km) (1000 / km berikut nya)
// Max 45 rb (36 km - 60 km) diatas itu ditolak
// Semut 80%

// RUMUS PERHITUNGAN SAAT INI (create_form.js)
// Min 25 rb (6 km) (4000 / km berikut nya)
// Max ~
// Semut 65%
function check_field()
{
  
  if ($('#origin_address').val() != '' &&
  	  $('#origin_city').val() != '' && 
      $('#origin_province').val() != '' &&
      $('#origin_country').val() != '' &&
      $('#origin_zip').val() != '' &&
      $('#destination_address').val() != '' &&
      $('#destination_city').val() != '' &&
      $('#destination_province').val() != '' &&
      $('#destination_country').val() != '' &&
      $('#destination_zip').val() != '' &&
      $('#item_name').val() != '' &&
      $('#item_length').val() != '' &&
      $('#item_width').val() != '' &&
      $('#item_height').val() != '' &&
      $('#item_weight').val() != ''
  )
  {
		// var distance = parseFloat($('#distance').val());
		// var cost = 0;

		// if (isNaN(distance)) {
		// 	cost = 0;
		// } 
		// else {
		// 	var minDistance = 10000; //satuan Meter
		// 	var maxDistance_min = 36000; //satuan Meter
		// 	var maxDistance_max = 45000; //satuan Mter
		// 	var extraDistance = 0;
		// 	var price_per_next_km = 1000; //rupiah
		// 	var minPrice = 19000; //rupiah
		// 	var maxPrice = 45000; //rupiah

		// 	if(distance > minDistance)
		// 	{
		// 		extraDistance = distance - minDistance;
		// 		extraPrice = Math.round(extraDistance/1000) * 1000;
		// 		//console.log(extraDistance);
		// 		//console.log(extraPrice);

		// 		cost = extraPrice + minPrice;

		// 		if(distance > maxDistance_min && distance < maxDistance_max){
		// 		 	cost = maxPrice;
		// 		}
		// 	}
		// 	if(distance < minDistance)
		// 	{
		// 		cost = minPrice;
		// 	}
		// 	if(distance > maxDistance_max){
		// 		alert("Your shipment can't be processed");
		// 		cost = 0;
		// 	}

		//     //extra_distance = Math.max(distance - 6000,0)
		//     //cost = 25000 + (Math.round(extra_distance/250)*1000)

			$.post(config.base_url + "hello/calculate_price", { origin_lat: $('#origin_lat').val(), 
																origin_lon: $('#origin_lng').val(),
																destination_lat: $('#destination_lat').val(),
																destination_lon: $('#destination_lng').val()
															  },
		   function(ex)
		   {
				if(parseInt(ex))
				{

					$('#cost').val(ex);
					$('#cost-view').html(numberToRupiah(ex));
				}
				else{
					alert(ex);
					$('#cost').val('0');
					$('#cost-view').html(numberToRupiah('0'));
				}
				console.log(ex);
		   });
		//}

		
  	}
}


function numberToRupiah(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	return 'Rp. '+parts[0]+',-'
}

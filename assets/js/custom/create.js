var t;
var counter;

$(document).ready(function(){

	t = $('#tables').DataTable({
		"scrollX": true,
		"columnDefs": [
			{
				"targets": [ 0, 1, 2 ],
				"visible": false
			}
		]
	});

	$("#combo_multi").change(function(event) {
		t.clear().draw();
		
		$('#cost-view').html("Rp 0");
		
		var multi = $("#combo_multi").val();
		
		switch(multi) {
			case "1":
				$("#lb_title").html("Asal");
				document.getElementById("origin_search").placeholder = "Alamat Pengambilan, (PT. IMS Logistics)",
				document.getElementById("destination_address").placeholder = "Detail Alamat Pengambilan",
				document.getElementById("destination_name").placeholder = "Nama Lengkap Pengirim";
				break;
			case "2":
				$("#lb_title").html("Asal");
				document.getElementById("origin_search").placeholder = "Alamat Pengambilan, (PT. IMS Logistics)",
				document.getElementById("destination_address").placeholder = "Detail Alamat Pengambilan",
				document.getElementById("destination_name").placeholder = "Nama Lengkap Pengirim";
				break;
			case "3":
				$("#lb_title").html("Tujuan");
				document.getElementById("origin_search").placeholder = "Alamat Pengantaran, (PT. IMS Logistics)",
				document.getElementById("destination_address").placeholder = "Detail Alamat Penerima",
				document.getElementById("destination_name").placeholder = "Nama Lengkap Penerima";
				break;
			default:
				alert ('null');
		}
		
		$('#img_multi').attr("src", config.base_url + 'assets/img/create/00' + $(this).val() + '.jpg' );
	});

	/* filtering destination id */
	$('#addRow').on( 'click', function () {
		var input0 = document.getElementById("origin_search");
		var input1 = document.getElementById("destination_address");
		var input2 = document.getElementById("destination_name");
		
		if ($('#lb_title').html("Asal")) {
			input0.placeholder = "Alamat Pengambilan, (PT. IMS Logistics)";
			input1.placeholder = "Detail Alamat Pengambilan";
			input2.placeholder = "Nama Lengkap Pengirim";
		}
		if ($('#lb_title').html("Tujuan")) {
			input0.placeholder = "Alamat Pengantaran, (PT. IMS Logistics)";
			input1.placeholder = "Detail Alamat Penerima";
			input2.placeholder = "Nama Lengkap Penerima";
		}
		return false;
	});

	/* filtering destination */
    $('.btn-radio').click(function(e) {
		$('.btn-radio').removeClass('active');
		
		$(this).addClass('active');
		
		var button_val = $(this).children( "span" ).html();
		
		$('#destination_caption').val(button_val);
    });

    

   	//number
 	// if($('#destination_phone').val() != "") {
	//     var value = $('#destination_phone').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	//     var intRegex = /^\d+$/;
	//     alert('helo');
	//     if(!intRegex.test(value)) {
	//         errors += "Field must be numeric.<br/>";
	//         success = false;
	//     }
	// } 
	// else {
	//     errors += "Field is blank.</br />";
	//     success = false;
	// }

    function swal_warn(message) {
		swal({	title: 'Lengkapi Informasi Anda',
				text: message,
				imageUrl: config.base_url + 'assets/img/warning-icon-yellow.png',
				animation: false,
				time:2000 
			});
    }

    $('#addRow').on( 'click', function (e) {
		var message = "";
    	var format_swal = false;

		if ($('#destination_name').val() == '') {
			message = "Anda belum mengisi nama anda";
			swal_warn(message);
			return false;
		} 
		if ($('#destination_address').val() == '') {
			message = "Anda belum mengisi alamat anda";
			swal_warn(message);
			return false;
		}
		if ($('#destination_email').val() == '') {
			message = "Anda belum mengisi email anda";
			swal_warn(message);
			return false;
		}
		if ($('#destination_phone').val() == '') {
			message = "Anda belum mengisi no telfon anda";
			swal_warn(message);
			return false;
		}
		if ($('#destination_qty').val() == '') {
			message = "Anda belum mengisi jumlah barang yang anda ingin kirimkan";
			swal_warn(message);
			return false;
		}
		if ($('#destination_caption').val() == '') {
			message = "Anda belum mengisi keterangan barang";
			swal_warn(message);
			return false;
		}

		var rowAsal = checkLoc("Asal");
		var rowTujuan = checkLoc("Tujuan");
		
		var checkRow = false;
		var lokasi = "Asal";
		
		var multi = $("#combo_multi").val();
		var message = ""
		
		var btn_del = '';
		
		if (rowAsal + rowTujuan >= 1) {
			btn_del = '<a href="javascript:void(0)" onclick="rem_point(this);" class="glyp-color"><span class="glyphicon glyphicon-trash"></span></a>';
		}
		
		switch(multi) {
			case "1":
				if ((rowAsal <= 1) && (rowAsal + rowTujuan < 2)) {
					checkRow = true;
				}
				if (rowAsal == 1) {
					lokasi = "Tujuan";
				}
				message = 'Hanya Bisa Satu ASAL dan Satu TUJUAN';
				break;
			case "2":
				if ((rowAsal <= 1) && (rowAsal + rowTujuan < 6)) {
					checkRow = true;
				}
				if (rowAsal == 1) {
					lokasi = "Tujuan";
				} 
				message = 'Hanya Bisa Satu ASAL dan Maksimal 5 TUJUAN';
				break;
			case "3":
				lokasi = "Tujuan";
				
				if ((rowTujuan <= 1) && (rowTujuan + rowAsal < 6)) {
					checkRow = true;
				}
				if (rowTujuan == 1) {
					lokasi = "Asal";
				}
				message = 'Hanya Bisa Satu TUJUAN dan Maximal 5 ASAL'
				break;
			default:
				swal('null');
				//alert ('null');
		}
		
		if (!checkRow) {
			swal({	title: 'Oops !!!',
					text: message,
					imageUrl: config.base_url + 'assets/img/warning-icon-yellow.png',
					animation: false,
					time:2000 
				});
			//alert(message);
		}
		else {
			var row = [
				counter,
				$('#origin_lat').val(),
				$('#origin_lng').val(),
				lokasi,
				$('#destination_name').val(),
				$('#destination_address').val(),
				$('#destination_phone').val(),
				$('#destination_email').val(),
				$('#destination_qty').val(),
				$('#destination_caption').val(),
				$('#panjang').val(),
				$('#tinggi').val(),
				$('#lebar').val(),
				$('#berat').val(),
				btn_del
			];

			$.post(
				config.base_url + "shipment/multi", 
				{ 
					multi: multi,
					data: getData(row)
				},
				function(ex) {
					if (ex.success) {
						$('#cost-view').html(ex.price);
						t.row.add(row).draw();
						$("#lb_title").html(checkHeader());
						counter++;
					}
					else {
						swal(ex.message);
						//alert(ex.message);
					}
				}
			);
			return false;
		}
    });
 
    $('#b_submit').on( 'click', function () {
		var rowAsal = checkLoc("Asal");
		var rowTujuan = checkLoc("Tujuan");
		
		var checkRow = false;
		
		var multi = $("#combo_multi").val();
		var message = "";

		switch(multi) {
			case "1":
				if ((rowAsal == 1) && (rowTujuan == 1)) {
					checkRow = true;
				}
				message = 'Harap isi ASAL dan TUJUAN Terlebih Dahulu';
				break;
			case "2":
				if ((rowAsal == 1) && (rowTujuan > 0) && (rowTujuan < 6)) {
					checkRow = true;
				} 
				message = 'Harap isi ASAL dan TUJUAN Terlebih Dahulu';
				break;
			case "3":
				if ((rowTujuan == 1) && (rowAsal > 0) && (rowAsal < 6)) {
					checkRow = true;
				} 
				message = 'Harap isi ASAL dan TUJUAN Terlebih Dahulu';
				break;
			default:
				swal('null');
		}
		
		if (!checkRow) {
			swal({	title: 'Oops !!!',
					text: message,
					imageUrl: config.base_url + 'assets/img/warning-icon-red.png',
					animation: false,
					time:2000 
				});
		}
		else {
			$.post(
				config.base_url + "shipment/add_cart", 
				{ 
					multi: multi,
					data: getData()
				},
				function(ex) {
					if (ex.success) {
						shipment_resetAll();
						
						$('.cart-total-items').html("<span class='cart-no'>" + ex.total + "</span>");
						$('#cart_summary').load(config.base_url+'hello/cart_summary #cartmodal');
					}
					else {
						swal(ex.message);
					}
				}
			);
		}
		
    });
	
	function shipment_resetAll() {
		counter = 1;
		
		$("#combo_multi").val("1");
		
		t.clear().draw();
		$('#cost-view').html("0");
		$("#lb_title").html("Asal");
		
		$("#datepicker").datetimepicker({defaultDate: moment().add('hours', 3)});
		
		shipment_reset();
	}
	function shipment_reset() {
		$('#origin_lat').val("-6.2297465");
		$('#origin_lng').val("106.829518");

		$('#destination_name').val("");
		$('#destination_address').val("");
		$('#destination_email').val("");
		$('#destination_phone').val("");
		
		$('.btn-radio').removeClass('active');
		$('.btn-radio').first().addClass('active');
		$('#destination_caption').val($('.btn-radio').first().children( "span" ).html());
		$('#destination_qty').val("1");
		$('#panjang').val(),
		$('#tinggi').val(),
		$('#lebar').val(),
		$('#berat').val()
	}
	
	shipment_resetAll();
}); 

function rem_point(e) {
	t
	.row( $(e).parents('tr') )
	.remove()
	.draw();
	
	$.post(
		config.base_url + "shipment/multi", 
		{ 
			multi: $("#combo_multi").val(),
			data: getData()
		},
		function(ex) {
			if (ex.success) {
				$('#cost-view').html(ex.price);

				counter++;
			}
			else {
				alert (ex.message);
			}
		}
	);
};

function set_address(results) {
	console.log(results);
	
	$('#origin_lat').val(results.geometry.location.lat());
	$('#origin_lng').val(results.geometry.location.lng());
	
	$('#destination_address').val(results.formatted_address);
}

function checkLoc(satu) {
	var data = t.rows().data();
	
	var rowBanyak = 0;
	
	for (var i=0; i<data.length; i++) {
		if (data[i][3] == satu) {
			rowBanyak++;
		}
	}
	
	return rowBanyak;
}

function getData(row) {
	var rows = [];
	
	var data = t.rows().data();
	
	for (var i=0; i<data.length; i++) {
		rows.push(data[i]);
	}
	
	if ($.isArray(row)) {
		rows.push(row);
	}
	
	return JSON.stringify(rows);
}

function checkHeader() {
	var multi = $("#combo_multi").val();
	var lokasi = "";
	
	var rowAsal = checkLoc("Asal");
	var rowTujuan = checkLoc("Tujuan");

	switch(multi) {
		case "1":
			if ((rowAsal <= 1) && (rowAsal + rowTujuan < 2)) {
				checkRow = true;
			}
			if (rowAsal == 1) {
				lokasi = "Tujuan";
			}
			break;
		case "2":
			if ((rowAsal <= 1) && (rowAsal + rowTujuan < 6)) {
				checkRow = true;
			}
			if (rowAsal == 1) {
				lokasi = "Tujuan";
			}
			break;
		case "3":
			lokasi = "Tujuan";
			
			if ((rowTujuan <= 1) && (rowTujuan + rowAsal < 6)) {
				checkRow = true;
			}
			if (rowTujuan == 1) {
				lokasi = "Asal";
			}
			break;
		default:
			alert ('null');
	}
	
	return lokasi;
}


/* maps */
var geocoder = new google.maps.Geocoder();
var latlng = new google.maps.LatLng(-6.2297465,106.829518);

//SETTING MAP
var mapOptions = {
  scrollwheel: false,
  disableDefaultUI: true,
  zoom: 12,
  center: latlng,
  mapTypeId: 'roadmap',
  streetViewControl: false,
  panControl: false,
  zoomControl: true,
}


function initialize_from() {
  var id_map = "senderdata";
  var id_search = "origin_search";

  //MAP
  var origin_map = new google.maps.Map(document.getElementById(id_map), mapOptions);

  //MARKER
  var origin_marker = new google.maps.Marker({
    position: latlng,
    map: origin_map,
    draggable:true,
    animation: google.maps.Animation.DROP,
  });

  //EVENT MARKER
  google.maps.event.addListener(origin_marker, "dragend", function (e) {
    geocoder.geocode({'latLng': e.latLng}, function(results, status) {
	  set_address(results[0]);
    })
  });

  //SEARCH MAP
  var autocomplete = new google.maps.places.Autocomplete((document.getElementById(id_search)),{/*types:['geocode']*/});
  google.maps.event.addListener(autocomplete, "place_changed", function() {
    var results = autocomplete.getPlace();

    origin_map.setCenter(results.geometry.location);
    origin_map.setZoom(17);  // Why 17? Because it looks good.
    origin_marker.setPosition(results.geometry.location);

    set_address(results);
  });

  google.maps.event.addDomListener(document.getElementById(id_search), 'keydown', function(e) {
    if (e.keyCode == 13)
      e.preventDefault();
  });
}

// RUNNING MAP
google.maps.event.addDomListener(window, 'load', initialize_from);

function initialize() {
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var latlng = new google.maps.LatLng(-6.2297465,106.829518);

	var mapOptions = {
		disableDefaultUI: true,
		zoom: 12,
		center: latlng,
		mapTypeId: 'roadmap',
		streetViewControl: false,
		panControl: false,
		zoomControl: true,
	}

	var map = new google.maps.Map(document.getElementById('directiondata'), mapOptions);
	directionsDisplay.setMap(map)

	var from = new google.maps.LatLng($('#lat_from').val(), $('#lng_from').val());
	var to = new google.maps.LatLng($('#lat_to').val(), $('#lng_to').val());
	var setup = {
		origin: from,
		destination: to,
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		avoidHighways: true,
		avoidTolls: true,
		avoidFerries: true,
	}

	directionsService.route(setup, callback);

	function callback(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	}
}


google.maps.event.addDomListener(window, 'load', initialize);

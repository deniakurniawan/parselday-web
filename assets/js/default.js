/*!
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 30
        }, 1500, 'easeInOutExpo');
        //event.preventDefault();
        if($anchor.attr('href') == '#shipment') {
            $('#origin_search').focus();
        }else if($anchor.attr('href') == '#track') {
            $('#shipment_id').focus();
        }else if($anchor.attr('href') == '#contact') {
            $('#name').focus();
        }
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top',
    offset: 30,
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// Check validation on contact form and send email (replacable with CI code)
/*
$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
            var firstName = name; // For Success/Failure Message
            
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "././mail/contact_me.php",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    message: message
                },
                cache: false,
                success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your message has been sent. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});
*/

/* When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});

/* Map utilities 
$('#senderdata').locationpicker({
    location: {latitude: -6.175120, longitude: 106.827132},
    disableDefaultUI: true,
    radius: 0,
    inputBinding: {
        locationNameInput: $('#origin_address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        alert("Sender data changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
});
$('#recipientdata').locationpicker({
    location: {latitude: -6.175120, longitude: 106.827132},
    radius: 0,
    inputBinding: {
        locationNameInput: $('#recipient-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        alert("Recipient data changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
});*/

// Autocomplete users
/*
var $input = $('#sendername');
$input.typeahead({source:[
            {id: "1", name: "Adi Suadi"},
            {id: "2", name: "Budi Syahbudi"}, 
            {id: "3", name: "Christopher Columbus"}, 
            {id: "4", name: "Dana Suyana"}, 
            {id: "5", name: "Edi Supendi"}, 
            {id: "6", name: "Fara Mathafacka"}, 
            {id: "7", name: "Gogon Gundrung"}, 
            {id: "8", name: "Hadi Ismaya"}, 
            {id: "9", name: "Joko Santoso"},  
            {id: "10", name: "Merriam Belina"}], 
            autoSelect: true}); 
$input.change(function() {
    var current = $input.typeahead("getActive");
    if (current) {
        if (current.name == $input.val()) {
            
        } else {
            
        }
    } else {
        
    }
});
var $input = $('#recipientname');
$input.typeahead({source:[
            {id: "1", name: "Adi Suadi"},
            {id: "2", name: "Budi Syahbudi"}, 
            {id: "3", name: "Christopher Columbus"}, 
            {id: "4", name: "Dana Suyana"}, 
            {id: "5", name: "Edi Supendi"}, 
            {id: "6", name: "Fara Mathafacka"}, 
            {id: "7", name: "Gogon Gundrung"}, 
            {id: "8", name: "Hadi Ismaya"}, 
            {id: "9", name: "Joko Santoso"},  
            {id: "10", name: "Merriam Belina"}], 
            autoSelect: true}); 
$input.change(function() {
    var current = $input.typeahead("getActive");
    if (current) {
        if (current.name == $input.val()) {
            
        } else {
            
        }
    } else {
        
    }
});
*/
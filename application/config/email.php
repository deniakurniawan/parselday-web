<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'ssl://smtp.zoho.com';
$config['smtp_port'] = '465';
$config['smtp_timeout'] = '7';
$config['smtp_user'] = 'noreply@parselday.com';
$config['smtp_pass'] = '6IkYjYe0jKXPiLkD';
//$config['smtp_crypto'] = ''
$config['wordwrap'] = TRUE;

$config['mail_sender'] = 'noreply@parselday.com';
$config['mail_sender_name'] = 'ParselDay';

/* End of file email.php */
/* Location: ./application/config/email.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_price extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function get_grup_area($nama) {
		$query = $this->db->query("SELECT grup FROM area WHERE nama = '".$nama."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['grup'];
		}
		return 0;
	}

	function get_price_area($area, $setting) {
		$query = $this->db->query("SELECT price FROM area_price WHERE area_id = " . $area . " AND setting >= " . $setting . " ORDER BY setting LIMIT 1;");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0]['price'];
		}
		return 0;
	}
}

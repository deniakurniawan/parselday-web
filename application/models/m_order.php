<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_order extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function getOrderId() {
		$this -> db -> select('Last');
		$this -> db -> from('auto_number');
		$this -> db -> where('Object','Order');
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		$seq = 1;
		if($query -> num_rows() == 1) {
			$seq = ((int)$query->result()[0]->Last) + 1;
		}
		$month = ['A','B','C','D','E','F','G','H','I','J','K','L'];
		$order_id = 'CO' . date('y') . $month[date('n')-1] . date('d') . ($seq % 10000 == 0 ? sprintf("%04s", $seq) : sprintf("%04s", $seq));

		return $order_id;
	}

	function encryptLink($string){
		//$this->load->library('encrypt');
		//$this->encrypt->set_cipher(MCRYPT_BLOWFISH);
		//$key = 'parselday';
		//$encrypted_string = $this->encrypt->encode($string, $key);
		$encrypted_string = md5($string);
		$encrypted_string = str_replace(array('+', '/', '='), array('-', '_', '~'), $encrypted_string);
		return $encrypted_string;
	}
}

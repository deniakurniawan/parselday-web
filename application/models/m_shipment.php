<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_shipment extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function getTrackUrl($shipment_id) {
		$query = $this->db->query("SELECT a.*, b.Tanggal as TglPOP, c.Tanggal as TglPOD, timediff(c.tanggal, b.tanggal) as waktu FROM shipment a left join signature2 b on a.id = b.Shipment and b.Tipe = 'pop' left join signature2 c on a.id = c.Shipment and c.Tipe = 'pod' WHERE a.Shipment = '".$shipment_id."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		} else {
			return false;
		}
	}

	function getShipment($parsel) {
		$query = $this->db->query("SELECT * FROM shipment WHERE track_url = '".$parsel."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		}
		return false;
	}
	


	function getTracking($parsel) {
		$query = $this->db->query("SELECT a.*, b.Tanggal as TglPOP, c.Tanggal as TglPOD, timediff(c.tanggal, b.tanggal) as waktu FROM shipment a left join signature2 b on a.id = b.Shipment and b.Tipe = 'pop' left join signature2 c on a.id = c.Shipment and c.Tipe = 'pod' WHERE a.track_url = '".$parsel."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		}
		return false;
	}

	function getAreaID($city) {
		$query = $this->db->query("SELECT ID FROM area WHERE nama = '".$city."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		}
		return false;
	}

	function getShipmentAll() {
		$query = $this->db->query("SELECT ID, Dari_Lat, Dari_Lon FROM shipment");
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return false;
	}

	function getSelectedShipment($order_id) {
		$query = $this->db->query("SELECT * FROM shipment WHERE OrderID = '".$order_id."'");
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		else{
			return false;
		}
	}

	function getSelectedOrder($order_id) {
		$query = $this->db->query("SELECT * FROM orders WHERE ID = '".$order_id."'");
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		else{
			return false;
		}
	}

	function getShipmentId() {
		$this -> db -> select('Last');
		$this -> db -> from('auto_number');
		$this -> db -> where('Object','Shipment');
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		$seq = 1;
		if($query -> num_rows() == 1) {
			$seq = ((int)$query->result()[0]->Last) + 1;
		}
		$month = ['A','B','C','D','E','F','G','H','I','J','K','L'];
		$shipment_id = 'JO' . date('y') . $month[date('n')-1] . date('d') . ($seq % 10000 == 0 ? sprintf("%04s", $seq) : sprintf("%04s", $seq));

		return $shipment_id;
	}

	function deleteOrder($order_id) {
		$query = $this->db->query("DELETE FROM orders WHERE ID = '".$order_id."'");
		$query2 = $this->db->query("DELETE FROM shipment WHERE OrderID = '".$order_id."'");
	}


	function updateLast() {
		$this -> db -> select('Last');
		$this -> db -> from('auto');
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1) {
			$seq = ((int)$query->result()[0]->Last) + 1;
		}
		$data = array('Last'=>$seq);
		$this->db->update('auto',$data);
	}

	function encryptLinkTrack($string){
		//$this->load->library('encrypt');
		//$key = 'parseldaytrack';
		//$encrypted_string = $this->encrypt->encode($string, $key);
		$encrypted_string = md5($string);
		$encrypted_string = str_replace(array('+', '/', '='), array('-', '_', '~'), $encrypted_string);
		return $encrypted_string;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_base extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function insert($table_name,$data) {
		$this->db->insert($table_name,$data);
		return $this->db->insert_id();
	}

	function update($table_name,$data,$field_key) {
		return $this->db->update($table_name,$data,$field_key);
		//return true;
	}

	function getSelectedData($table_name,$field_key,$order_by = '') {
		if ($order_by != '')
			$this->db->order_by($order_by);
		$query = $this -> db -> get_where($table_name,$field_key);
		if($query -> num_rows() > 0) {
			return $query -> result_array();
		} else {
			return false;
		}
	}

	function select($table_name,$column_name = '*',$where_clause = array(),$where_string='') {
		$this->db->select($column_name);
		if (count($where_clause) > 0)
			$this->db->where($where_clause);
		if ($where_string != '')
			$this->db->where($where_string);
		return $this->db->get($table_name)->result_array();
	}

	function autocomplete($table_name, $term, $column_name = '*', $where_clause = array()) {
		$this->db->select($column_name);
		$this->db->like('Nama', $term);
		return $this->db->get_where($table_name, $where_clause, 10)->result_array();
	}
}

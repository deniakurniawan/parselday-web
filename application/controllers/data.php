<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller {
	public function index() {
	}
	
	public function lang($lang) {
		if ($lang == "en")
			$this->session->set_userdata(array('lang' => 'english'));
		else
			$this->session->set_userdata(array('lang' => 'indonesia'));
		
		redirect('/');
	}
}

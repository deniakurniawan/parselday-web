<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shipment extends CI_Controller {
	function __construct() {
		parent::__construct();
		
		header('Content-Type: application/json');
		
		$this->load->library('general');
		
		$this->load->library('cart');
		$this->load->library('tank_auth');
		
		$this->load->model('m_base');
		$this->load->model('m_shipment');
		$this->load->model('m_order');
	}
	
	function calculate($multi, $data) {
		$result = array();
		$ori = array();
		$des = array();
		
		$success = TRUE;
		$message = '';
		$point = 0;
		$distance = 0;
		$harga = 0;
		
		// PISAH ORI DAN DES
		for($i=0; $i<sizeof($data); $i++) {
			$latlng = $data[$i][1] . ',' . $data[$i][2];
			
			if ($data[$i][3] == 'Asal') {
				array_push($ori, $latlng);
			}
			elseif ($data[$i][3] == 'Tujuan') {
				array_push($des, $latlng);
			}
		}
		
		if (sizeof($ori) > 0 && sizeof($des) > 0) {
			$origins = join('|',$ori);
			$destinations = join('|',$des);
			
			switch ($multi) {
				case "1": // ONE TO ONE
					$point = sizeof($des);
					
					if (sizeof($ori) > 1 || sizeof($des) > 1 || $point == 0) {
						$success = FALSE;
						$message = '\nAsal dan tujuan hanya boleh 1';
						$point = 0;
					}
					
					break;
				case "2": // ONE TO MANY
					$point = sizeof($des);
					
					if (sizeof($ori) > 1 || sizeof($des) > 5 || $point == 0) {
						$success = FALSE;
						$message = '\nAsal hanya boleh 1 dan tujuan maksimal 5';
						$point = 0;
					}
					
					break;
				case "3": // MANY TO ONE
					$point = sizeof($ori);
					
					if (sizeof($ori) > 1 || sizeof($des) > 5 || $point == 0) {
						$success = FALSE;
						$message = '\nAsal maksimal 5 dan tujuan hanya boleh 1';
						$point = 0;
					}
					
					break;
				default: // WRONG INPUT
					$success = FALSE;
					$message = 'salah input';
					$point = 0;
			}
		
			if ($success) { // CALCULATE PRICE
				$distance = round($this->general->multiDistance($ori, $des));
				$distance = $distance < 6 ? 6 : $distance;
				
				$harga = ($distance * 2500) + ($point * 5000);
			}
		}
		
		$result['success'] = $success;
		$result['message'] = $message;
		$result['distance'] = $distance;
		$result['price'] = $harga;
		
		return $result;
	}
	
	function multi() {
		header('Content-Type: application/json');
		
		$multi = $this->input->post('multi');
		$data = json_decode($this->input->post('data'));
		
		echo json_encode($this->calculate($multi, $data));
	}
	
	function add_cart() {
		$cart_id = "";
		
		if ($this->cart->total_items()>0){
			$cart_id = end($this->cart->contents())['id']+1;
		}
		else{
			$cart_id = 1;
		}
		
		$multi = $this->input->post('multi');
		$multiName = '';
		$shipment = json_decode($this->input->post('data'));
		
		switch ($multi) {
			case "1": // ONE TO ONE
				$multiName = 'ONE TO ONE';
				
				break;
			case "2": // ONE TO MANY
				$multiName = 'ONE TO MANY';
				
				break;
			case "3": // MANY TO ONE
				$multiName = 'MANY TO ONE';
				
				break;
			default: // WRONG INPUT
				$multiName = 'WRONG INPUT';
		}
		
		
		$calculate = $this->calculate($multi, $shipment);
		
		if ($calculate['success']) {
			$data = array(
				'id'      => $cart_id,
				'qty'     => 1,
				'price'   => $calculate['price'],
				'name'    => $multiName,
				'options' => $shipment
			);
			
			$this->cart->insert($data);
		}
		
		$result['success'] = $calculate['success'];
		$result['message'] = $calculate['message'];
		$result['total'] = $this->cart->total_items();
		
		echo json_encode($result);
	}
	
	function remove_cart() {
		$rowid = $this->input->post('rowid');
		
		$data = array(
			'rowid'   => $rowid,
			'qty'     => 0,
		);

		$this->cart->update($data);
		
		echo json_encode($this->cart->contents());
	}
	
	function empty_cart() {
		$this->cart->destroy();
		
		echo json_encode($this->cart->contents());
	}
	
	function create_order() {
	}
	
	function confirm_payment() {
	}
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Hello extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->_init();
	}

	private function _init() {
		if ($this->session->userdata('lang') == '') {
			$this->session->set_userdata(array('lang' => 'indonesia'));
		}
		
		$this->lang->load('tank_auth', $this->session->userdata('lang'));
		$this->lang->load('email', $this->session->userdata('lang'));
        $this->lang->load("content", $this->session->userdata('lang'));
		
		$this->load->library('cart');
		$this->load->library('tank_auth');
		
		$this->load->model('m_base');
		$this->load->model('m_shipment');
		$this->load->model('m_order');
		
		$this->output->set_template('frontpage');
		
		$this->load->section('cart_summary', 'cart/summary.php');
		$this->load->section('footer', 'footer.php');

		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
	}
	
	public function abc($lang)
	{
		if ($lang == "en")
			$this->session->set_userdata(array('lang' => 'english'));
		else
			$this->session->set_userdata(array('lang' => 'indonesia'));
		
		redirect('/');
	}

	function index() {

		$this->load->css($this->config->base_url()."assets/css/bootstrap-datetimepicker.min.css");
		// $this->load->css($this->config->base_url()."assets/css/jquery-ui.min.css");
		
		$this->load->css($this->config->base_url()."assets/css/jquery.dataTables.min.css");
		$this->load->css($this->config->base_url()."assets/css/mystyle.css");
		$this->load->css($this->config->base_url()."assets/css/sweetalert2.css");
		// $this->load->css($this->config->base_url()."assets/css/toggle-switch.css");
		// $this->load->css($this->config->base_url()."assets/css/bootstrap-select.min.css");

		// $this->load->js($this->config->base_url().'assets/js/jquery-ui.min.js');

		$this->load->js('https://maps.googleapis.com/maps/api/js?v=3&libraries=places&region=id');

		$this->load->js($this->config->base_url()."assets/js/moment.js");
		$this->load->js($this->config->base_url()."assets/js/bootstrap-datetimepicker.min.js");
		
		$this->load->js($this->config->base_url()."assets/js/jquery.dataTables.min.js");
		// $this->load->js($this->config->base_url()."assets/js/bootstrap-select.js");
		$this->load->js($this->config->base_url()."assets/js/custom/create.js");
		$this->load->js($this->config->base_url()."assets/js/jquery.validate.min.js");

		// $this->load->js($this->config->base_url().'assets/js/create_form.js');
		// $this->output->set_template('frontpage');

		$data['frontpage'] = true;
		
		$this->load->view('multidrop/create', $data);
	}

	function login() {
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('/');
		} 
		else if ($this->input->post('form') == 'login') {
			$this->load->library('form_validation');
			$this->load->library('security');

			$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
					$this->config->item('use_username', 'tank_auth'));
			$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

			$this->form_validation->set_rules('login_email', 'Login', 'trim|required|xss_clean');
			$this->form_validation->set_rules('login_password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('login_remember', 'Remember me', 'integer');

			// Get login for counting attempts to login
			if ($this->config->item('login_count_attempts', 'tank_auth') AND
					($login = $this->input->post('login_email'))) {
				$login = $this->security->xss_clean($login);
			} else {
				$login = '';
			}

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->login(
						$this->form_validation->set_value('login_email'),
						$this->form_validation->set_value('login_password'),
						$this->form_validation->set_value('login_remember'),
						$data['login_by_username'],
						$data['login_by_email'])) {								// success
					redirect('/');

				} else {
					$errors = $this->tank_auth->get_error_message();
					if (isset($errors['banned'])) {								// banned user
						$this->_show_message($this->lang->line('auth_message_banned').' '.$errors['banned']);

					} else {													// fail
						foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
					}
				}
			}

			if (count($data['errors']) > 0)
				$data['login_form'] = 1;

			$this->load->view('login', $data);
		}
		else {
			$this->load->view('login');
		}
	}

	function logout() {
		$this->tank_auth->logout();

		redirect('/');
	}

	function register() {
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('/');
		} else if ($this->input->post('form') == 'register') {
			$this->load->library('form_validation');

			$use_username = $this->config->item('use_username', 'tank_auth');
			if ($use_username) {
				$this->form_validation->set_rules('register_username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash|is_unique[users.username]');
			}
			$this->form_validation->set_rules('register_email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('register_password', 'Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('register_phone', 'Phone', 'trim|xss_clean|alpha_dash|numeric');

			$data['errors'] = array();

			$email_activation = $this->config->item('email_activation', 'tank_auth');

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->create_user(
						$use_username ? $this->form_validation->set_value('register_username') : '',
						$this->form_validation->set_value('register_email'),
						$this->form_validation->set_value('register_password'),
						$email_activation))) {									// success

					$names = explode(' ', $this->input->post('register_fullname'));
					if (count($names) > 1) {
						$data_user['NamaBelakang'] = array_pop($names);
						$data_user['Nama'] = implode(' ',$names);
					} else {
						$data_user['Nama'] = $names[0];
						$data_user['NamaBelakang'] = $names[0];
					}
					$data_user['HP'] = $this->input->post('register_phone');
					$key_user['id'] = $data['user_id'];
					$this->m_base->update('users',$data_user,$key_user);

					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					//if ($email_activation) {									// send "activate" email
					//	$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

					//	$this->_send_email('activate', $data['email'], $data);

					//	unset($data['password']); // Clear password (just for any case)

					//	$this->_show_message($this->lang->line('auth_message_registration_completed_1'));
					//} else {
						if ($this->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email

							$this->_send_email('welcome', $data['email'], $data);
						}

						$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
								$this->config->item('use_username', 'tank_auth'));
						$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

						if ($this->tank_auth->login(
								$this->form_validation->set_value('register_email'),
								$this->form_validation->set_value('register_password'),
								0,
								$data['login_by_username'],
								$data['login_by_email'])) {								// success

							unset($data['password']); // Clear password (just for any case)

							redirect('/');
						} else {
							unset($data['password']); // Clear password (just for any case)
						}

					//	$this->_show_message($this->lang->line('auth_message_registration_completed_2').' '.anchor('/auth/login/', 'Login'));
					//}
				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
				if (count($data['errors']) == 0) {
					redirect('/');
				}
			} else {
				//$this->session->set_flashdata('message','No Match PIN Found!');

				$data['register_form'] = 1;

				$this->load->view('login',$data);
			}
		} else {
			show_404();
		}
	}
	
	function account() {
		$this->load->model('m_base');
		$this->load->library('form_validation');

		if ($this->tank_auth->is_logged_in()) {
			$key_user['id']=$this->tank_auth->get_user_id();
			$users=$this->m_base->getSelectedData('users',$key_user)[0];

			$key_order['UserID'] = $this->tank_auth->get_user_id();
			$orders = $this->m_base->getSelectedData('orders',$key_order,'Tanggal desc');

			$data = array();
			$data['user'] = $users;
			$data['orders'] = $orders;
		} else if ($this->input->post('form') == 'edit') {
			$this->form_validation->set_rules('Nama', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('NamaBelakang', 'Username', 'trim|required|xss_clean');
			$use_username = $this->config->item('use_username', 'tank_auth');
			if ($use_username) {
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash|is_unique[users.username]');
			}
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('HP', 'Phone', 'trim|xss_clean|alpha_dash|numeric');

			if ($this->form_validation->run()) {
				$user_data = array();
				$user_data['Nama'] = $this->form_validation->set_value('Nama');
				$user_data['NamaBelakang'] = $this->form_validation->set_value('NamaBelakang');
				$user_data['username'] = $this->form_validation->set_value('username');
				$user_data['email'] = $this->form_validation->set_value('email');
				$user_data['HP'] = $this->form_validation->set_value('HP');
				$user_key['id'] = $this->tank_auth->get_user_id();
				if ($this->m_base->update('users',$user_data,$user_key)) {
					$this->session->set_flashdata('message', 'Your detail account has been successfully changed.');
					redirect('hello/account');
				}
			}
		} else if ($this->input->post('form') == 'change') {
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->change_password(
						$this->form_validation->set_value('old_password'),
						$this->form_validation->set_value('new_password'))) {	// success
					$this->session->set_flashdata('message', $this->lang->line('auth_message_password_changed'));
					redirect('hello/account');
				} else {														// fail
					$data['errors'] = array();

					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
			}
		} else {
			show_404();
		}
		$this->load->js($this->config->base_url().'assets/js/account.js');
		$this->load->view('user/account', $data);
	}

	function view_order() {
		$this->load->library('uri');
		$order_url  = $this->uri->segment(4);
		$order_key['Order_Url'] = $order_url;

		$orders = $this->m_base->getSelectedData('orders', $order_key);

		if (!$orders) {
			show_404();
		} else {
			$order = $orders[0];
			$shipments = $this->m_shipment->getSelectedShipment($order['ID']);

			$data['order'] = $order;
			if ($shipments) {
				$data['shipments'] = $shipments;
			}

			$this->load->view('view_order', $data);
		}
	}

	function contact_us() {
		$this->load->library('form_validation');
		$mail['name']=$this->input->post('name');
		$mail['email']=$this->input->post('email');
		$mail['phone']=$this->input->post('phone');
		$mail['message']=$this->input->post('message');

		if ($this->form_validation->run() == FALSE) {
			$message = "From ".$mail['name']."<br />";
			$message.= "Phone ".$mail['phone']."<br />";
			$message.= nl2br($mail['message']);

			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('noreply@parselday.com', 'ParselDay');
			$this->email->to($mail['email']);
			$this->email->subject('Contact From '.$mail['name']);
			$this->email->message($message);
			$this->email->send();
			//echo $this->email->print_debugger();
		} else {
			
		}
	}

	function about_us() {
		$this->load->view('about_us');
	}

	function contact() {
		$this->output->unset_template();
		
		$print = '';

		if ($this->tank_auth->is_logged_in()) {
			$term = trim($this->input->get('term'));
			$where_clause = array();
			$where_clause['UserID'] = $this->tank_auth->get_user_id();

			$print = json_encode($this->m_base->autocomplete('kontak', $term, 'id, nama as label, nama as value, Lat as lat, Lon as lng, Alamat as address, Kelurahan as village, Kecamatan as sub_district, Kota as city, Provinsi as province, Negara as country, Zip as zip, Kontak as name, Telpon as phone, Email as email', $where_clause));
		} else {
			$print = json_encode(array());
		}
		
		$data['print']=$print;
		$this->load->view('data', $data);
	}

	/**
	 *
	 *	INSERT OR UPDATE USER's CONTACT on CREATE SHIPMENT
	 *
	 */

	function insert_update_contact($name, $user_id, $prefix, $data) {
		$contact['Nama'] = $data[$prefix.'contact'];
		$contact['Alamat'] = $data[$prefix.'address'];
		$contact['Kelurahan'] = $data[$prefix.'village'];
		$contact['Kecamatan'] = $data[$prefix.'sub_district'];
		$contact['Kota'] = $data[$prefix.'city'];
		$contact['Provinsi'] = $data[$prefix.'province'];
		$contact['Negara'] = $data[$prefix.'country'];
		$contact['Zip'] = $data[$prefix.'zip'];
		$contact['Lat'] = $data[$prefix.'lat'];
		$contact['Lon'] = $data[$prefix.'lng'];
		$contact['Kontak'] = $data[$prefix.'name'];
		$contact['Telpon'] = $data[$prefix.'phone'];
		$contact['Email'] = $data[$prefix.'email'];
		$contact['UserID'] = $user_id;

		$where_clause = array();
		$where_clause['UserID'] = $user_id;
		$where_clause['Nama'] = $name;

		$my_contacts = $this->m_base->select('kontak', 'ID', $where_clause);

		if (count($my_contacts) > 0) {
			$this->m_base->update('kontak', $contact, $my_contacts[0]);
		} else {
			$this->m_base->insert('kontak', $contact);
		}
	}

	/**
	 *
	 * SHOPPING CART
	 *
	 */

	function cart_summary() {
		$this->output->unset_template();
		$this->load->view('cart/summary');
	}

	function cart(){
		$this->load->css($this->config->base_url()."assets/css/jquery.dataTables.min.css");
		$this->load->css($this->config->base_url()."assets/css/mystyle.css");
		
		$this->load->js($this->config->base_url()."assets/js/jquery.dataTables.min.js");
		$this->load->js($this->config->base_url()."assets/js/custom/cek_order.js");
		$this->load->js($this->config->base_url()."assets/js/jquery.validate.min.js");
		// $this->load->view('cart/complete');	
		$this->load->view('multidrop/reviews');	
	}

	function emptyCart(){
		$this->cart->destroy();
	}

	function removeItems(){
		$data = array(
               'rowid' => $this->input->post('rowid'),
               'qty'   => 0
        );
        $this->cart->update($data);
        $this->output->unset_template();

        $data = array(
        	'total_items' => $this->cart->total_items(),
        	'total' => number_format($this->cart->total(),0,',','.'));
			
		$data['print']=json_encode($data);
		$this->load->view('data', $data);


        // echo json_encode($data);
	}

	function create_order(){
		header('Content-Type: application/json');
		#Shipment Date
		$additional_data['shipment_date']=$this->input->post('shipment_date');
		
		//origin data
		$additional_data['origin_lat']=$this->input->post('origin_lat');
		$additional_data['origin_lng']=$this->input->post('origin_lng');
		$additional_data['origin_address']=$this->input->post('origin_address');
		$additional_data['origin_village']=$this->input->post('origin_village');
		$additional_data['origin_city']=$this->input->post('origin_city');
		$additional_data['origin_country']=$this->input->post('origin_country');
		$additional_data['origin_sub_district']=$this->input->post('origin_sub_district');
		$additional_data['origin_province']=$this->input->post('origin_province');
		$additional_data['origin_zip']=$this->input->post('origin_zip');
		$additional_data['origin_name']=$this->input->post('origin_name');
		$additional_data['origin_phone']=$this->input->post('origin_phone');
		$additional_data['origin_email']=$this->input->post('origin_email');
		
		//destination data
		$additional_data['destination_lat']=$this->input->post('destination_lat');
		$additional_data['destination_lng']=$this->input->post('destination_lng');
		$additional_data['destination_address']=$this->input->post('destination_address');
		$additional_data['destination_village']=$this->input->post('destination_village');
		$additional_data['destination_city']=$this->input->post('destination_city');
		$additional_data['destination_country']=$this->input->post('destination_country');
		$additional_data['destination_sub_district']=$this->input->post('destination_sub_district');
		$additional_data['destination_province']=$this->input->post('destination_province');
		$additional_data['destination_zip']=$this->input->post('destination_zip');
		$additional_data['destination_name']=$this->input->post('destination_name');
		$additional_data['destination_phone']=$this->input->post('destination_phone');
		$additional_data['destination_email']=$this->input->post('destination_email');

		//items data
		$additional_data['item_name']=$this->input->post('item_name');
		$additional_data['item_lenght']=$this->input->post('item_lenght');
		$additional_data['item_width']=$this->input->post('item_width');
		$additional_data['item_height']=$this->input->post('item_height');
		$additional_data['item_weight']=$this->input->post('item_weight');
		$additional_data['notes']=$this->input->post('notes');
		$additional_data['distance']=$this->input->post('distance');

		if ($this->tank_auth->is_logged_in()) {
			$user_id = $this->tank_auth->get_user_id();
			$additional_data['origin_contact']=$this->input->post('origin_contact');
			$additional_data['destination_contact']=$this->input->post('destination_contact');
			
			if (trim($this->input->post('origin_contact')) != '') {
				$this->insert_update_contact(trim($this->input->post('origin_contact')), $user_id, 'origin_', $additional_data);
			}
			if (trim($this->input->post('destination_contact')) != '') {
				$this->insert_update_contact(trim($this->input->post('destination_contact')), $user_id, 'destination_', $additional_data);
			}
		} else {
			$additional_data['origin_contact']='';
			$additional_data['destination_contact']='';
		}

		//required data for cart
		if ($this->cart->total_items()>0){
			$cart_id = end($this->cart->contents())['id']+1;
		}
		else{
			$cart_id = 1;
		}

		$data = array(
               'id'      => $cart_id,
               'qty'     => 1,
               'price'   => $this->input->post('cost'),
               'name'    => 'From '.$additional_data['origin_sub_district'].' to '.$additional_data['destination_sub_district'],
               'options' => $additional_data
            );

		if ($this->cart->insert($data)) {
			$this->output->unset_template();
			$data['print']=json_encode($this->cart->total_items());
			$this->load->view('data', $data);
			// echo json_encode($this->cart->total_items());
		}
	}

	function confirmCart(){
		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			#CHECK IF order_id ISSET: Untuk handle kembalian dari unfinished veritrans
			#Regenerate cart?
			$var = array();
			$var['order'] = array();

			if ($order_id = $this->input->get('order_id')) {
				$key_order['OrderID'] = $order_id;
				
				$orders = $this->m_base->getSelectedData('orders',$key_order);
				
				if ($orders) {
					$orders = $orders[0];
					
					if ($orders['StatusID'] == -1) {
						$shipments = $this->m_shipment->getSelectedShipment($orders['ID']);
						foreach ($shipments as $shipment) {
							#Shipment Date
							$additional_data['shipment_date']=$this->input->post('shipment_date');
							
							//origin data
			    			$additional_data['origin_contact']=$shipment['Dari'];
							$additional_data['origin_lat']=$shipment['Dari_Lat'];
							$additional_data['origin_lng']=$shipment['Dari_Lon'];
							$additional_data['origin_address']=$shipment['Dari_Alamat'];
							$additional_data['origin_village']=$shipment['Dari_Kelurahan'];
							$additional_data['origin_city']=$shipment['Dari_Kota'];
							$additional_data['origin_country']=$shipment['Dari_Negara'];
							$additional_data['origin_sub_district']=$shipment['Dari_Kecamatan'];
							$additional_data['origin_province']=$shipment['Dari_Provinsi'];
							$additional_data['origin_zip']=$shipment['Dari_Zip'];
							$additional_data['origin_name']=$shipment['Dari_Kontak'];
							$additional_data['origin_phone']=$shipment['Dari_Telpon'];
							$additional_data['origin_email']=$shipment['Dari_Email'];
							
							//destination data
			    			$additional_data['destination_contact']=$shipment['Untuk'];
							$additional_data['destination_lat']=$shipment['Untuk_Lat'];
							$additional_data['destination_lng']=$shipment['Untuk_Lon'];
							$additional_data['destination_address']=$shipment['Untuk_Alamat'];
							$additional_data['destination_village']=$shipment['Untuk_Kelurahan'];
							$additional_data['destination_city']=$shipment['Untuk_Kota'];
							$additional_data['destination_country']=$shipment['Untuk_Negara'];
							$additional_data['destination_sub_district']=$shipment['Untuk_Kecamatan'];
							$additional_data['destination_province']=$shipment['Untuk_Provinsi'];
							$additional_data['destination_zip']=$shipment['Untuk_Zip'];
							$additional_data['destination_name']=$shipment['Untuk_Kontak'];
							$additional_data['destination_phone']=$shipment['Untuk_Telpon'];
							$additional_data['destination_email']=$shipment['Untuk_Email'];

							//items data
							$additional_data['item_name']=$shipment['Barang'];
							$additional_data['item_lenght']=$shipment['Panjang'];
							$additional_data['item_width']=$shipment['Lebar'];
							$additional_data['item_height']=$shipment['Tinggi'];
							$additional_data['item_weight']=$shipment['Berat'];
							$additional_data['notes']=$shipment['Catatan'];
							$additional_data['distance']=$shipment['Jarak'];

							//required data for cart
							if ($this->cart->total_items()>0){
								$cart_id = end($this->cart->contents())['id']+1;
							}
							else{
								$cart_id = 1;
							}
							$data = array(
				               'id'      => $cart_id,
				               'qty'     => 1,
				               'price'   => $shipment['Harga'],
				               'name'    => 'From '.$additional_data['origin_sub_district'].' to '.$additional_data['destination_sub_district'],
				               'options' => $additional_data
				            );

							$this->cart->insert($data);
						}
						#PASSING ORDER DATA
						$var['order_id'] = $order_id;

						$var['order']['first_name'] = $orders['First_Name'];
						$var['order']['last_name'] = $orders['Last_Name'];
						$var['order']['phone'] = $orders['Phone'];
						$var['order']['email'] = $orders['Email_Confirmation'];
						$var['order']['address'] = $orders['Address'];
						$var['order']['city'] = $orders['City'];
						$var['order']['country'] = $orders['Country'];
						$var['order']['province'] = $orders['Province'];
						$var['order']['zip'] = $orders['Zip'];

						#DELETE FROM DATABASE
						$this->m_shipment->deleteOrder($orders['ID']);
					}
				}
			}

			#IF order_id IS NOT SET: Show yg ada di cart
			#TODO: CART CHECKING
			if ($this->cart->total_items()>0){
				$var['users']= array();
				if ($this->tank_auth->is_logged_in()) {
					$key['id']=$this->tank_auth->get_user_id();
					$var['users']=$this->m_base->getSelectedData('users',$key)[0];
				}
				
				$this->load->css($this->config->base_url()."assets/css/mystyle.css");
				$this->load->js($this->config->base_url().'assets/js/confirm_cart.js');
				$this->load->view('multidrop/confirm_cart',$var);
			} else {
				$this->load->view('cart/empty');
			}
		}
		
		else if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->load->library('veritrans');

			#CHECK MANDATORY FIELDS
			if ($this->cart->total_items()>0){
	        	$orders['Total_Items']=$this->cart->total_items();
		        $orders['Tanggal']=date('Y-m-d H:i:s');
		        $orders['First_Name'] = $this->input->post('first_name');
		        $orders['Last_Name'] = $this->input->post('last_name');
		        $orders['Phone'] = $this->input->post('phone');
		        $orders['Address'] = $this->input->post('address');
		        $orders['City'] = $this->input->post('city');
		        $orders['Province'] = $this->input->post('province');
		        $orders['Country'] = $this->input->post('country');
		        $orders['Zip'] = $this->input->post('zip');
		        $orders['Payment_Method'] = $this->input->post('payment_method');
		        $orders['Amount_Total'] = $this->cart->total();
		        $orders['Email_Confirmation'] = $this->input->post('email');
		        $orders['StatusID'] = -1;
				
		        if ($this->input->post('payment_method') == 1) { // TRANSFER
			        $orders['StatusID'] = 0;
		        }
				else if ($this->input->post('payment_method') == 2) { // PIN
			        if ($this->tank_auth->is_logged_in()) {
			        	$corp_key['UserID']=$this->tank_auth->get_user_id();
			        	$corp_pin = $this->m_base->getSelectedData('corp_pin',$corp_key);

			        	if (!$corp_pin) {
							$this->session->set_flashdata('message','No Match PIN Found!');

			        		redirect('hello/confirmCart','refresh');
							return;
			        	}
						else {
			        		if ($this->input->post('corp_pin') == $corp_pin[0]['pin']) {
			        			$orders['StatusID'] = 3;
			        			$orders['Tanggal_Payment'] = date('Y-m-d H:i:s');
			        			$orders['Tanggal_Confirm'] = date('Y-m-d H:i:s');
								
								/* KONFIRMASI EMAIL CORP_PIN */
								$datas['order'] = $orders;
								$datas['shipments'] = $this->cart->contents();

								$this->_send_email('confirm_order_for_pin',$orders['Email_Confirmation'], $datas);
			        		} else {
								$this->session->set_flashdata('message','No Match PIN Found!');

				        		redirect('hello/confirmCart','refresh');
								return;
			        		}
			        	}
			        }
					else {
						$this->session->set_flashdata('message','No Match PIN Found!');

		        		redirect('hello/confirmCart','refresh');
						return;
			        }
		        }
				
		        $orders['OrderID'] = $this->m_order->getOrderId();
		        $orders['Order_Url'] = $this->m_order->encryptLink(date('Y-m-d H:i:s'));
				
		        if ($this->tank_auth->is_logged_in()) {
		        	$orders['UserID'] = $this->tank_auth->get_user_id();
		        }

		        if($orders['ID'] = $this->m_base->insert('orders',$orders)){
		        	$data['OrderID'] = $orders['ID'];
					
					// 151005 ADE NUGROHO WIDJAJA					
					$this->load->library('general');
					
		        	foreach ($this->cart->contents() as $items){
						// $lat=$items['options']['origin_lat'];
						// $lon=$items['options']['origin_lng'];
						
						// $location = $this->general->gpsLocationName($lat, $lon);
						
						// $kota = $location['city'];
						
						// $area = $this->m_shipment->getAreaID($kota);
						
						// if ($area) {
							// $data['area'] .=  $area['ID'];
						// }
						
		        		$data['Tanggal']=date('Y-m-d H:i:s');
			    		$data['Shipment']=$this->m_shipment->getShipmentId();
		    			// $data['Dari']=$items['options']['origin_contact'];
		    			// $data['Dari_Alamat']=$items['options']['origin_address'];
		    			// $data['Dari_Kelurahan']=$items['options']['origin_village'];
		    			// $data['Dari_Kecamatan']=$items['options']['origin_sub_district'];
		    			// $data['Dari_Kota']=$items['options']['origin_city'];
		    			// $data['Dari_Provinsi']=$items['options']['origin_province'];
		    			// $data['Dari_Negara']=$items['options']['origin_country'];
		    			// $data['Dari_Zip']=$items['options']['origin_zip'];
		    			// $data['Dari_Lat']=$items['options']['origin_lat'];
		    			// $data['Dari_Lon']=$items['options']['origin_lng'];
		    			// $data['Dari_Kontak']=$items['options']['origin_name'];
		    			// $data['Dari_Telpon']=$items['options']['origin_phone'];
		    			// $data['Dari_Email']=$items['options']['origin_email'];

		    			// $data['Untuk']=$items['options']['destination_contact'];
		    			// $data['Untuk_Alamat']=$items['options']['destination_address'];
		    			// $data['Untuk_Kelurahan']=$items['options']['destination_village'];
		    			// $data['Untuk_Kecamatan']=$items['options']['destination_sub_district'];
		    			// $data['Untuk_Kota']=$items['options']['destination_city'];
		    			// $data['Untuk_Provinsi']=$items['options']['destination_province'];
		    			// $data['Untuk_Negara']=$items['options']['destination_country'];
		    			// $data['Untuk_Zip']=$items['options']['destination_zip'];
		    			// $data['Untuk_Lat']=$items['options']['destination_lat'];
		    			// $data['Untuk_Lon']=$items['options']['destination_lng'];
		    			// $data['Untuk_Kontak']=$items['options']['destination_name'];
		    			// $data['Untuk_Telpon']=$items['options']['destination_phone'];
		    			// $data['Untuk_Email']=$items['options']['destination_email'];

		    			// $data['Barang']=$items['options']['item_name'];
		    			// $data['Panjang']=$items['options']['item_lenght'];
		    			// $data['Lebar']=$items['options']['item_width'];
		    			// $data['Tinggi']=$items['options']['item_height'];
		    			// $data['Berat']=$items['options']['item_weight'];
		    			// $data['Jarak']=$items['options']['distance'];
		    			// $data['Catatan']=$items['options']['notes'];
						
		    			// if ($this->input->post('payment_method') == 2) { // PIN
			    			// $data['Status'] = 1;
			    		// }
						// else {
			    			// $data['Status'] = 0;
			    		// }
						
		    			// $data['track_url'] = $this->m_shipment->encryptLinkTrack($data['Shipment']);
			    		$data['Harga']=$items['subtotal'];
						
			    		// $data['vendor_price']=$items['subtotal'] * 0.8;
						// $pv = intval($data['Jarak']/1000)*1000;
			    		// $data['vendor_price']=($pv<15000?15000:($pv>40000?40000:$pv));
						
				        if ($this->tank_auth->is_logged_in()) {
				        	$data['UserID'] = $this->tank_auth->get_user_id();
				        }
						
			    		$titik['shipment_id'] = $this->m_base->insert('shipment',$data);
						
						foreach ($items['options'] as $row){
							$titik['lat'] = $row[1];
							$titik['lng'] = $row[2];
							
							$titik['titik'] = $row[3];
							
							$titik['kontak'] = $row[4];
							$titik['alamat'] = $row[5];
							$titik['telpon'] = $row[6];
							$titik['email'] = $row[7];
							
							$titik['barang'] = $row[9];
							
							$this->m_base->insert('titik_lokasi',$titik);
						}
		    		}

			    	$this->cart->destroy();
		        }
				
		        //update info registered user
		        if ($this->tank_auth->is_logged_in()) {
		        	$key['id']=$this->tank_auth->get_user_id();
		        	#TODO: Tambahin info lengkap address
		        	$users['Alamat'] = $orders['Address'];
		        	$users['Kota'] = $orders['City'];
		        	$users['Provinsi'] = $orders['Province'];
		        	$users['Negara'] = $orders['Country'];
		        	$users['Zip'] = $orders['Zip'];
		        	$this->m_base->update('users',$users,$key);
		        }
    		}
			
			#CREATE ORDER
			$order_url = $orders['Order_Url'];
			$order_id = $orders['OrderID'];

			if ($this->input->post('create_account')) {
				$this->tank_auth->create_user(
						$this->input->post('username'),
						$this->input->post('email'),
						$this->input->post('password'),
						$this->input->post('email'));
			}
			
			// VERITRANS
			if ($this->input->post('payment_method') == 3) {
				$this->output->unset_template();

				$transaction_details = array(
					'order_id' => $order_id,
					'gross_amount' => $orders['Amount_Total'],
				);
				$items_details = array();
				$no = 0;

				$shipments = $this->m_shipment->getSelectedShipment($orders['ID']);
				foreach ($shipments as $shipment) {
					$no++;
					$billing_item = array(
						'id' => $shipment['Shipment'],
						'price' => $shipment['Harga'],
						'quantity' => 1,
						'name' => 'From '.$shipment['Dari_Kecamatan'].' to '.$shipment['Untuk_Kecamatan'],
					);
					array_push($items_details, $billing_item);
				}
				$billing_address = array(
					'first_name' => $orders['First_Name'],
					'last_name' => $orders['Last_Name'],
					'address' => $orders['Address'],
					'city' => $orders['City'],
					'postal_code' => $orders['Zip'],
					'phone' => $orders['Phone'],
					'country_code' => 'IDN',
				);
				$customer_details = array(
					'first_name' => $orders['First_Name'],
					'last_name' => $orders['Last_Name'],
					'email' => $orders['Email_Confirmation'],
					'phone' => $orders['Phone'],
					'billing_address' => $billing_address,
				);
				$transaction = array(
					'payment_type' => 'vtweb',
					'vtweb' => array(
						'credit_card_3d_secure' => true,
						'finish_redirect_url' => $this->config->base_url()."hello/paymentFinish/".$orders['Order_Url'],
					),
					'transaction_details' => $transaction_details,
					'customer_details' => $customer_details,
					'items_details' => $items_details,
				);
				
				$redirect_url = $this->veritrans->getRedirectionUrl($transaction);

				redirect($redirect_url, 'refresh');
				return;
			}
			
			$data['order'] = $orders;
			$data['shipments'] = $this->m_shipment->getSelectedShipment($data['OrderID']);
			$this->session->set_flashdata('orders', $orders);
			$this->session->set_flashdata('shipments', $data['shipments']);
			
			// TRANSFER
			if ($this->input->post('payment_method') == 1) {
		    	$this->_send_email('confirm_order',$orders['Email_Confirmation'],$data);

				redirect('hello/page/thankyouco','refresh');
			}
			
			// PIN
			else if ($this->input->post('payment_method') == 2) {
				redirect('hello/page/thankyouconfirm','refresh');
			}
			
			else {
				show_404();
			}
		}
	}

	/**
	 *
	 * VeriTrans Handler
	 *
	 */

	function paymentFinish(){
		$this->load->library('uri');
		$order_url  = $this->uri->segment(4);

		$veritrans['order_id'] = $this->input->get('order_id');
		$veritrans['status_code'] = $this->input->get('status_code');
		$veritrans['transaction_status'] = $this->input->get('transaction_status');
		$key['orderID'] = $veritrans['order_id'];
		$key['order_url'] = $order_url;
		$orders = $this->m_base->getSelectedData('orders',$key);
		if (!$orders) {
			show_404();
			return;
		} else {
			$order = $orders[0];
			if ($veritrans['status_code'] == '200' || $veritrans['status_code'] == '201') {
				## ORDER STATUS CHANGED TO CONFIRMED ##
				$fields['StatusID'] = 2;
				$fields['Tanggal_Payment'] = date('Y-m-d H:i:s');
				$fields['Tanggal_Confirm'] = date('Y-m-d H:i:s');
				$this->m_base->update('orders',$fields,$key);

				## SHIPMENT STATUS CHANGED TO READY ##
				$fields_shipment['Status']=1;
				$key_shipment['OrderID']=$order['ID'];
				$this->m_base->update('shipment',$fields_shipment,$key_shipment);
				
				$this->session->set_flashdata('orders',$order);
				$this->session->set_flashdata('shipments', $this->m_shipment->getSelectedShipment($order['ID']));
				redirect('hello/page/thankyouconfirm','refresh');
				
				/* KONFIRMASI EMAIL VERITRANS */
				$data['order'] = $orders;
				$data['shipments'] = $this->cart->contents();
				$this->_send_email('cc_confirmed_payment', $orders['Email_Confirmation'],$data);

			} else if ($veritrans['status_code'] == '202') {
				header('Location: '.$this->config->base_url().'hello/confirmCart?order_id='.$veritrans['order_id']);
			} else {
				redirect('hello/page/transaction_error','refresh');
			}
		}
	}

	function confirm_order(){
		$order = $this->input->get('order');
		$key['Order_Url'] = $order;

		$data_order = $this->m_base->getSelectedData('orders',$key);
		if (!$data_order) {
			show_404();
			return;
		} else {
			$data_order = $data_order[0];
			if ($data_order['StatusID']==0){
				if($data['shipments'] = $this->m_shipment->getSelectedShipment($data_order['ID'])){
					$data['order_id'] = $data_order['ID'];
					$this->load->view('payment',$data);
				}
				else{
					show_404();
				}
			}
			elseif ($data_order['StatusID']==1){
				$data['print']="your confirmation has been sent, please wait for our confirmation";
				$this->load->view('data', $data);

				// echo "your confirmation has been sent, please wait for our confirmation";
			}
		}
	}

	function confirmPayment(){
		$data['Metode_Bayar'] = $this->input->post('payment_method');
		$data['Nama_Rekening'] = $this->input->post('account_holder');
		$data['Nomor_Rekening'] = $this->input->post('account_number');
		$data['Bank_Pembayar'] = $this->input->post('bank');
		$data['Jumlah_Bayar'] = $this->input->post('paid_amount');
		$data['OrderID'] = $this->input->post('order_id');

		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
		$config['file_name'] = $data['OrderID'];

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('payment_slip')){
        	$file = $this->upload->data();
        	$data['Bukti_Bayar'] = $file['file_name'];
        }
		
		if ($this->m_base->insert('payment',$data)){
			$order['StatusID'] = 1;
			$order['Tanggal_Payment'] = date('Y-m-d H:i:s');
			$key['id'] = $data['OrderID'];

			if ($this->m_base->update('orders',$order,$key)) {
				$this->load->view('thankyouconfirm');
			}
			else{
				show_404();
			}
		}
		else {
			show_404();
		}
	}

	function track() {
		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			$this->load->library('uri');
			$shipment_url  = $this->uri->segment(3);

			// $this->load->model('m_shipment');

			if ($shipment_url) {
				$shipment = $this->m_shipment->getTracking($shipment_url);

				if (!$shipment) {
					show_404();
				} else {
					$data['shipment_id'] = $shipment['Shipment'];
					$data['shipment'] = $shipment;
					$this->load->view('track/parsel',$data);
				}
			}

		}
		else if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$shipment_id = $this->input->post('shipment_id');
			// $this->load->model('m_shipment');
			$shipment = array();

			if ($shipment_id) {
				$shipment = $this->m_shipment->getTrackUrl($shipment_id);

				$data['shipment_id'] = $shipment_id;

				if (!$shipment) {
					show_404();
				} else {
					$data['shipment_id'] = $shipment['Shipment'];
					$data['shipment'] = $shipment;
					$this->load->view('track/parsel',$data);
				}
			}
			else {
				redirect('/');
			}
		}
		else {
			redirect('/');
		}
	}

	function _getMessage($order_id,$type,$url){
		$shipment = $this->m_shipment->getSelectedShipment($order_id);
		$message = '';
		if ($type=='confirm'){
			$no = 1;
			$amount_total = 0;
			$message = "<table><th>#</th><th>Item</th><th>Route</th><th>Cost</th>";
			foreach ($shipment as $ship) {
				$message.= "<tr>";
	        	$message.="<td>".$no."</td>";
	        	$message.= "<td>".$ship['Barang']."</td>";
	        	$message.= "<td>From ".$ship['Dari_Kecamatan']." to ".$ship['Untuk_Kecamatan']."</td>";
	        	$message.= "<td>".$ship['Harga']."</td>";
	        	$message.= "</tr>";
	            $amount_total+=$ship['Harga'];
	        	$no++;
			}
			$message.= "<tr><td colspan='5'>".$amount_total."</td><tr>";
	        $message.= "</table>";
	        $message.="Please click this <a href=".$this->config->base_url()."hello/confirm_order?order=".$url.">Link<a> to confirm your payment";
		}
		elseif ($type=='approve') {
			$no = 1;
			$amount_total = 0;
			$message = "<table><th>#</th><th>From</th><th>To</th><th>Cost</th>";
			foreach ($shipment as $ship) {
				$message.= "<tr>";
	        	$message.="<td>".$no."</td>";
	        	$message.= "<td>".$ship['Barang']."</td>";
	        	$message.= "<td>From ".$ship['Dari_Kecamatan']." to ".$ship['Untuk_Kecamatan']."</td>";
	        	$message.= "<td>".$ship['Harga']."</td>";
	        	$message.= "</tr>";
	            $amount_total+=$ship['Harga'];
	        	$no++;
			}
			$message.= "<tr><td colspan='5'>".$amount_total."</td><tr>";
	        $message.= "</table>";
	        $message.="Please click this <a href=".$this->config->base_url()."hello/confirm_order?order=".$url.">Link<a> to confirm your payment";
		}
		return $message;
	}

	/**
	 * Send email message of given type (activate, forgot_password, etc.)
	 *
	 * @param	string
	 * @param	string
	 * @param	array
	 * @return	void
	 */
	function _send_email($type, $email, &$data)
	{
		$this->load->library('email');
		//$config['protocol'] = 'mail';
		//$config['mailtype'] = 'html';
		//$config['charset'] = 'utf-8';
		//$config['wordwrap'] = TRUE;
		
		//$this->email->initialize($config);
		// Update by ANW 2015-06-22
		$this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		//$this->email->to($email);
		$this->email->to(array($email));
		
		// if ($type == 'welcome')
		$this->email->bcc($this->config->item('webmaster_email', 'tank_auth') . ', pejantanseo@gmail.com, peter.wijaya@imslogistics.com, sugianto@imslogistics.com');
		
		$this->email->subject(sprintf($this->lang->line('email_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
		$this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
		$this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
		$this->email->send();

		//echo $this->email->print_debugger();
	}

	function getAltAddress(){
		$source = $this->input->post('source');
		$data = array();
		$data['first_name'] = '';
		$data['last_name'] = '';
		$data['phone'] = '';
		$data['email'] = '';
		$data['address'] = '';
		$data['city'] = '';
		$data['country'] = '';
		$data['province'] = '';
		$data['zip'] = '';
		if ($source=='asdf') {
			if ($this->tank_auth->is_logged_in()) {
				$key['id'] = $this->tank_auth->get_user_id();
				$user = $this->m_base->getSelectedData('users',$key);
				if (!$user) {
				} else {
					$user = $user[0];
					$data['first_name'] = isset($user['Nama']) ? $user['Nama'] : '';
					$data['last_name'] = isset($user['NamaBelakang']) ? $user['NamaBelakang'] : '';
					$data['phone'] = isset($user['HP']) ? $user['HP'] : '';
					$data['email'] = isset($user['email']) ? $user['email'] : '';
					$data['address'] = isset($user['Alamat']) ? $user['Alamat'] : '';
					$data['city'] = isset($user['Kota']) ? $user['Kota'] : '';
					$data['country'] = isset($user['Negara']) ? $user['Negara'] : '';
					$data['province'] = isset($user['Provinsi']) ? $user['Provinsi'] : '';
					$data['zip'] = isset($user['Zip']) ? $user['Zip'] : '';
				}
			}
		} else if ($source=='sender' || $source=='recipient') {
			foreach ($this->cart->contents() as $items){
				if ($source=='sender'){
					$prefix = 'origin_';
				} elseif ($source=='recipient') {
					$prefix = 'destination_';
				}
				$names = explode(' ', $items['options'][$prefix.'name']);
				if (count($names) > 1) {
					$data['last_name'] = array_pop($names);
					$data['first_name'] = implode(' ',$names);
				} else {
					$data['first_name'] = $names[0];
					$data['last_name'] = $names[0];
				}
				$data['phone'] = $items['options'][$prefix.'phone'];
				$data['email'] = $items['options'][$prefix.'email'];
				$data['address'] = $items['options'][$prefix.'address'];
				$data['city'] = $items['options'][$prefix.'city'];
				$data['country'] = $items['options'][$prefix.'country'];
				$data['province'] = $items['options'][$prefix.'province'];
				$data['zip'] = $items['options'][$prefix.'zip'];
			}
		}
		$this->output->unset_template();
        // echo json_encode($data);
		
		$data['print']=json_encode($data);
		$this->load->view('data', $data);

	}

	function page(){
		$this->load->library('uri');
		$url  = $this->uri->segment(3);
		$this->load->view($url);
	}

	function confirm() {
		if ($this->tank_auth->is_logged_in() && $this->tank_auth->get_level() == 0) {
			if ($this->input->server('REQUEST_METHOD') == 'GET') {
				#FETCH DATA ORDER & PAYMENT
				//$where['StatusID'] = 1;
				$data['orders'] = $this->m_base->select('orders','*',array(),'StatusID = 1 OR Tanggal_Confirm = DATE(NOW())');
				$this->load->view('admin/orders', $data);
			} else if ($this->input->server('REQUEST_METHOD') == 'POST') {
				$order_id = $this->input->post('order_id');
				$key['ID'] = $order_id;
				$orders = $this->m_base->getSelectedData('orders',$key);
				if (!$orders) {
					$this->session->set_flashdata('message','No Orders Found!');
					redirect('hello/confirm','refresh');
					return;
				} else {
					$order = $orders[0];
					if ($order['StatusID'] == -1) {
						$this->session->set_flashdata('message','Feedback from VeriTrans Needed!');
						redirect('hello/confirm','refresh');
						return;
					} else if ($order['StatusID'] == 0) {
						$this->session->set_flashdata('message','Payment Confirmation Needed!');
						redirect('hello/confirm','refresh');
						return;
					} else if ($order['StatusID'] == 1) {
						$fields['StatusID'] = $order['StatusID'] + 1;
						$fields['Tanggal_Confirm'] = date('Y-m-d H:i:s');
						$this->m_base->update('orders',$fields,$key);
						## SHIPMENT STATUS CHANGED TO READY ##
						$fields_shipment['Status']=1;
						$key_shipment['OrderID']=$order['ID'];
						$this->m_base->update('shipment',$fields_shipment,$key_shipment);

						$data['order'] = $order;
						$data['shipments'] = $this->m_shipment->getSelectedShipment($order['ID']);
						$this->_send_email('order_confirmed',$order['Email_Confirmation'],$data);

						$this->session->set_flashdata('message','Order '.$order['OrderID'].' is Confirmed!');
						redirect('hello/confirm','refresh');
						return;
					} else if ($order['StatusID'] == 2) {
						$this->session->set_flashdata('message','Order '.$order['OrderID'].' has been Confirmed!');
						redirect('hello/confirm','refresh');
						return;
					} else {
						$this->session->set_flashdata('message','Order Status for '.$order['OrderID'].' is Unknown!');
						redirect('hello/confirm','refresh');
						return;
					}
				}
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	function delivered_notification() {
		$this->load->library('uri');
		$shipment_url  = $this->uri->segment(4);
		$shipment = $this->m_shipment->getShipment($shipment_url);

		if (!$shipment) {
			return 0;
		} else {
			$key['ID'] = $data['shipment']['OrderID'];
			$order = $this->m_base->getSelectedData('orders',$key);

			if (!$order) {
				return 0;
			} else {
				$this->_send_email('goods_delivered',$order['Email_Confirmation'],$data);

				return 1;
			}
		}
	}
	
	function calculate_multi() {
		header('Content-Type: application/json');
		
		$this->load->library('general');
		
		$this->output->unset_template();
		
		$multi = $this->input->post('multi');
		$data = json_decode($this->input->post('data'));
		$result = array();
		$ori = array();
		$des = array();
		
		$success = TRUE;
		$message = '';
		$point = 0;
		$distance = 0;
		$harga = 0;
		
		for($i=0; $i<sizeof($data); $i++) {
			$latlng = $data[$i][1] . ',' . $data[$i][2];
			
			if ($data[$i][3] == 'Asal') {
				array_push($ori, $latlng);
			}
			elseif ($data[$i][3] == 'Tujuan') {
				array_push($des, $latlng);
			}
		}
		
		if (sizeof($ori > 0) && sizeof($des) > 0) {
			$origins = join('|',$ori);
			$destinations = join('|',$des);
			
			$distance = round($this->general->multiDistance($ori, $des));
			
			if ($multi == 'false') { // one to many
				$point = sizeof($des);
				
				if (sizeof($ori) > 1) {
					$success = FALSE;
					$message = '\nAsal hanya boleh 1';
					$point = 0;
				}
			}
			else { // many to one
				$point = sizeof($ori);
				
				if (sizeof($des) > 1) {
					$success = FALSE;
					$message = '\nTujuan hanya boleh 1';
					$point = 0;
				}
			}
		}
		
		if ($point > 5) { // max 10 point
			$success = FALSE;
			$message .= '\nTitik berikutnya hanya boleh 5';
		}
		else {
			$harga = ($distance * 2000) + ($point * 5000);
		}
		
		$result['success'] = $success;
		$result['message'] = $message;
		$result['distance'] = $distance;
		$result['origins'] = $origins;
		$result['destinations'] = $destinations;
		$result['price'] = "Rp " . number_format($harga,0,',','.');
		
		$data['print']= json_encode($result);
		$this->load->view('data', $data);
	}

	function calculate_price(){
		header('Content-Type: application/json');

		//supaya viewnya ga ikut
		 $this->output->unset_template();

		$customer_id = $this->tank_auth->get_user_id();
		$result = 0;
		
		$origin_lat = (double)$this->input->post("origin_lat");
		$origin_lon = (double)$this->input->post("origin_lon");

		$destination_lat = (double)$this->input->post("destination_lat");
		$destination_lon = (double)$this->input->post("destination_lon");
		
		
		// KOTA DALAM AREA PARSELDAY
		
		$this->load->library('general');
		
		$loc1 = $this->general->gpsLocationName($origin_lat, $origin_lon);
		$loc2 = $this->general->gpsLocationName($destination_lat, $destination_lon);
		
		$kota = $loc1['city'];
		
		$area = $this->m_shipment->getAreaID($kota);
		
		if ($area) {
			$this->direction->set_origin($origin_lat, $origin_lon);
			$this->direction->set_destination($destination_lat, $destination_lon);
			$distance = $this->direction->get_distance()->getValue();
			
			$this->geocode->set_coordinate($origin_lat, $origin_lon);
			$sub_district_origin = $this->geocode->get_sub_district();
			
			$this->geocode->set_coordinate($destination_lat, $destination_lon);
			$sub_district_destination = $this->geocode->get_sub_district();

			// $this->direction->set_origin(-6.187554, 106.82887400000004);
			// $this->direction->set_destination(-6.262461, 107.15367400000002);
			// $distance = $this->direction->get_distance()->getValue();
			

			//get user price type config (string)
			$price_config = $this->price_method->price_config($customer_id);

			$price_type = $price_config['price_type'];
			$id_group = $price_config['id_group'];
		
			if($price_type == 1){
				$result = $this->price_method->flat_rate($id_group, $distance);
			}
			if($price_type == 2)
			{
				$result = $this->price_method->distance_rate($id_group, $distance);
			}
			if($price_type == 3){
				$result = $this->price_method->matrix_rate($id_group, $sub_district_origin, $sub_district_destination);
			}
			if($price_type == 4){
				$this->load->model('m_price');
				
				$area1 = $this->m_price->get_grup_area($loc1['city']);
				$area2 = $this->m_price->get_grup_area($loc2['city']);
				
				if ($area1 == $area2) {
					$distance = 0;
				}
				
				$price = $this->m_price->get_price_area($area1, $distance);
				
				if ($price == 0) {
					$result = "Shipment tidak dapat dibuat!";
				}
				else {
					$result = $price;
				}
			}
		}
		//KOTA DILUAR AREA PARSELDAY
		else {
			$result =  $kota . " belum masuk daftar";
		}
		
		$data['print']=json_encode($result);
		$this->load->view('data', $data);
	}

	public function test_function(){
		$this->output->unset_template();
		// $this->geocode->set_coordinate(-6.191322, 106.742527);
		// var_dump($this->geocode->get_sub_district());
		
		$this->load->model('m_price');
		$data['print']=$this->m_price->get_price_area(6, 20);

		$this->load->view('data', $data);
	}
}
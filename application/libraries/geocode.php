<?php 
use Ivory\GoogleMap\Services\Geocoding\Geocoder;
use Ivory\GoogleMap\Services\Geocoding\GeocoderRequest;
use Ivory\GoogleMap\Services\Geocoding\GeocoderProvider;
use Geocoder\HttpAdapter\CurlHttpAdapter;

class Geocode{

	private $geocoder;
	
	private $request;
	
	private $response;
	
	private $results;
	
	function __construct(){
	
		$this->geocoder = new Geocoder();
		$this->geocoder->registerProviders(array(
			new GeocoderProvider(new CurlHttpAdapter()),
		));
		
		$this->request = new GeocoderRequest();
	}
	
	private function get_result(){
		$this->response = $this->geocoder->geocode($this->request);
		$this->results = $this->response->getResults();
	}
	
	public function set_coordinate($lat, $lon){
		$this->request->setCoordinate($lat, $lon);
		$this->get_result();
	}

	public function get_sub_district(){
		
		foreach ($this->results as $result) {
			foreach ($result->getAddressComponents('administrative_area_level_3') as $addressComponent) {
				return $addressComponent->getLongName();
			}
		}
	}
}
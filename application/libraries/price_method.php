<?php

class Price_method{

	private $ci;
	
	private $cost;
	
	function __construct(){
		$this->ci =& get_instance();
		$this->ci->load->database();
		$this->ci->load->library("Tank_auth");
	}
	
	public function price_config($user_id){

		$data = array(
			"id_customer" => $user_id
		);

		$query;
		$config = array();

		if($user_id != "" || $user_id != null){

			$query = $this->ci->db->get_where("price_config", $data);

			if($query->num_rows > 0){

				$config = array(
					"price_type" => $query->row()->price_type,
					"id_group" => $query->row()->id_group
				);
				
			}
			else{
				$query = $this->ci->db->get_where("price_config", array("id_customer" => 0));
				$config = array(
					"price_type" => $query->row()->price_type,
					"id_group" => $query->row()->id_group
				);
			}
		}
		else{
			$query = $this->ci->db->get_where("price_config", array("id_customer" => 0));

			$config = array(
				"price_type" => $query->row()->price_type,
				"id_group" => $query->row()->id_group
			);
		}

		return $config;
	}

	public function distance_rate($id_group, $distance){
		$extra_distance = 0;
		$total_extra_price = 0;

		$data_id_group = array(
			"distance_group" => $id_group
		);

		try{

			$query = $this->ci->db->get_where("distance_price", $data_id_group)->row();

			if(!$query)
			{
				throw new Exception("Error request on distance rate");
			}
			
		} catch (Exception $e) {
			return $e->getMessage();
		}

		$min_distance = $query->start_m;
		$max_distance = $query->max_m;

		$min_price = $query->start_price;
		$max_price = $query->max_price;

		$extra_price_per_km = $query->km_price;

		$extra_distance = $distance - $min_distance;
		$total_extra_price = round($extra_distance/1000) * $extra_price_per_km;

		$this->cost = $min_price;

		if($distance > $max_distance && $max_distance != 0){
			$this->cost = "error_distance_rate";
		}
		else if(($this->cost + $total_extra_price) > $max_price && $max_price != 0){
			$this->cost = $max_price;
		}else if($total_extra_price > 0){			
			$this->cost = $min_price + $total_extra_price;
		}

		return $this->cost;
	}
	
	public function flat_rate($id_group, $distance){
		
		$data_id_group = array(
			"flat_group" => $id_group
		);

		try{

			$query = $this->ci->db->get_where("flat_price", $data_id_group)->row();

			if(!$query)
			{
				throw new Exception("Error request on flat rate");
			}
			
		} catch (Exception $e){
			return $e->getMessage();
		}
		
		$max_km = (int)$query->max_m;
		$price = (int)$query->price;
		
		$this->cost = $price;
		
		if($distance > $max_km){
			$this->cost = "error_flat_rate";
		}
		return $this->cost;
	}
	
	public function matrix_rate($id_group, $origin, $destination){
		
		$data_id_group = array(
			"matrix_group" => $id_group,
			"origin" => $origin,
			"destination" => $destination
		);

		try{

			$query = $this->ci->db->get_where("matrix_price", $data_id_group)->row();

			if(!$query)
			{
				throw new Exception("error_matrix_rate");
			}
			
		} catch (Exception $e){
			return $e->getMessage();
		}

		$this->cost = $query->price;
		return $this->cost;
	}
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class General 
{
	function __construct()
	{
	}

	function randomNumber($len=4) {
		$number = "";
		
		for ($i=0; $i<$len; $i++) {
			$number .= rand(0, 9);
		}
		
		return $number;
	}
	
	function addChar (
		$str,
		$char,
		$len
	) {
		$newStr = $str;
		
		for ($i=0; $i<($len-strlen($str)); $i++) {
			$newStr = $char . $newStr;
		}
		
		return $newStr;
	}
	
	function alMonth($index) {
		$month = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
		
		return $month[$index-1];
	}
	
	function sendSMS($noHP, $message) {
		// $smsUserKey = urlencode('8finat');
		// $smsPassKey = urlencode('parsel123!@#');
		// $smsHP = urlencode($noHP);
		// $smsMessage = urlencode($message);
		
		// $url = 'https://reguler.zenziva.net/apps/smsapi.php';
		// $url .= '?userkey=' . $smsUserKey;
		// $url .= '&passkey=' . $smsPassKey;
		// $url .= '&nohp=' . $smsHP;
		// $url .= '&pesan=' . $smsMessage;
		
		$smsToken = urlencode('123456');
		$smsHP = urlencode($noHP);
		$smsMessage = urlencode($message);
		
		$url = 'http://apps.smsdoro.com/kirim/';
		$url .= '?token=' . $smsToken;
		$url .= '&hp=' . $smsHP;
		$url .= '&pesan=' . $smsMessage;
		
		return file_get_contents($url);
	}
	
	function distance (
		$originLat,
		$originLon,
		$destinationLat,
		$destinationLon
	) {
		$json = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $originLat . ',' . $originLon . '&destinations=' . $destinationLat . ',' . $destinationLon . '&avoid=tolls');
		$obj = json_decode($json);
		return floatval($obj->rows[0]->elements[0]->distance->value) / 1000;
	}
	
	function multiDistance (
		$origins,
		$destinations
	) {
		$json = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . join('|', $origins) . '&destinations=' . join('|', $destinations) . '&avoid=tolls');
		$obj = json_decode($json);
		
		$distance = 0;
		$point = 0;
		
		for ($i=0; $i<sizeof($obj->rows[0]->elements); $i++) {
			if ($distance < floatval($obj->rows[0]->elements[$i]->distance->value)) {
				$distance = floatval($obj->rows[0]->elements[$i]->distance->value);
				
				$point = $i;
			}
		}
		
		if (sizeof($origins) == 1 && sizeof($destination == 1)) { // one to one
		}
		elseif (sizeof($origins) == 1 && sizeof($destination > 1)) { // one to many
			$ori = $origins;
			$des = $destination[$i];
			unset ($destination[$i]);
			$way = $destination;
			
			$json = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$ori.'&destination='.$des.'&waypoints=optimize:true|'.join('|', $way). '&avoid=tolls');
			$obj = json_decode($json);
			
			$distance = 0;
			
			for ($i=0; $i<sizeof($obj->routes[0]->legs); $i++) {
				$distance += $obj->routes[0]->legs[$i]->distance->value;
			}
		}
		elseif (sizeof($origins) > 1 && sizeof($destination) == 1) { // many to one
			$ori = $origins[$i];
			$des = $destination;
			unset ($origins[$i]);
			$way = $origins;
			
			$json = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$ori.'&destination='.$des.'&waypoints=optimize:true|'.join('|', $way). '&avoid=tolls');
			$obj = json_decode($json);
			
			$distance = 0;
			
			for ($i=0; $i<sizeof($obj->routes[0]->legs); $i++) {
				$distance += $obj->routes[0]->legs[$i]->distance->value;
			}
		}
		
		return $distance / 1000;
	}
	
	function gpsLocationName($lat, $lon) {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lon;
		
		$data = json_decode(file_get_contents($url));
		
		$zip='';
		$village='';
		$subdistrict='';
		$city='';
		$province='';
		$country='';
		
		foreach ($data->results[0]->address_components as $row) {
			if ($row->types[0] == "postal_code") {
				$zip=$row->long_name;
			}
			if ($row->types[0] == "administrative_area_level_4") {
				$village=$row->long_name;
			}
			if ($row->types[0] == "administrative_area_level_3") {
				$subdistrict=$row->long_name;
			}
			if ($row->types[0] == "administrative_area_level_2") {
				$city=$row->long_name;
			}
			if ($row->types[0] == "administrative_area_level_1") {
				$province=$row->long_name;
			}
			if ($row->types[0] == "country") {
				$country=$row->long_name;
			}
		}
		
		$result = array (
			'village'=>$village,
			'subdistrict'=>$subdistrict,
			'city'=>$city,
			'province'=>$province,
			'country'=>$country,
			'zip'=> $zip
		);
		
		return $result;
	}
	
	function saveMap($filename, $lat, $lon) {
		$url	= 'http://maps.googleapis.com/maps/api/staticmap?center=' . $lat . ',' . $lon . "&zoom=15&size=320x240&format=jpeg&markers=%7Clabel:P%7C" . $lat . ',' . $lon;
		
		$contents=file_get_contents($url);
		$save_path=$filename;
		
		file_put_contents($save_path,$contents);
		
		return TRUE;
	}
	
	function object_to_array($obj) {
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
			$new = array();
			foreach($obj as $key => $val) {
				$new[$key] = $this->object_to_array($val);
			}
		}
		else $new = $obj;
		return $new;
	}
}
?>
<?php

use Ivory\GoogleMap\Services\Base\TravelMode;
use Ivory\GoogleMap\Services\Base\UnitSystem;
use Ivory\GoogleMap\Services\Directions\Directions;
use Ivory\GoogleMap\Services\Directions\DirectionsRequest;
use Ivory\GoogleMap\Services\Geocoding\Geocoder;
use Ivory\GoogleMap\Services\Geocoding\GeocoderProvider;
use Ivory\GoogleMap\Services\Geocoding\GeocoderRequest;
use Widop\HttpAdapter\CurlHttpAdapter;

class Direction{

	private $directions;
	
	private $request;
	
	private $response;
	
	private $routes;
	
	private $legs;
	
	function __construct(){
	
		//call directions class
		$this->directions = new Directions(new CurlHttpAdapter());
		
		$this->request = new directionsRequest();
	}

	public function get_routes(){
	
		$this->request->setOptimizeWaypoints(true);

		$this->request->setAvoidTolls(true);

		$this->request->setRegion('us');
		$this->request->setLanguage('en');
		
		$this->request->setTravelMode(TravelMode::DRIVING);
		$this->request->setUnitSystem(UnitSystem::METRIC);
		
		$this->response = $this->directions->route($this->request);
		
		$this->routes = $this->response->getRoutes();
	}
	
	public function get_legs(){
	
		$this->get_routes();
		
		foreach($this->routes as $route){
			$this->legs = $route->getLegs();
		}
	}
	
	public function set_origin($lat, $lon){
		$this->request->setOrigin($lat, $lon, true);
	}	
	
	public function set_destination($lat, $lon){
		$this->request->setDestination($lat, $lon, true);
	}
	
	
	public function get_distance(){
	
		$this->get_legs();
		
		foreach($this->legs as $leg){
			$distance = $leg->getDistance();
		}
		
		return $distance;
	}
	
	public function get_duration($origin, $destination){
		$this->get_legs();
		
		foreach($this->legs as $leg){
			$duration = $leg->getDuration();
		}
		
		return $duration;
	}
}
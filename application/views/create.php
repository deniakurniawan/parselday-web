    <!-- Create Shipment Section -->
    <section id="shipment">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('heading_create_shipment'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('sub_heading_create_shipment'); ?></h3>
                </div>
            </div>
            <form name="createOrder" id="createOrderForm" action="<?php echo $this->config->base_url() . 'hello/create_order'; ?>" method="POST">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label><?php echo lang('label_shipment_date'); ?></label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" name="shipment_date" value="<?php echo date('D, d M Y H:i'); ?>" required readonly />
                        <!--span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span-->
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="row">
                
                 <div class="col-md-6">
                    
                    <?php if ($this->tank_auth->is_logged_in()) { ?>
                      <div class="form-group">
                        <label><?php echo lang('label_sender_details');?></label>
                        <div class="input-group">
                          <input type="text" class="form-control typeahead" id="sendername" name="origin_contact" placeholder="<?php echo lang('placeholder_details');?>">
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-user"></span>
                          </span>
                        </div>
                      </div>
                    <?php } ?>
                    <div class="form-group">
                      <label><?php echo lang('label_origin'); ?></label>
                      <input type="text" class="form-control" id="origin_search" name="origin_search" placeholder="<?php echo lang('placeholder_origin'); ?>">
                      <input type="hidden" id="origin_lat" name="origin_lat" value="-6.2297465">
                      <input type="hidden" id="origin_lng" name="origin_lng" value="106.829518">
                      <p><sup><?php echo lang('help_origin'); ?></sup></p>
                    </div>
                    <div class="form-group">
                      <div id="senderdata" style="width: 100%; height: 400px;"></div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_address" name="origin_address" placeholder="<?php echo lang('placeholder_location_details'); ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_village" name="origin_village" placeholder="<?php echo lang('placeholder_village'); ?>">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_city" name="origin_city" placeholder="<?php echo lang('placeholder_city'); ?>" required>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_country" name="origin_country" placeholder="<?php echo lang('placeholder_country'); ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_sub_district" name="origin_sub_district" placeholder="<?php echo lang('placeholder_sub_district'); ?>">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_province" name="origin_province" placeholder="<?php echo lang('placeholder_province'); ?>" required>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="origin_zip" name="origin_zip" placeholder="<?php echo lang('placeholder_zip'); ?>" required>
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label><?php echo lang('label_contact_detail'); ?></label>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="origin_name" name="origin_name" placeholder="<?php echo lang('placeholder_name'); ?>" required>
                          </div>  
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="origin_phone" name="origin_phone" placeholder="<?php echo lang('placeholder_phone'); ?>" required>
                          </div>  
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="email" class="form-control" id="origin_email" name="origin_email" placeholder="<?php echo lang('placeholder_email'); ?>" required>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <?php if ($this->tank_auth->is_logged_in()) { ?>
                      <div class="form-group">
                        <label><?php echo lang('label_recipient_details');?></label>
                        <div class="input-group">
                          <input type="text" class="form-control typeahead" id="recipientname" name="destination_contact" placeholder="<?php echo lang('placeholder_details');?>">
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-user"></span>
                          </span>
                        </div>
                      </div>
                    <?php } ?>
                    <div class="form-group">
                      <label><?php echo lang('label_destination'); ?></label>
                      <input type="text" class="form-control" id="destination_search" name="destination_search" placeholder="<?php echo lang('placeholder_destination'); ?>">
                      <input type="hidden" id="destination_lat" name="destination_lat">
                      <input type="hidden" id="destination_lng" name="destination_lng">
                      <p><sup><?php echo lang('help_destination'); ?></sup></p>
                    </div>
                    <div class="form-group">
                      <div id="recipientdata" style="width: 100%; height: 400px;"></div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_address" name="destination_address" placeholder="<?php echo lang('placeholder_location_details'); ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_village" name="destination_village" placeholder="<?php echo lang('placeholder_village'); ?>">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_city" name="destination_city" placeholder="<?php echo lang('placeholder_city'); ?>" required>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_country" name="destination_country" placeholder="<?php echo lang('placeholder_country'); ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_sub_district" name="destination_sub_district" placeholder="<?php echo lang('placeholder_sub_district'); ?>">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_province" name="destination_province" placeholder="<?php echo lang('placeholder_province'); ?>">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="destination_zip" name="destination_zip" placeholder="<?php echo lang('placeholder_zip'); ?>" required>
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label><?php echo lang('label_contact_detail'); ?></label>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="destination_name" name="destination_name" placeholder="<?php echo lang('placeholder_name'); ?>">
                          </div>  
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="destination_phone" name="destination_phone" placeholder="<?php echo lang('placeholder_phone'); ?>" required>
                          </div>  
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="email" class="form-control" id="destination_email" name="destination_email" placeholder="<?php echo lang('placeholder_email'); ?>">
                          </div>
                        </div>
                      </div>
                    </div>
                          
                  </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label><?php echo lang('label_item_description'); ?></label>
                    <input type="text" class="form-control" id="item_name" name="item_name" placeholder="<?php echo lang('placeholder_item'); ?>" required>
                  </div>
                </div>
                
                <div class="col-md-2">
                  <div class="form-group">
                    <label><?php echo lang('label_length'); ?></label>
                    <input type="number" min="0" max="50" class="form-control" id="item_lenght" name="item_lenght" placeholder="<?php echo lang('placeholder_length'); ?>" required>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label><?php echo lang('label_width'); ?></label>
                    <input type="number" min="0" max="50" class="form-control" id="item_width" name="item_width" placeholder="<?php echo lang('placeholder_width'); ?>" required>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label><?php echo lang('label_height'); ?></label>
                    <input type="number" min="0" max="50" class="form-control" id="item_height" name="item_height" placeholder="<?php echo lang('placeholder_height'); ?>" required>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label><?php echo lang('label_weight'); ?></label>
                    <input type="number" min="0" max="25" class="form-control" id="item_weight" name="item_weight" placeholder="<?php echo lang('placeholder_weight'); ?>" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <br />
                <div class="col-md-6">
                  <label><?php echo lang('label_note'); ?></label>
                  <textarea class="form-control" rows="5" id="notes" name="notes" placeholder="<?php echo lang('placeholder_note'); ?>"></textarea>
                </div>
                
                <div class="col-md-6">
                  <br />
                  <p><strong><?php echo lang('label_please_note_that'); ?></strong></p>
                  <ul>
                    <li><?php echo lang('label_note_1'); ?></li>
                    <li><?php echo lang('label_note_2'); ?></li>
                  </ul>
                </div>
              </div>
              <div class="row submission">
                <div class="col-sm-6 text-right">
                  <p>
                    <input type="hidden" class="form-control" id="distance" name="distance" placeholder="Distance">
                    <input type="hidden" min="1" class="form-control" id="cost" name="cost">
                   <?php echo lang('label_tariff'); ?> : <strong id="cost-view">Rp0,-</strong>
                  </p>
                </div>
                <div class="col-sm-6 text-left">
                  <button type="submit" class="btn btn-lg btn-primary"><?php echo lang('button_add_to_cart'); ?></button>
                </div>
              </div>
            </form>
        </div>
    </section>

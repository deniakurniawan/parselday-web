    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title"><?php echo lang('ty_order_title'); ?></h1>
            
            <?php 
            $orders=$this->session->flashdata('orders');
            $shipments=$this->session->flashdata('shipments');
            if ($orders && $shipments){
            ?>
                <p><?php echo lang('ty_order_sub_title_1'); ?></p>
                <p><?php echo lang('ty_order_sub_title_2'); ?>:</p>

            	<h3><?php echo lang('ty_order_label_summary_order'); ?></h3>
            	<div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th><?php echo lang('ty_order_table_shipment'); ?></th>
                        <th><?php echo lang('ty_order_table_origin'); ?></th>
                        <th><?php echo lang('ty_order_table_destination'); ?></th>
                        <th><?php echo lang('ty_order_table_items'); ?></th>
                        <th style="text-align:center"><?php echo lang('tariff'); ?></th>
                      </tr>
                    </thead>
                    <tbody>
            	<?php 
            	$no = 0;
            	$amount_total=0;
            	foreach ($shipments as $items){
            		$no++;
                    echo "<tr id='cartid_".$no."'>";
                    echo "<td>".$items['Shipment']."</td>";
                    echo "<td>".$items['Dari_Kontak'].' - '.$items['Dari_Telpon']."<br />";
                    echo $items['Dari_Alamat']."<br />";
                    echo $items['Dari_Kelurahan'].", ".$items['Dari_Kecamatan']."<br />";
                    echo $items['Dari_Provinsi']." ".$items['Dari_Zip']."</td>";
                    echo "<td>".$items['Untuk_Kontak']." - ".$items['Untuk_Telpon']."<br />";
                echo $items['Untuk_Alamat']."<br />";
                    echo $items['Untuk_Kelurahan'].", ".$items['Untuk_Kecamatan']."<br />";
                    echo $items['Untuk_Provinsi']." ".$items['Untuk_Zip']."</td>";
                    echo "<td>".$items['Barang']."</td>";
                    echo "<td align='right'>".$items['Harga']."</td>";
	                echo "</tr>";
	                $amount_total +=$items['Harga'];
	                  }  
	                echo "<tr class='info'>"; 
                    echo "<td colspan='3'>";
	                echo "<td><h4><?php echo lang('ty_order_table_amount_total'); ?></h4></td>";
	                echo "<td align='right'><h4>".$amount_total."</h4></td>";
	                echo "</tr>";
            	
            	?>
            	 	</tbody>
                </table>
                </div>

                <p><?php echo lang('ty_order_payment_info_1'); ?></p>

                <p><?php echo lang('ty_order_payment_info_2'); ?> <a href="<?php echo PARSELDAY_URL . 'hello/confirm_order?order=' . $orders['Order_Url']; ?>"><?php echo lang('ty_order_payment_info_3'); ?></a>
                </p>

            	<p><?php echo lang('ty_order_payment_info_4'); ?> <?php echo $orders['Email_Confirmation'];?>. <?php echo lang('ty_order_payment_info_5'); ?></p>
            <?php }
            ?>
        </div>
    </section>
    
    
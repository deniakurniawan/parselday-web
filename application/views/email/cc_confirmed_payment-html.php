<html>
<head><title>Thank You - ParselDay!</title></head>

<body>
	<div style="margin: 0; padding: 30px 0;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<!--td width="5%"></td-->
				<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
					Dear <?php echo $order['First_Name']; ?>,<br />
					<br />
					this is your payment detail:<br />
					<br />
					<br />
                    <div>
                        <table cellspacing = "10">
                            <thead>
                                <tr>
                                    <th style = "text-align: left">Origin</th>
                                    <th style = "text-align: left">Destination</th>
                                    <th style = "text-align: left">Items</th>
                                    <th style = "text-align: left">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($shipments as $shipment) {
                                    echo "<tr>";
                                        echo "<td min-width='30%'>".$shipment['options']['origin_name'].' - '.$shipment['options']['origin_phone']."<br />";
                                        echo $shipment['options']['origin_address']."<br />";
                                        echo $shipment['options']['origin_village'].", ".$shipment['options']['origin_sub_district']."<br />";
                                        echo $shipment['options']['origin_province']." ".$shipment['options']['origin_zip']."</td>";
                                        echo "<td min-width='30%'>".$shipment['options']['destination_name'].' - '.$shipment['options']['destination_phone']."<br />";
                                        echo $shipment['options']['destination_address']."<br />";
                                        echo $shipment['options']['destination_village'].", ".$shipment['options']['destination_sub_district']."<br />";
                                        echo $shipment['options']['destination_province']." ".$shipment['options']['destination_zip']."</td>";
                                        echo "<td min-width='20%'>".$shipment['options']['item_name']."</td>";
                                        echo "<td min-width='20%'>".$shipment['subtotal']."</td>";
                                    echo "</tr>";
                                } ?>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><h4>Total</h4></td>
                                    <td><h4><?php echo $order['Amount_Total']; ?></h4></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br />
                    <br />
                    Thank you for your payment. We will inform you when your goods already reach the destination.<br />
                    <br />
                    <br />
                    Regards,<br />
                    Admin ParselDay<br />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>



Dear <?php echo $order['First_Name']; ?>,

Please check your Shipment Order below before making payment:

<?php foreach ($shipments as $shipment) { ?>
1.	Shipment #	:	<?php echo $shipment['Shipment']; ?>
	Origin		:	<?php echo $shipment['Dari_Kontak'].' - '.$shipment['Dari_Telpon']; ?>
					<?php echo $shipment['Dari_Alamat']; ?>
					<?php echo $shipment['Dari_Kelurahan'].", ".$shipment['Dari_Kecamatan']; ?>
					<?php echo $shipment['Dari_Provinsi']." ".$shipment['Dari_Zip']; ?>
	Destination	:	<?php echo $shipment['Untuk_Kontak'].' - '.$shipment['Untuk_Telpon']; ?>
					<?php echo $shipment['Untuk_Alamat']; ?>
					<?php echo $shipment['Untuk_Kelurahan'].", ".$shipment['Untuk_Kecamatan']; ?>
					<?php echo $shipment['Untuk_Provinsi']." ".$shipment['Untuk_Zip']; ?>
	Items		:	<?php echo $shipment['Barang']; ?>
	Tariff		:	<?php echo $shipment['Harga']; ?>

<?php } ?>

Total	:	<?php echo $order['Amount_Total']; ?>


Please make a payment by transfer to one of the accounts listed below NOT LATER THAN 45 MINUTES:
Account Holder: PT. Parsel Ekspress Indonesia
Account Number: 450 897 8888
Bank Name: BCA Cabang Bidakara Jakarta


If possible, please include your shipment number in the transfer news.
Your order will be cancelled, WITHOUT confirmation if payment is NOT made within maximally 45 minutes.
Please SEND CONFIRMATION after making a payment by clicking <?php echo site_url("hello/confirm_order?order=".$order['Order_Url']); ?> .


Regards,
Admin ParselDay

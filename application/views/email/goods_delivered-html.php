<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Goods Delivered - ParselDay!</title></head>

<body>
	<div style="margin: 0; padding: 30px 0;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<!--td width="5%"></td-->
				<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
					Dear <?php echo $order['First_Name']; ?>,<br />
					<br />
					Thank you for using ParselDay, your goods has been arrived at destination. We are pleased to serve you and wait for your next order. Have a Nice ParselDay.<br />
                    <br />
                    <br />
                    Regards,<br />
                    Admin ParselDay<br />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>

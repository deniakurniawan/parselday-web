<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Order Info from ParselDay!</title></head>

<body>
	<div style="margin: 0; padding: 30px 0;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<!--td width="5%"></td-->
				<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
					Dear <?php echo $order['First_Name']; ?>,<br />
					<br />
					Please check your Shipment Order below before making payment:<br />
					<br />
					<br />
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th>Shipment #</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Items</th>
                                    <th>Tarif</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($shipments as $shipment) {
                                    echo "<tr>";
                                        echo "<td min-width='10%'>".$shipment['Shipment']."</td>";
                                        echo "<td min-width='30%'>".$shipment['Dari_Kontak'].' - '.$shipment['Dari_Telpon']."<br />";
                                        echo $shipment['Dari_Alamat']."<br />";
                                        echo $shipment['Dari_Kelurahan'].", ".$shipment['Dari_Kecamatan']."<br />";
                                        echo $shipment['Dari_Provinsi']." ".$shipment['Dari_Zip']."</td>";
                                        echo "<td min-width='30%'>".$shipment['Untuk_Kontak'].' - '.$shipment['Untuk_Telpon']."<br />";
                                        echo $shipment['Untuk_Alamat']."<br />";
                                        echo $shipment['Untuk_Kelurahan'].", ".$shipment['Untuk_Kecamatan']."<br />";
                                        echo $shipment['Untuk_Provinsi']." ".$shipment['Untuk_Zip']."</td>";
                                        echo "<td min-width='15%'>".$shipment['Barang']."</td>";
                                        echo "<td min-width='15%'>".$shipment['Harga']."</td>";
                                    echo "</tr>";
                                } ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td><h4>Total</h4></td>
                                    <td><h4><?php echo $order['Amount_Total']; ?></h4></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br />
                    <br />
                    Please make a payment by transfer to one of the accounts listed below <strong>NOT LATER THAN 45 MINUTES</strong>:<br />
                    Account Holder: <strong>PT. Parsel Ekspress Indonesia</strong><br />
                    Account Number: <strong>450 897 8888</strong><br />
                    Bank Name: <strong>BCA Cabang Bidakara Jakarta</strong><br />
                    <br />
                    <br />
                    If possible, please include your shipment number in the transfer news.<br />
                    Your order will be cancelled, WITHOUT confirmation if payment is NOT made within maximally 45 minutes.<br />
                    Please SEND CONFIRMATION after making a payment by clicking <nobr><a href='<?php echo site_url("hello/confirm_order?order=".$order['Order_Url']); ?>'><strong>HERE</strong></a>.</nobr><br />
                    <br />
                    <br />
                    Regards,<br />
                    Admin ParselDay<br />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>

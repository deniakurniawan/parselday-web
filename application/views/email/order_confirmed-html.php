<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Thank You - ParselDay!</title></head>

<body>
	<div style="800px; margin: 0; padding: 30px 0;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<!--td width="5%"></td-->
				<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
					Dear <?php echo $order['First_Name']; ?>,<br />
					<br />
					Thank you for your payment. We will inform you when your goods already reach the destination.<br />
                    <br />
                    <br />
                    Regards,<br />
                    Admin ParselDay<br />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>

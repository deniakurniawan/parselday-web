<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ParselDay - Mengirim Parsel Murah, Cepat dan Aman ke Seluruh Indonesia</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $this->config->base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!--link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"-->        

    <!-- Custom Fonts -->
    <link href="<?php echo $this->config->base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript">
    var config = {
        base_url: "<?php echo $this->config->base_url(); ?>",
        is_logged_in: <?php echo $this->tank_auth->is_logged_in() ? 'true' : 'false'; ?>,
    };
    </script>

    <?php
    foreach($css as $file){
      echo "\n\t\t";
      ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
    } echo "\n\t";
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">



    <!-- Navigation -->
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-top hidden-xs">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-right">
                <?php if ($this->tank_auth->is_logged_in()) {
                    echo "Hello ".$this->tank_auth->get_username(),", <a href='".$this->config->base_url()."hello/logout'>Sign Out</a>";
                } else {
                    echo "Already have an account? <a href='".$this->config->base_url()."hello/login'>Sign In</a> or <a href='".$this->config->base_url()."hello/login'>Register Here</a>";
                } ?>
              </div>
            </div>
            
          </div>
        </div>    
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                  <img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" alt="ParselDay Logo" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>#shipment">Create Shipment</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>#track">Track</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>#services">How it Works</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>hello/about_us">About Us</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>#contact">Contact</a>
                    </li>
                    
                    <li class="visible-xs-block">
                        <a class="page-scroll" href="#" data-toggle="modal" data-target="#cartmodal"><span class='cart-total-items'><?php echo $this->cart->total_items(); ?></span> Shipment(s)</a>
                    </li>
                    <li class="visible-xs-block">
                        <?php if ($this->tank_auth->is_logged_in()) {
                            echo "<a class='page-scroll' href='".$this->config->base_url()."hello/logout'>".$this->tank_auth->get_username()." (Sign Out)</a>";
                        } else {
                            echo "<a class='page-scroll' href='".$this->config->base_url()."hello/login'>Login / Register</a>";
                        } ?>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <div class="floating-cart hidden-xs">
      <a href="#" data-toggle="modal" data-target="#cartmodal">
        <span class="glyphicon glyphicon-shopping-cart" style="font-size: 24px;"></span><br />
        <span class='cart-total-items'><?php echo $this->cart->total_items(); ?></span> items<br />
        <small>Checkout?</small>
      </a>
    </div>

    <?php echo $output; ?>

    <?php if($this->load->get_section('footer') != '') {
        echo $this->load->get_section('footer');
    } ?>

    <div id='cart_summary'>
        <?php if($this->load->get_section('cart_summary') != '') {
            echo $this->load->get_section('cart_summary');
        } ?>
    </div>

    <!-- jQuery -->
    <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/classie.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/cbpAnimatedHeader.min.js"></script>
    
    <script src="<?php echo $this->config->base_url(); ?>assets/js/default.js"></script>

    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/cart.js"></script>

    <?php
    foreach($js as $file){
        echo "\n\t\t";
        ?><script src="<?php echo $file; ?>"></script><?php
    } echo "\n\t";
    ?>
</body>

</html>

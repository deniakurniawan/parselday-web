<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Jasa Kurir Online kirim barang dalam waktu 3 jam di area Jakarta Bogor Depok Tangerang Bekasi. Tel/Whatsapp 08 222 100 100 9">
    <meta name="author" content="ParselDay">
    <meta name="keywords" content="jasa kurir cepat, jasa kurir, jasa kurir jakarta, kurir jakarta, kurir motor, pesan antar, delivery, one day delivery, antar jemput barang, kurir antar barang, kurir antar kue, kurir barang jakarta, kurir cod jakarta, kurir di jakarta, kurir dokumen, kurir express, kurir ekspedisi, kurir freelance, kurir harian, kurir indonesia, kurir jakarta, kurir jakarta 24 jam, kurir jabodetabek, kurir makanan jakarta, kurir makanan, kurir ojek jakarta, kurir online jakarta, kurir pengiriman, kurir sepeda, city courier jakarta, delivery jakarta, delivery makanan, jasa angkutan barang, antar dokumen, jasa pengiriman barang, jasa kurir pengiriman barang, jasa expedisi jakarta">

    <title><?php echo lang('title'); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $this->config->base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!--link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"-->        

    <!-- Custom Fonts -->
    <link href="<?php echo $this->config->base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
    var config = {
        base_url: "<?php echo $this->config->base_url(); ?>",
        is_logged_in: <?php echo $this->tank_auth->is_logged_in() ? 'true' : 'false'; ?>,
    };
    </script>

<?php
	foreach($css as $file){
?>
    <link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
<?php
	}
?>

    <link rel="shortcut icon" href="<?php echo $this->config->base_url(); ?>assets/img/fav-icon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo $this->config->base_url(); ?>assets/img/fav-icon.png" type="image/x-icon">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-46023868-5', 'auto');
      ga('send', 'pageview');
    </script>
</head>

<body id="page-top" class="index <?php echo isset($frontpage) ? 'frontpage' : ''; ?>">
	<h1 style="display: none">ParselDay - Jasa Kurir 3 Jam Sampai</h1>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-top hidden-xs">
          <div class="container">
            <div class="row">
              <div class="col-md-6 text-left">
                 <?php echo lang('select_language'); ?> : 
                  <select id = "select_language">
                    <option value = "en" <?php if ($this->session->userdata('lang') == "english") echo "selected='selected'" ?>>English</option>
                    <option value = "id" <?php if ($this->session->userdata('lang') == "indonesia") echo "selected='selected'" ?>>Indonesia</option>
                  </select> 
              </div>
              <div class="col-md-6 text-right">
                <img src="<?php echo $this->config->base_url(); ?>assets/img/logo-whatsapp.png" height="20px" alt="Phone:" />&nbsp;&nbsp;<a href='tel:+6282221001009'>08 222 100 100 9</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($this->tank_auth->is_logged_in()) {
                    echo "Hello, <strong>" . $this->tank_auth->get_username() . "</strong>. " . sprintf(lang('welcome_signin_user'), $this->config->base_url() . "hello/account", $this->config->base_url() . "hello/logout");
                } else {
                    echo sprintf(lang('account_singin_header'), $this->config->base_url() . "hello/login", $this->config->base_url() . "hello/login");
                } ?>
              </div>
            </div>
          </div>
        </div>    
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if (isset($frontpage)) { ?>
                    <a class="navbar-brand page-scroll visible-lg-block" href="#page-top">
                      <img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" alt="ParselDay Logo" />
                    </a>
                <?php } else { ?>
                    <a class="navbar-brand page-scroll" href="<?php echo $this->config->base_url(); ?>">
                      <img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" alt="ParselDay Logo" />
                    </a>
                <?php } ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="menu-transition-eff">
                        <a class="page-scroll" href="<?php echo isset($frontpage) ? '' : $this->config->base_url(); ?>#shipment"><?php echo lang('menu_create_shipment'); ?></a>
                    </li>
                    <li class="menu-transition-eff">
                        <a class="page-scroll" href="<?php echo isset($frontpage) ? '' : $this->config->base_url() ?>#track"><?php echo lang('menu_track'); ?></a>
                    </li>
                    <li class="menu-transition-eff">
                      <?php if ($this->tank_auth->is_logged_in() && $this->tank_auth->get_level() == 0) { ?>
                          
                              <a class="page-scroll" href="<?php echo $this->config->base_url() . 'hello/confirm'; ?>">Confirm</a>
                          
                      <?php } else { ?>
                          
                              <a class="page-scroll" href="<?php echo isset($frontpage) ? '' : $this->config->base_url(); ?>#services"><?php echo lang('menu_how_it_works'); ?></a>
                          
                      <?php } ?>
                    </li>
                    <li class="menu-transition-eff">
                        <a class="page-scroll" href="<?php echo $this->config->base_url() . "hello/about_us"; ?>"><?php echo lang('menu_about_us'); ?></a>
                    </li>
                    <li class="menu-transition-eff">
                        <a class="page-scroll" href="<?php echo isset($frontpage) ? '' : $this->config->base_url(); ?>#contact"><?php echo lang('menu_contact'); ?></a>
                    </li>
                    <li class="menu-transition-eff">
                        <a class="page-scroll" href="<?php echo $this->config->base_url(); ?>blog">BLOG</a>
                    </li>
                    
                    <li class="visible-xs-block">
                        <a class="page-scroll" href="#" data-toggle="modal" data-target="#cartmodal"><span class='cart-total-items2'><?php echo $this->cart->total_items(); ?></span> Shipment(s)</a>
                    </li>
                    <li class="visible-xs-block">
                        <?php if ($this->tank_auth->is_logged_in()) {
                            echo "<a class='page-scroll' href='".$this->config->base_url() . 'hello/logout' ."'>".$this->tank_auth->get_username()." (Sign Out)</a>";
                        } else {
                            echo "<a class='page-scroll' href='".$this->config->base_url() . 'hello/login' ."'>Login / Register</a>";
                        } ?>
                    </li>
                    <li class="visible-xs-block">
                        <a class="page-scroll" href="">My Account</a>
                    </li>
                    <li class="visible-xs-block">
                        <a class="page-scroll" href="">Change Password</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        <div class="floating-cart-button">
          <div class="floating-cart hidden-xs text-center">
            <a href="<?= PARSELDAY_URL; ?>hello/cart">
              <img src="<?php echo $this->config->base_url(); ?>assets/img/icon-deliver-mini.png" alt="Cart" /><br />
              <!--span class="glyphicon glyphicon-shopping-cart" style="font-size: 24px;"></span><br /-->
              <!--span class='cart-total-items'><?php echo $this->cart->total_items(); ?></span--> 
              <!-- <?php echo lang('floating_cart_1'); ?><br /> -->
              <!-- <small><?php echo lang('floating_cart_2'); ?></small> -->
            </a>
            <span class='cart-total-items'>
            <?php if ($this->cart->total_items() > 0) {
              echo "<span class='cart-no'>".$this->cart->total_items()."</span>";
            } else {
              echo "";
            } ?>
            </span>
          </div>
        </div>
    </nav>


    <!--Track-->
    <!-- <?php if (isset($frontpage)) { ?>
            <div class="trackbox-mobile visible-sm-block visible-xs-block">
              <div class="container">
                <form id="track_mobile" action="<?php echo $this->config->base_url(); ?>hello/track" method='POST'>
                  <div class="col-xs-8">
                    <div class="form-group">
                      <input type="text" class="form-control" name="shipment_id" placeholder="<?php echo lang('placeholder_track'); ?>">
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" style="width: 100%;">TRACK</button>
                    </div>
                  </div>
                    
                </form>
              </div>
            </div>
        <?php } ?>
    -->
<?php
/*
    <div class="floating-cart hidden-xs text-center">
      <a href="#" data-toggle="modal" data-target="#cartmodal">
        <img src="<?php echo $this->config->base_url(); ?>assets/img/icon-deliver.png" alt="Cart" /><br />
        <!--span class="glyphicon glyphicon-shopping-cart" style="font-size: 24px;"></span><br /-->
        <!--span class='cart-total-items'><?php echo $this->cart->total_items(); ?></span--> <?php echo lang('floating_cart_1'); ?><br />
        <small><?php echo lang('floating_cart_2'); ?></small>
      </a>
      <span class='cart-total-items'>
      <?php if ($this->cart->total_items() > 0) {
        echo "<span class='cart-no'>".$this->cart->total_items()."</span>";
      } else {
        echo "";
      } ?>
      </span>
    </div>
	*/
?>

    <?php if (isset($frontpage)) { ?>
        <!-- Header -->
	<div class="visible-lg-block visible-md-block" style="margin-top: 108px">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="1" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active"> <img class="banner-full" src="assets/img/banner/2016-promo.jpg" alt="ParselDay Promo Tarif Flat Rp 49.000"></div>
				<div class="item"> <img class="banner-full" src="assets/img/banner/2016-HTU.jpg" data-src="" alt="ParselDay di Surabaya"></div>
				<div class="item"> <img class="banner-full" src="assets/img/banner/2016-POD.jpg" data-src="" alt="Cara Menggunakan ParselDay"></div>
			</div>
		<!-- 	<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
		</div>
	</div>
	<div class="visible-sm-block visible-xs-block">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="1" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active"> <img src="assets/img/banner/2016-promo.jpg" style="width:100%" alt="ParselDay Promo Tarif Flat Rp 49.000"></div>
				<div class="item"> <img src="assets/img/banner/2016-HTU.jpg" style="width:100%" data-src="" alt="ParselDay di Surabaya"></div>
				<div class="item"> <img src="assets/img/banner/2016-POD.jpg" style="width:100%" data-src="" alt="Cara Menggunakan ParselDay"></div>
			</div>
			<!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
		</div>
	</div>
    <?php } ?>

    <?php echo $output; ?>

    <?php if (isset($frontpage)) { ?>
        <!-- Track Shipment Section -->
        <section id="track">
          <div class="container">
            <div class="row">
              <div class="col-lg-4 col-md-6">
                <h3><?php echo lang('label_track_your_shipment'); ?></h3>
              </div>
              <div class="col-lg-8 col-md-6">
                <form action="<?php echo $this->config->base_url(); ?>hello/track" method="POST">
                  <div class="form-group">
                    <input type="text" id="shipment_id" name="shipment_id" class="form-control" placeholder="<?php echo lang('placeholder_track'); ?>">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        
        <!-- Services/How it Works Section -->
        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading"><?php echo lang('heading_how_it_works'); ?></h2>
                        <h3 class="section-subheading text-muted"><?php echo lang('sub_heading_how_it_works'); ?></h3>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <p class="text-center">
                          <img src="<?php echo $this->config->base_url(); ?>assets/img/icon-shipment.png" alt="Create Shipment" />
                        </p>
                        <h4 class="service-heading"><?php echo lang('feature_create_shipment'); ?></h4>
                        <p class="text-muted"><?php echo lang('sub_feature_create_shipment'); ?></p>
                    </div>
                    <div class="col-md-4">
                        <p class="text-center">
                          <img src="<?php echo $this->config->base_url(); ?>assets/img/icon-pay.png" alt="Make payment" />
                        </p>
                        <h4 class="service-heading"><?php echo lang('feature_pay'); ?></h4>
                        <p class="text-muted"><?php echo lang('sub_feature_pay'); ?></p>
                    </div>
                    <div class="col-md-4">
                        <p class="text-center">
                          <img src="<?php echo $this->config->base_url(); ?>assets/img/icon-deliver.png" alt="Pick and deliver" />
                        </p>
                        <h4 class="service-heading"><?php echo lang('feature_we_pick_and_deliver'); ?></h4>
                        <p class="text-muted"><?php echo lang('sub_feature_we_pick_and_deliver'); ?></p>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Contact Section -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading"><?php echo lang('label_contact_us'); ?></h2>
                        <h3 class="section-subheading text-muted"><?php echo lang('label_sub_contact_us'); ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form name="sentMessage" id="contactForm" novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="<?php echo lang('placeholder_contact_name'); ?>" id="name" name="name" required data-validation-required-message="Please enter your name.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="<?php echo lang('placeholder_contact_email'); ?>" id="email" name="email" required data-validation-required-message="Please enter your email address.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input type="tel" class="form-control" placeholder="<?php echo lang('placeholder_contact_phone'); ?>" id="phone" name="phone" required data-validation-required-message="Please enter your phone number.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="<?php echo lang('placeholder_contact_message'); ?>" id="message" name="message" required data-validation-required-message="Please enter a message."></textarea>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-xl"><?php echo lang('button_send_contact'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>

    <?php 
        echo $this->load->get_section('footer');
    ?>

    <div id='cart_summary'>
        <?php if($this->load->get_section('cart_summary') != '') {
            echo $this->load->get_section('cart_summary', 'cart/summary.php');
        } ?>
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/classie.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/cbpAnimatedHeader.min.js"></script>
    
    <!--script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?v=3&libraries=places&sensor=false&region=id'></script-->
    <!--script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/maps.js"></script-->
    
    <!--script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/moment.js"></script-->
    <!--script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script-->
    
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap3-typeahead.min.js"></script>
          
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jqBootstrapValidation.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/default.js"></script>

    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/cart.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/confirm_cart.js"></script>
    <!--script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/create_form.js"></script-->
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/sweetalert2.js"></script>

    <?php
    foreach($js as $file){
        echo "\n\t\t";
        ?><script src="<?php echo $file; ?>"></script><?php
    } echo "\n\t";
    ?>
	
	<script type="text/javascript">
        $("#select_language").change(function(event) {
            window.location = '<?php echo $this->config->base_url() . "data/lang/"; ?>'+$(this).val();
        });
	</script>
	
<!-- Google Code for parselday.com Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 968411879;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NSTwCMXb-V8Q55XjzQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968411879/?label=NSTwCMXb-V8Q55XjzQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	
	<script type="text/javascript">
		// window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		// d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		// _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		// $.src="//v2.zopim.com/?3G4417c62T8QEsvNHY0dHw5dIBkYfEQc";z.t=+new Date;$.
		// type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
</body>
</html>

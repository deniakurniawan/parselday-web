    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title"><?php echo lang('about_us_page_title'); ?></h1>
            
            <p><?php echo lang('about_us_page_sub_title_1'); ?></p>
 
            <p><?php echo lang('about_us_page_sub_title_2'); ?></p>
    </section>
    
    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('about_us_heading_milestone'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('about_us_sub_heading_milestone'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo $this->config->base_url(); ?>assets/img/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4><?php echo lang('about_us_timeline_heading_march'); ?></h4>
                                    <h4 class="subheading"><?php echo lang('about_us_timeline_sub_heading_march'); ?></h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted"><?php echo lang('about_us_timeline_body_march'); ?></p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo $this->config->base_url(); ?>assets/img/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4><?php echo lang('about_us_timeline_heading_april'); ?></h4>
                                    <h4 class="subheading"><?php echo lang('about_us_timeline_sub_heading_april'); ?></h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted"><?php echo lang('about_us_timeline_body_april'); ?></p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo $this->config->base_url(); ?>assets/img/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4><?php echo lang('about_us_timeline_heading_may'); ?></h4>
                                    <h4 class="subheading"><?php echo lang('about_us_timeline_sub_heading_may'); ?></h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted"><?php echo lang('about_us_timeline_body_may'); ?></p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4><?php echo lang('about_us_join_our_story_1'); ?>
                                    <br><?php echo lang('about_us_join_our_story_2'); ?>
                                    <br><?php echo lang('about_us_join_our_story_3'); ?></h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
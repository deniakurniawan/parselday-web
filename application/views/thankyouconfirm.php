    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title">Thank You For your Payment</h1>
            
            <p>Thank you for your payment. You will be notifed by email the courier’s identity information soon. You will be able to know the status of your shipment, simply put your shipment number in “Track Your Shipment” column. </p>
            
        </div>
    </section>
    
    
    
    <!-- Track Shipment Section -->
    <section id="page">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2>Parsel Status</h2>
            <h3 class="section-subheading text-muted">Shipment <?php echo $shipment_id; ?> is <?php echo isset($shipment) ? ' ' : 'not '; ?>found.</h3>
          </div>
        </div>
        <?php if (isset($shipment)) {
			$status = array("0", "1", "2", "3", "4", "5");
			
			$status1 = array("0", "1", "2");
			$status2 = array("3");
			$status3 = array("4", "5");
			$status4 = array("3", "4", "5");
			
			$tglpop = date_create($shipment["TglPOP"]);
			$tglpod = date_create($shipment["TglPOD"]);
			
			$tgl001 = date_format($tglpop, "d M Y H:i");
			$tgl002 = intval(substr($shipment["waktu"], 0, 2)) . ' Hour, ' . intval(substr($shipment["waktu"], 3, 2)) . ' Minutes';
			$tgl003 = date_format($tglpod, "d M Y H:i");
			
			
			if (in_array($shipment["Status"], $status)) {
				$img001 = "";
				$img002 = "";
				$img003 = "";
				
				$alert001 = "alert-info";
				$alert002 = "alert-info";
				$alert003 = "alert-info";
				
				if (in_array($shipment["Status"], $status1)) {
					$img001 = "<img src=\"" . PARSELDAY_URL . "assets/img/track/001.jpg\" class=\"img-responsive center-block\" alt=\"001\">";
					
					$alert001 = "alert-success";
				}
				if (in_array($shipment["Status"], $status2)) {
					$img002 = "<img src=\"" . PARSELDAY_URL . "assets/img/track/002.jpg\" class=\"img-responsive center-block\" alt=\"002\">";
					
					$alert002 = "alert-success";
				}
				if (in_array($shipment["Status"], $status3)) {
					$img003 = "<img src=\"" . PARSELDAY_URL . "assets/img/track/003.jpg\" class=\"img-responsive center-block\" alt=\"003\">";
					
					$alert003 = "alert-success";
				}
		?>
		<div class="row">
			<div class="col-sm-4">
				<?php echo $img001; ?>
			</div>
			<div class="col-sm-4">
				<?php echo $img002; ?>
			</div>
			<div class="col-sm-4">
				<?php echo $img003; ?>
			</div>
		</div>
		  
		<div class="row">
			<div class="col-sm-12">
				<img src="<?php echo PARSELDAY_URL; ?>assets/img/track/004.jpg" class="img-responsive" style="width: 100%; height: 5px;" alt="004">
			</div>
		</div>
		  
		<div class="row">
			<div class="col-sm-4">
				&nbsp;
			</div>
			<div class="col-sm-4">
				&nbsp;
			</div>
			<div class="col-sm-4">
				&nbsp;
			</div>
		</div>
		  
		<div class="row">
			<div class="col-sm-4 col-sm-push-8">
			<?php if (in_array($shipment["Status"], $status3)) { ?>
				<div class="alert <?php echo $alert003; ?>" role="alert">
					<h3 class="text-center">Delivered</h3>
					<img src="<?php echo PARSELDAY_URL; ?>assets/img/track/005.png" class="img-responsive center-block" alt="004">
					<h4 class="text-center"><?php echo $tgl003; ?></h4>
				</div>
			<?php } ?>
			</div>
			<div class="col-sm-4">
			<?php if (in_array($shipment["Status"], $status4)) { ?>
				<div class="alert <?php echo $alert002; ?>" role="alert">
					<h3 class="text-center">In Transport</h3>
					<img src="<?php echo PARSELDAY_URL; ?>assets/img/track/005.png" class="img-responsive center-block" alt="004">
					<h4 class="text-center"><?php echo $tgl002; ?></h4>
				</div>
			<?php } ?>
			</div>
			<div class="col-sm-4 col-sm-pull-8">
				<div class="alert <?php echo $alert001; ?>" role="alert">
					<h3 class="text-center">Pick Up</h3>
					<img src="<?php echo PARSELDAY_URL; ?>assets/img/track/005.png" class="img-responsive center-block" alt="004">
					<h4 class="text-center"><?php echo $tgl001; ?></h4>
				</div>
			</div>
		</div>
			<?php }
			} ?>
      </div>
    </section>
    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title">My Account</h1>

            <?php if ($this->session->flashdata('message')) { ?>
            	<div id="success" class="col-md-10">
            		<div class='alert alert-success'>
            			<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            			<?php echo '<strong>'.$this->session->flashdata('message').'</strong>'; ?>
            		</div>
            	</div>
        	<?php } ?>
        	<?php echo validation_errors('<div class="col-md-10"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div></div>'); ?>
			<?php if (isset($errors)) { ?>
				<div class="col-md-10">
					<div class='alert alert-danger'>
						<button type="button" class="close" data-dismiss='alert' aria-hidden='true'>&times;</button>
						<?php if (isset($errors)) {
							foreach ($errors as $k => $v) echo $v.'<br />';
						} ?>
					</div>
				</div>
			<?php } ?>

        	<div style="clear:both;"></div>

        	<div class="row">
        		<div class="col-md-4">
        			<h2>Detail Info</h2>
	                <form action="<?php echo $this->config->base_url(); ?>hello/account" method="POST">
	        			<div class="form-group">
	        				<label for="Nama">First Name</label>
	        				<input type="text" class="form-control" id="Nama" name="Nama" readonly value="<?php echo isset($user['Nama']) ? $user['Nama'] : ''; ?>">
	        			</div>
	        			<div class="form-group">
	        				<label for="NamaBelakang">Last Name</label>
	        				<input type="text" class="form-control" id="NamaBelakang" name="NamaBelakang" readonly value="<?php echo isset($user['NamaBelakang']) ? $user['NamaBelakang'] : ''; ?>">
	        			</div>
	        			<div class="form-group">
	        				<label for="username">Username</label>
	        				<input type="text" class="form-control" id="username" name="username" readonly value="<?php echo isset($user['username']) ? $user['username'] : ''; ?>">
	        			</div>
	        			<div class="form-group">
	        				<label for="email">Email</label>
	        				<input type="email" class="form-control" id="email" name="email" readonly value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>">
	        			</div>
	        			<div class="form-group">
	        				<label for="HP">Phone</label>
	        				<input type="text" class="form-control" id="HP" name="HP" readonly value="<?php echo isset($user['HP']) ? $user['HP'] : ''; ?>">
	        			</div>
	        			<div class="form-group">
		                    <input type="hidden" name="form" value="edit"></input>
							<button type="button" class="btn btn-primary" id="btnEdit" onclick="editAccount(false)">Edit</button>
							<button type="button" class="btn" id="btnCancel" onclick="editAccount(true)">Cancel</button>
							<button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
						</div>
					</form>
					<div class="form-group">
						<a href="#" data-toggle="modal" data-target="#changepasswordmodal">Change Password?</a>
					</div>
				</div>
				<?php if (isset($orders) && $orders != false) { ?>
				<div class="col-md-8">
					<h2>Recent Orders</h2>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Order #</th>
									<th>Date</th>
									<th>Payment Method</th>
									<th>Total Shipments</th>
									<th>Total Amount</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($orders as $order) { ?>
									<tr>
										<td><?php echo $order['OrderID']; ?></td>
										<td><?php echo $order['Tanggal']; ?></td>
										<td><?php if ($order['Payment_Method'] == 4) { echo 'Corp PIN'; } else if ($order['Payment_Method'] == 1) { echo 'Bank Transfer'; } else if ($order['Payment_Method'] == 3) { echo 'Credit Card'; } ?></td>
										<td><?php echo $order['Total_Items'].' item(s)'; ?></td>
										<td><?php echo $order['Amount_Total']; ?></td>
										<td><?php switch($order['StatusID']) {
											case -1:
												echo 'Waiting for VeriTrans';
												break;
											case 0:
												echo 'Waiting for Payment Confirmation';
												break;
											case 1:
												echo 'To be Confirmed';
												break;
											case 2:
												echo 'Confirmed';
												break;
					                        case 3:
					                          echo 'Coorporate (Unpaid)';
					                          break;
											default:
												echo 'ERROR';
										} ?></td>
										<td>
											<a href="<?php echo $this->config->base_url() . 'hello/view_order'. '/' .$order['Order_Url']; ?>">View Order</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<?php } else { ?>
				<div class="col-md-8">
					<h2>No Order Yet.</h2>
				</div>
				<?php } ?>
			</div>

    <!-- Modal for Change Password -->
    <div class="modal fade" id="changepasswordmodal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <h3>Change Password</h3>
                <form action="<?php echo $this->config->base_url() . 'hello/account'; ?>" method="POST">
	    			<div class="form-group">
		                <input type="password" class="form-control" name="old_password" placeholder="Current Password">
		            </div>
	    			<div class="form-group">
	    	            <input type="password" class="form-control" name="new_password" placeholder="New Password">
		            </div>
	    			<div class="form-group">
		                <input type="password" class="form-control" name="confirm_new_password" placeholder="Confirm New Password">
		            </div>
		            <div class="form-group">
	                    <input type="hidden" name="form" value="change"></input>
	                    <button type="submit" class="btn btn-primary">Submit</button>
	                </div>
		        </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
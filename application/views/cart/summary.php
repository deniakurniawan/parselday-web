    <div class="modal fade" id="cartmodal" tabindex="-1" role="dialog" aria-labelledby="careerModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <?php if ($this->cart->total_items() > 0) { ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title"><?php echo lang('popup_heading'); ?></h3>
            </div>
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <!-- ##EDITED BY BUDIONO 7 JUN 2015 ## <caption>Daftar harga pengiriman ke Indonesia. Untuk update terbaru, kontak sales kami di <strong>info@parselday.com</strong></caption> -->
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo lang('popup_items'); ?></th>
                          <th><?php echo lang('popup_shipment'); ?></th>
                          <th><?php echo lang('popup_qty'); ?></th>
                          <th style="text-align:center"><?php echo lang('popup_tariff'); ?></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 0;
                          foreach ($this->cart->contents() as $items){
                            $no++;
                            echo "<tr class='cartid_".$no."'>";
                            echo "<td>".$no."</td>";
                            echo "<td>".$items['options']['item_name']."</td>";
                            echo "<td>".$items['name']."</td>";
                            echo "<td>".$items['qty']."</td>";
                            echo "<td align='right'>Rp".number_format($items['price'],0,',','.').",-</td>";
                            echo "<td><span class='glyphicon glyphicon-trash' onclick=removeItems(".$no.",'".$items['rowid']."')></span></td>";
                          echo "</tr>";
                        } ?>
                        <tr class="info">
                          <td colspan="3"></td>
                          <td><h5>Total</h5></td>
                          <td align='right'><h5>Rp<span class='cart-total-amt'><?php echo number_format($this->cart->total(),0,',','.'); ?></span>,-</h5></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="completeCart()"><?php echo lang('popup_button_view_detail_shipmens'); ?></button>
              <button type="button" class="btn btn-primary" onclick="checkOut()"><?php echo lang('popup_button_checkout'); ?></button>
            </div>
          <?php } else { ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title"><?php echo lang('popup_empty_heading'); ?></h3>
              <caption><?php echo lang('popup_empty_sub_heading'); ?></caption>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" name="cart_create" onclick="createNew(<?php echo isset($frontpage) ? 1 : 0; ?>)">Create Shipment</button>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="modal fade" id="checkoutmodal" tabindex="-1" role="dialog" aria-labelledby="careerModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Check Out Confirmation</h3>
          </div>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6">
                <input type="text" class="form-control typeahead" id="email_address" name="checkout_email" placeholder="Your email address...">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="checkOut()">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title"><?php echo lang('complete_title'); ?></h1>
            
            <p><?php echo lang('complete_sub_title_1'); ?></p>
            <p><?php echo lang('complete_sub_title_2'); ?></p>
            
            <div class="table-responsive">
              <table class="table table-striped">
                <!-- ##EDITED BY BUDIONO 7 JUN 2015 ##<caption>Daftar harga pengiriman ke Indonesia. Untuk update terbaru, kontak sales kami di <strong>info@parselday.com</strong></caption> -->
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?php echo lang('complete_origin'); ?></th>
                    <th><?php echo lang('complete_destination'); ?></th>
                    <th><?php echo lang('complete_items'); ?></th>
                    <th style="text-align:center"><?php echo lang('complete_tariff'); ?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 0;
                    foreach ($this->cart->contents() as $items){
                      $no++;
                      $options = $items['options'];
                      echo "<tr class='cartid_".$no."'>";
                      echo "<td>".$no."</td>";
                      echo "<td>".$options['origin_name'].' - '.$options['origin_phone']."<br />";
                      echo $options['origin_address']."<br />";
                      // echo $options['origin_lat'].",".$options['origin_lng']."<br />";
                      echo $options['origin_village'].", ".$options['origin_sub_district']."<br />";
                      echo $options['origin_province']." ".$options['origin_zip']."</td>";
                      echo "<td>".$options['destination_name'].' - '.$options['destination_phone']."<br />";
                      echo $options['destination_address']."<br />";
                      // echo $options['destination_lat'].",".$options['destination_lng']."<br />";
                      echo $options['destination_village'].", ".$options['destination_sub_district']."<br />";
                      echo $options['destination_province']." ".$options['destination_zip']."</td>";
                      echo "<td>".$items['options']['item_name']."</td>";
                      echo "<td align='right'>Rp".number_format($items['price'], 0, ',', '.').",-</td>";
                      echo "<td><span class='glyphicon glyphicon-trash' onclick=removeItems(".$no.",'".$items['rowid']."')></span></td>";
                    echo "</tr>";
                  } ?>
                  <tr class="info">
                    <td colspan="3"></td>
                    <td><h4>Total</h4></td>
                    <td align='right'><h4>Rp<span class='cart-total-amt'><?php echo number_format($this->cart->total(),0,',','.'); ?></span>,-</h4></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="row submission">
              <div class="col-sm-6 text-right">
                <p><button type="button" class="btn btn-default btn-lg" onclick="createNew()"><?php echo lang('complete_button_create_more_shipment'); ?></button></p>
              </div>
              <div class="col-sm-6 text-left">
                <button type="button" class="btn btn-primary btn-lg" onclick="checkOut()"><?php echo lang('complete_button_checkout'); ?></button>
              </div>
            </div>
            
        </div>
    </section>
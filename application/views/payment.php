    <!-- Page Section -->
    <section id="page">
        <div class="container">
          
            <h1 class="page-title"><?php echo lang('confirm_payment_title'); ?></h1>
            
            <p><?php echo lang('confirm_payment_sub_title'); ?></p>
            <div class="row">
              <div class="col-md-4">
                <form action="<?php echo PARSELDAY_URL . 'hello/confirmPayment'; ?>" method="POST" enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="hidden" name="order_id" value="<?php echo $order_id;?>">
                  </div>
                  <div class="form-group">
                    <label><?php echo lang('confirm_payment_label_account_holder'); ?></label>
                    <input type="text" class="form-control" name="account_holder" placeholder="<?php echo lang('confirm_payment_placeholder_account_holder'); ?>" required>
                  </div>
                  <div class="form-group">
                    <label><?php echo lang('confirm_payment_label_bank'); ?></label>
                    <input type="text" class="form-control" name="bank" placeholder="<?php echo lang('confirm_payment_placeholder_bank'); ?>" required>
                  </div>
                  <div class="form-group">
                    <label><?php echo lang('confirm_payment_label_account_number'); ?></label>
                    <input type="text" class="form-control" name="account_number" placeholder="<?php echo lang('confirm_payment_placeholder_account_number'); ?>" required>
                  </div>
                  <div class="form-group">
                    <label><?php echo lang('confirm_payment_label_paid_amount'); ?></label>
                    <input type="number" class="form-control" name="paid_amount" placeholder="<?php echo lang('confirm_payment_placeholder_paid_amount'); ?>" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile"><?php echo lang('confirm_payment_label_payment_slip'); ?></label>
                    <input type="file" name="payment_slip" id="exampleInputFile" >
                    <p class="help-block"><?php echo lang('confirm_payment_label_info_upload'); ?></p>
                  </div>
                  <button type="submit" class="btn btn-primary"><?php echo lang('confirm_payment_button_send_confirmation'); ?></button>
                </form>
              </div>
              <div class="col-md-8">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th><?php echo lang('confirm_payment_table_shipment'); ?> #</th>
                        <th><?php echo lang('confirm_payment_table_item'); ?></th>
                        <th><?php echo lang('confirm_payment_table_route'); ?></th>
                        <th><?php echo lang('confirm_payment_table_jumlah'); ?></th>
                        <th style="text-align:center"><?php echo lang('confirm_payment_table_tarif'); ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 0;
                        $amount_total=0;
                        if (count($shipments)>0){

                          foreach ($shipments as $items){
                            $no++;
                            echo "<tr id='cartid_".$no."'>";
                            echo "<td>".$items['Shipment']."</td>";
                            echo "<td>".$items['Barang']."</td>";
                            echo "<td>From ".$items['Dari_Kecamatan']." to ".$items['Untuk_Kecamatan']."</td>";
                            echo "<td>1</td>";
                            echo "<td align='right'>Rp".number_format($items['Harga'], 0, ',', '.')."</td>";
                          echo "</tr>";
                          $amount_total +=$items['Harga'];
                          }  
                          echo "<tr>"; 
                          echo "<td colspan='3'></td>";
                          echo "<td><h4><?php echo lang('confirm_payment_table_amount_total'); ?></h4></td>";
                          echo "<td align='right'><h4>Rp".number_format($amount_total,0,',','.')."</h4></td>";
                          echo "</tr>";
                      }
                      ?>
                    </tbody>
                  </table>
              </div>
            </div>
        </div>
</section>
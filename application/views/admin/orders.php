    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title">Orders</h1>

            <?php if ($this->session->flashdata('message')) { ?>
            	<div id="success" class="col-md-10">
            		<div class='alert alert-success'>
            			<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            			<strong><?php echo $this->session->flashdata('message') ?></strong>
            		</div>
            	</div>
        	<?php } ?>

        	<div style="clear:both;"></div>

            <p>Lorem ipsum dolor sit amet consectetuer pretium Vestibulum vel Aenean id. Suscipit ligula accumsan ligula nibh turpis turpis volutpat ut enim ridiculus. Non netus sapien nec mauris Maecenas Lorem sem Aliquam Nulla Suspendisse. Dignissim urna.</p>
            <p>Nulla sapien Phasellus enim semper In pede sapien convallis wisi non. Elit sed Phasellus accumsan aliquet semper urna suscipit mattis tellus dignissim. Gravida rhoncus purus metus Suspendisse cursus semper tellus sem dictumst ac. Ut platea tincidunt cursus vitae adipiscing pellentesque felis at Phasellus Morbi. Tempus volutpat feugiat egestas ut commodo lacinia non ut ipsum et. </p>

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Order #</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Billing Address</th>
                    <th>Payment Method</th>
                    <th>Total Shipments</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($orders as $order) { ?>
	                <tr>
	                  <td><?php echo $order['OrderID']; ?></td>
	                  <td><?php echo $order['Tanggal']; ?></td>
	                  <td><?php echo $order['First_Name'].' '.$order['Last_Name'].'<br />'.$order['Phone']; ?></td>
	                  <td><?php echo $order['Address'].'<br />'.$order['City'].', '.$order['Province'].' ('.$order['Zip'].')'; ?></td>
	                  <td><?php if ($order['Payment_Method'] == 4) { echo 'Corp PIN'; } else if ($order['Payment_Method'] == 1) { echo 'Bank Transfer'; } else if ($order['Payment_Method'] == 3) { echo 'Credit Card'; } ?></td>
	                  <td><?php echo $order['Total_Items'].' item(s)'; ?></td>
	                  <td><?php echo $order['Amount_Total']; ?></td>
	                  <td><?php switch($order['StatusID']) {
	                  	case -1:
	                  		echo 'Waiting for VeriTrans';
	                  		break;
	                  	case 0:
	                  		echo 'Waiting for Payment Confirmation';
	                  		break;
	                  	case 1:
	                  		echo 'To be Confirmed';
	                  		break;
	                  	case 2:
	                  		echo 'Confirmed';
	                  		break;
                        case 3:
                          echo 'Coorporate (Unpaid)';
                          break;
	                  	default:
	                  		echo 'ERROR';
	                  	} ?></td>
	                  <td>
                        <?php if ($order['StatusID'] == 1) { ?>
    	                  	<form action="<?php echo $this->config->base_url(); ?>hello/confirm" method="POST">
    	                  		<input type="hidden" name="order_id" value="<?php echo $order['ID']; ?>" />
    	                  		<button type="submit" class="btn btn-primary">CONFIRM</button>
    	                  	</form>
                        <?php } ?>
	                  </td>
	                </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
        </div>
    </section>
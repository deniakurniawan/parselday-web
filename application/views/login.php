    <!-- Page Section -->
    <section id="page">
        <div class="container">
          
            <div class="row">
              <div class="col-md-4">
                <h2><?php echo lang('login_heading'); ?></h2>
                <p><?php echo lang('login_sub_heading'); ?></p>
                <div id="success">
                  <?php if (isset($login_form)) { ?>
                    <div class='alert alert-danger'>
                      <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                      <?php echo validation_errors(); ?>
                      <?php foreach ($errors as $k => $v) echo $v.'<br />'; ?>
                    </div>
                  <?php } ?>
                </div>
                <form action="<?php echo $this->config->base_url() . 'hello/login' ?>" id="loginForm" method="POST">
                  <div class="form-group">
                    <label for="exampleInputEmail1"><?php echo lang('login_label_username'); ?></label>
                    <input type="text" class="form-control" id="login_email" name="login_email" placeholder="<?php echo lang('login_placeholder_username'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1"><?php echo lang('login_label_passwprd'); ?></label>
                    <input type="password" class="form-control" id="login_password" name="login_password" placeholder="<?php echo lang('login_placeholder_password'); ?>">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" id="login_remember" name="login_remember" value="1"> <?php echo lang('login_checkbox_remember_me'); ?>
                    </label>
                  </div>
                  <input type="hidden" name="form" value="login"></input>
                  <button type="submit" class="btn btn-primary"><?php echo lang('login_button_login'); ?></button>
                </form>
              </div>
              <div class="col-md-7 col-md-offset-1">
                <h2><?php echo lang('register_heading'); ?></h2>
                
                <p><?php echo lang('register_sub_heading_1'); ?></p>
                
                <ul>
                  <li><?php echo lang('register_info_1'); ?></li> 
                  <li><?php echo lang('register_info_2'); ?></li>
                  <li><?php echo lang('register_info_3'); ?></li>
                  <li><?php echo lang('register_info_4'); ?></li> 
                  <li><?php echo lang('register_info_5'); ?></li>
                </ul>
                
                <p><?php echo lang('register_sub_heading_2'); ?></p>
                
                <div id="success">
                  <?php if (isset($register_form)) { ?>
                    <div class='alert alert-danger'>
                      <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                      <?php echo validation_errors(); ?>
                      <?php foreach ($errors as $k => $v) echo $v.'<br />'; ?>
                    </div>
                  <?php } ?>
                </div>
                <form action="<?php echo $this->config->base_url() . 'hello/register'; ?>" id="registerForm" method="POST">
                  <div class="form-group">
                    <label for="fullname"><?php echo lang('register_label_name'); ?></label>
                    <input type="text" class="form-control" id="register_fullname" name="register_fullname" placeholder="<?php echo lang('register_placeholder_name'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="fullname"><?php echo lang('register_label_username'); ?></label>
                    <input type="text" class="form-control" id="register_username" name="register_username" placeholder="<?php echo lang('register_placeholder_username'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="email"><?php echo lang('register_label_email'); ?></label>
                    <input type="email" class="form-control" id="register_email" name="register_email" placeholder="<?php echo lang('register_placeholder_email'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="phone"><?php echo lang('register_label_phone'); ?></label>
                    <input type="tel" class="form-control" id="register_phone" name="register_phone" placeholder="<?php echo lang('register_placeholder_phone'); ?>">
                  </div>
                  <div class="form-group">
                    <label for="password"><?php echo lang('register_label_password'); ?></label>
                    <input type="password" class="form-control" id="register_password" name="register_password" placeholder="<?php echo lang('register_placeholder_password'); ?>">
                  </div>
                  <input type="hidden" name="form" value="register"></input>
                  <button type="submit" class="btn btn-primary"><?php echo lang('register_button_register'); ?></button>
                </form>
              </div>
            </div>
        </div>
    </section>
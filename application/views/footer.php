
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <span class="copyright">Copyright &copy; ParselDay 2015. All rights reserved.</span>
                </div>
                <div class="col-md-2">
                    <ul class="list-inline social-buttons">
                        <li><a href="http://bit.ly/twitterparselday" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="http://bit.ly/fbparselday" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="http://bit.ly/instaparselday" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul class="list-inline quicklinks">
                        <li>
							<a href="#" data-toggle="modal" data-target="#careermodal">Career</a>
                        </li>
                        <li>
							<a href="#" data-toggle="modal" data-target="#partnermodal">Partnership</a>
                        </li>
                        <li>
							<a href="#" data-toggle="modal" data-target="#termsmodal">Terms of Use</a>
                        </li>
                        <li>  
                          <img src="<?php echo $this->config->base_url(); ?>assets/img/visa.png" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modal for Career -->
    <div class="modal fade" id="careermodal" tabindex="-1" role="dialog" aria-labelledby="careerModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <h3>Career with ParselDay</h3>
                <p>ParselDay are actively looking for people with passion to work with us, and help us achieve
                our goals in becoming a trusted parcel company that offers quality service in Indonesia.</p>
                <p>We're specifically looking for people for these position(s):</p>
                <ul>
                  <li>  
                    <strong>Front-end Developer</strong><br />
                    Proficient in HTML5, CSS, Javascript and NodeJS, willing to work in tight schedule, a team player, 
                    has work in website/application development industry for a minimum of 2 (two) years.
                  </li>
                  <li>
                    <strong>Customer Support</strong><br />
                    Women, 21 - 32 years old, good in communicating in English and Bahasa Indonesia both written and orally, 
                    decent skill in Office applications (MS Word, MS Excel, etc)
                  </li>
                  <li>
                    <strong>Courier</strong><br />
                    Men, less than 40 years old, based in Jakarta, own a motorcycle, has a good understanding on Jakarta's road
                  </li>
                </ul> 
                <p>
                  For more information regarding this position, please send enquiry to <strong>info@parselday.com</strong> 
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Modal for Partnerships -->
    <div class="modal fade" id="partnermodal" tabindex="-1" role="dialog" aria-labelledby="partnerModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <h3>How to Be Parsel Day Partner</h3>
                <ol type="1">
                  <li>Download and install Parsel Day apps from Google Play Store</li>
                  <li>Fill the application form and take a photo of required documents</li>
                  <li>Pass a complete interview and background check</li>
                  <li>Attend onboarding session</li>
                  <li>Congrats, you are ready to delivering</li>
                </ol>
                <h3>Frequently Asked Question</h3>
                <dl>
                  <dt>Who are we looking for?</dt>
                  <dd>We are looking for a partner, who love meet meet new people everyday, have a desire to help others, focus on safety, and responsible for the delivery of a given task</dd>
                  <dt>What are the benefit for me?</dt>
                  <dd><ul type="disc">
                    <li>Extra money, the more you work, the more money you get</li>
                    <li>Your income will be paid every week</li>
                    <li>Flexibility in determining your working hours. You can choose to work part-time or full-time</li>
                    <li>Opportunity to meet new people and help others everyday</li>
                  </ul></dd>
                  <dt>What are the requirements to be Parsel Day Partner?</dt>
                  <dd><ul type="disc">
                    <li>Minimum age 18 years</li>
                    <li>Have Android Smartphone and Internet Package</li>
                    <li>Have vehicles (bicycles, motorcycles, cars, pick-up or truck)</li>
                    <li>Have a valid driver's license</li>
                    <li>Not have a criminal record (SKCK)</li>
                  </ul></dd>
                  <dt>If I already have a job, am I able to be a partner?</dt>
                  <dd>Sure, ParselDay offers you the flexibility to choose the hours of work and delivery you want to do</dd>
                  <dt>How do I apply to become Parsel Day Partner?</dt>
                  <dd>Download Parsel Day Partner app to your Android device and submit photos of the required documents through our apps.</dd>
                  <dt>Do I need to have my own vehicle?</dt>
                  <dd>The vehicle belongs to you or not, you certainly are responsible for ensuring the delivery of goods</dd>
                  <dt>How many hours should I work?</dt>
                  <dd>We give you the flexibility to determine your working hours, as long as you can manage your time</dd>
                  <dt>How will I be compensated?</dt>
                  <dd>If you accept and complete deliveries, you will earn a commission per delivery. Weekly settlements are transferred into your bank account. The more you do the delivery, the more you get the money</dd>
                  <dt>ParselDay Insurance Policy?</dt>
                  <dd>We hope you drive safely and obey rules of the road, it’s important for you to know that Parsel Day do not cover if you get into an accident.</dd>
                  <dt>What if I feel unsafe or uncomfortable about making a delivery?</dt>
                  <dd>Your safety are very important to Parsel Day. If you feel unsafe, you can call Parsel Day Customer Care immediately</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Modal for Terms of Use -->
    <div class="modal fade" id="termsmodal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <h3>Terms of Use</h3>
                <ol type="i">
                  <li>The sender shall specify the type of goods clearly in writing on the delivery ordering process.</li>
                  <li>The contents of the shipment should be in accordance with a written statement on the submission form. ParselDay is not responsible if there is a discrepancy between the statement and the contents of the shipment.</li>
                  <li>ParselDay reserve the right to cancel the delivery of goods if knowing that the delivered goods are goods that are prohibited and dangerous.</li>
                  <li>Senders are required to pack the goods till ready to send. ParselDay is not provide box or cardboard for shipping the goods.</li>
                  <li>Goods or documents sent will be the responsibility of ParselDay if the shipper has made a payment and confirm the payment to ParselDay.</li>
                  <li>ParselDay does not accept delivery of animals.</li>
                  <li>ParselDay does not receive the shipment of dangerous goods and prohibited by law such as narcotics, methamphetamine, liquor, alcohol, drugs and firearms and or its kind.</li>
                  <li>Delivery of goods has a maximum limit to the dimensions of 50cmx50cmx50cm or weight 25kg.</li>
                  <li>Senders can order delivery of goods at 07:00 until 18:00 for get the service delivery in the same day.</li>
                  <li>Senders are entitled to information about the identity of the courier and delivery status.</li>
                  <li>The sender is entitled to give advice and criticism of ParselDay related to service delivery of goods.</li>
                  <li>If there is a cancellation of the delivery of goods, then the cost will not be refund.</li>
                  <li>In case of loss, shortage and or damage to package or documents caused by negligence on the part of ParselDay, then the ParselDay will perform maximum reimbursement 10 times of the cost of shipping documents or package or a maximum of Rp 1.000.000, - (one million rupiah).</li>
                  <li>Customers have the option to purchase insurance on the goods delivered and make a claim in the event of damage caused by the ParcelDay.</li>
                  <li>Submissions deemed to have been received in good condition and legally, if the recipient has signed proof of delivery. Thus all sorts of demands made by the delivery or shipment signing no longer be the responsibility of ParselDay.</li>
                  <li>If the recipient is not there and are in a situation that does not allow for the courier to leave the goods then the receiver has two options, namely:
                    <ul style="list-style-type:disc">
                      <li>Goods can be picked up in person at the head office of ParselDay, or</li>
                      <li>Recipients can contact Parselday and apply for retransmission and will be charged of it.</li>
                    </ul>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

	<div class="spacer-top"></div>

	<section class="shipment">

		<div class="pad-space bg-submisison container" style="background:#fff;">

	        <div class="col-md-12 text-center">
		          <h2 class="section-heading">Pembayaran</h2>
		          <h3 class="section-subheading text-muted">Lakukan pembayaran agar barang anda dapat segera kami proses.</h3>
		    </div>

      <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <div class="pad-space">
                      <ul class="nav nav-pills nav-wizard setup-panel">
                        <li class="success">                          <!-- class="disable mean not visited" "success mean passed" "active mean visited"-->
                          <a href="#step-1" data-toggle="tab"><span>BUAT</span></a>
                          <div class="nav-arrow"></div>
                        </li>
                        <li class="success">
                          <div class="nav-wedge"></div>
                          <a href="#step-2" data-toggle="tab">CEK</a>
                          <div class="nav-arrow"></div>
                        </li>
                        <li class="active">
                          <div class="nav-wedge"></div>
                          <a href="#step-3" data-toggle="tab" data-backdrop="static" data-keyboard="false" ><span>BAYAR</span></a>
                        </li>
                      </ul>
                  </div>
              </div>
          </div>
            <div class="pad-mini-space">
              <div class="col-lg-6">
                <div class="bg-warning">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="container">
                        <h5>Total Pengiriman</h5>
                          <h5>Total Harga</h5>
                        </div>
                    </div>
                    <div class="col-lg-6">
                      <h5>: 2</h5>
                      <h5>: Rp. 240.000,-</h5>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

	    	<div class="col-md-12">
	    		<div class="col-md-6">
	    			<h3>Detil Penagihan</h3>
	    			<p>Anda harus mengisi tabel berikut ini secara akurat. Kami tidak akan menggunakan data yang diberikan untuk segala tujuan lain kecuali untuk pengecekan pembayaran. Data anda akan disimpan kedalam database kami dengan aman.</p>
	    			<form class="form inline">
	    				<div class="col-md-6 form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Nama Depan">
	    				</div>
	    				<div class="col-md-6 form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Nama Belakang">
	    				</div>
	    			</form>
	    			<div class="col-md-12">
	    				<div class="form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Nomer Handphone Anda">
	    				</div>
	    			</div>
	    			<div class="col-md-12">
	    				<div class="form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Email Anda">
	    				</div>
	    			</div>
	    			<div class="col-md-12">
	    				<div class="form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Alamat anda">
	    				</div>
	    			</div>

	    			<form class="inline">
	    				<div class="col-md-6 form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Kota">
	    				</div>
	    				<div class="col-md-6 form-group">
	    					<input type="text" class="form-control" id="#" placeholder="Provinsi">
	    				</div>
	    			</form>
	    			<form class="inline">
	    				<div class="col-md-6">
	    					<input type="text" class="form-control" id="#" placeholder="Negara">
	    				</div>
	    				<div class="col-md-6">
	    					<input type="text" class="form-control" id="#" placeholder="Kode Po">
	    				</div>
	    			</form>
	    			<div class="spacer-left">
	    				<div class="col-md-12">
		    			<div class="checkbox">
					        <label>
					          <input type="checkbox"> Buat Akun
					        </label>
					    </div>
					    </div>
				    </div>
	    		</div>

	    		<div class="col-md-6">
	    			<h3>&nbsp;</h3>
	    			<select class="form-control" placeholder="Transfer Bak">
	    				<option>Transfer bank</option>
	    				<option>Kartu kredit</option>
	    			</select>
	    			<br>
		    			<p text-align="justify">Harap Lakukan Pembayaran transfer pada rekening dibawah ini <b>TIDAK LEBIH DARI 45 MENIT:</b></p>
		    			<p text-align="justify">Pemegang Rekening: <b>PT. Parsel Express Indonesia</b><br>
		    			   No. Rekening: <b>450.897.888</b><br>
		    			   Nama Bank: <b>BCA Cabang Bidakara Jakarta</b>BCA Cabang Bidakara Jakarta</p>
	    				<p text-align="justify">Mohon cantumkan nomor pengiriman anda pada berita transfer. Pesanan anda akan dibatalkan <b>TANPA</b> Konfirmasi jika pembayaran <b>TIDAK</b> dilakukan dalam <i>waktu 45 menit.</i></p>
	    			<div class="col-md-12 text-center">
	    				<div class="btn btn-lg btn-primary raised">
	    				Konfirmasi Pembayaran
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	</section>

	
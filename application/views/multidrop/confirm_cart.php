

  <div class="spacer-top"></div>
    <!-- Page Section -->
	<section class="shipment">
		<div class="pad-space bg-submisison container" style="background:#fff;">
		
	        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
		          <h2 class="section-heading">Pembayaran</h2>
		          <h3 class="section-subheading text-muted">Lakukan pembayaran agar barang anda dapat segera kami proses.</h3>
		    </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <div class="pad-space">
							<ul class="nav nav-pills nav-wizard setup-panel">
								<li class="active">
									<a href="#step-1" data-toggle="tab"><span>BUAT</span></a>
									<div class="nav-arrow"></div>
								</li>
								<li class="active">
									<div class="nav-wedge"></div>
									<a href="#step-2" data-toggle="tab"><span>CEK</span></a>
									<div class="nav-arrow"></div>
								</li>
								<li class="active">
									<div class="nav-wedge"></div>
									<a href="#step-3" data-toggle="tab"><span>BAYAR</span></a>
								</li>
							</ul>
                  </div>
              </div>
          </div>
            <div class="pad-mini-space">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="bg-warning">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                      <h3>Total Harga</h3>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-6">
                      <h3>: <?= $this->cart->total(); ?> (<?= $this->cart->total_items(); ?>'s)</h3>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div> 

            <form action="<?php echo $this->config->base_url() . 'hello/confirmCart'; ?>" method="POST" enctype="multipart/form-data">
              
              <div class="row">
                <div class="chart-place">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1 class="page-title"><?php echo lang('confirm_cart_title'); ?></h1>
                    <?php if ($this->cart->total_items() == 1  && $this->tank_auth->is_logged_in()) { ?>
                      <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <div class="form-group">
                            Are you Sender or Recipient of this shipment?
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <div class="form-group">
                            <select id="src_alt_addr">
                              <option value="asdf">Nope</option>
                              <option value="sender">Sender</option>
                              <option value="recipient">Recipient</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    <?php } ?>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p><?php echo lang('confirm_cart_sub_title'); ?></p>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="first_name" name="first_name" placeholder="<?php echo lang('confirm_cart_placeholder_first_name'); ?>" required <?php if (isset($users['Nama'])) { echo 'value="'.$users['Nama'].'" '; } elseif (isset($order['first_name'])) { echo 'value="'.$order['first_name'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="last_name" name="last_name" placeholder="<?php echo lang('confirm_cart_placeholder_last_name'); ?>" required <?php if (isset($users['NamaBelakang'])) { echo 'value"'.$users['NamaBelakang'].'" '; } elseif (isset($order['last_name'])) { echo 'value="'.$order['last_name'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo lang('confirm_cart_placeholder_phone'); ?>" required <?php if (isset($users['HP'])) { echo 'value="'.$users['HP'].'" '; } elseif (isset($order['phone'])) { echo 'value="'.$order['phone'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo lang('confirm_cart_placeholder_email'); ?>" required <?php if (isset($users['email'])) { echo 'value="'.$users['email'].'" '; } elseif (isset($order['email'])) { echo 'value="'.$order['email'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="address" name="address" placeholder="<?php echo lang('confirm_cart_placeholder_address'); ?>" required <?php if (isset($users['Alamat'])) { echo 'value="'.$users['Alamat'].'" '; } elseif (isset($order['address'])) { echo 'value="'.$order['address'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="city" name="city" placeholder="<?php echo lang('confirm_cart_placeholder_city'); ?>" required <?php if (isset($users['Kota'])) { echo 'value="'.$users['Kota'].'" '; } elseif (isset($order['city'])) { echo 'value="'.$order['city'].'"'; } else { echo ''; } ?>>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="country" name="country" placeholder="<?php echo lang('confirm_cart_placeholder_country'); ?>" required <?php if (isset($users['Negara'])) { echo 'value="'.$users['Negara'].'" '; } elseif (isset($order['country'])) { echo 'value="'.$order['country'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                          <input type="text" class="form-control" id="province" name="province" placeholder="<?php echo lang('confirm_cart_placeholder_province'); ?>" <?php if (isset($users['Provinsi'])) { echo 'value="'.$users['Provinsi'].'" '; } elseif (isset($order['province'])) { echo 'value="'.$order['province'].'"'; } else { echo ''; } ?>>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" id="zip" name="zip" placeholder="<?php echo lang('confirm_cart_placeholder_zip'); ?>" required <?php if (isset($users['Zip'])) { echo 'value="'.$users['Zip'].'" '; } elseif (isset($order['zip'])) { echo 'value="'.$order['zip'].'"'; } else { echo ''; } ?>>
                        </div>
                      </div>
                    </div>

                    <?php if (!$this->tank_auth->is_logged_in()) { ?>
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <input type="checkbox" name="create_account" value="true" text="Create an account" id="check_create_acc">x
                            <label for="create_account"><?php echo lang('confirm_cart_checkbox_create_an_account'); ?></label>
                          </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="create_account">
                          <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="<?php echo lang('confirm_cart_placeholder_username'); ?>" id="username">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="<?php echo lang('confirm_cart_placeholder_password'); ?>" id="password">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" name="confirm_password" placeholder="<?php echo lang('confirm_cart_placeholder_retype_password'); ?>" id="confirm_password">
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>

                <div class="chart-place">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="btm-space"></div>
                      <label><?php echo lang('confirm_cart_label_payment_method'); ?></label>
                      <select class="form-control" name="payment_method" id="payment_method" required>
                        <option value="1"><?php echo lang('confirm_cart_dropdown_payment_method_1'); ?></option>
                        <?php if ($this->tank_auth->is_logged_in()) echo '<option value="2">' . lang("confirm_cart_dropdown_payment_method_2") . '</option>'; ?>
                        <option value="3"><?php echo lang('confirm_cart_dropdown_payment_method_3'); ?></option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-12 payment_method_info" id="payment_method_info_bt">
                    <p><?php echo lang('confirm_cart_payment_method_info_1'); ?>:</p>
                    <p><?php echo lang('confirm_cart_payment_method_info_2'); ?>: <strong>PT. Parsel Ekspress Indonesia</strong><br />
                      <?php echo lang('confirm_cart_payment_method_info_3'); ?>: <strong>450.897.8888</strong><br />
                      <?php echo lang('confirm_cart_payment_method_info_4'); ?>: <strong>BCA Cabang Bidakara Jakarta</strong></p>
<?php
                    // <p>Account Holder: <strong>PT. ParselDay Maju Internasional</strong><br />
                      // Account Number: <strong>450.897.8888</strong><br />
                      // Bank Name: <strong>BCA Cabang Bidakara Jakarta</strong></p>
                    // <!--<p>Account Holder: <strong>PT. ParselDay Maju Internasional</strong><br />
                      // Account Number: <strong>112-233.445.566</strong><br />
                      // Bank Name: <strong>Mandiri</strong></p>-->
?>
                    <p><?php echo lang('confirm_cart_payment_method_info_5'); ?></p>
                  </div>

                  <?php if ($this->tank_auth->is_logged_in()) { ?>
                    <div class="col-md-12 col-sm-12 col-xs-12 payment_method_info" id="payment_method_info_cp">
                      <div class="form-group">
                        <input type="text" class="form-control" name="corp_pin" placeholder="Corporate PIN">
                      </div>
                    </div>
                  <?php } ?>

                  <div class="col-md-12 col-sm-12 col-xs-12 payment_method_info" id="payment_method_info_vt">
                    <div class="form-group">
                      <img src="<?php echo $this->config->base_url(); ?>assets/img/visa.png" alt="visa mastercard" />
                    </div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary"><?php echo lang('confirm_cart_button_confirm_order'); ?></button>
                    </div>
                  </div>
                </div>
              </div>

              </form>
		
		
</section>


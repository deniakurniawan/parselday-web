
	<section class="shipment">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<h2 class="section-heading">BUAT PENGIRIMAN</h2>
					<h3 class="section-subheading text-muted">Isi tabel dibawah ini untuk memulai pengiriman paket anda sekarang.</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="pull-left">
						<div class="pad-space">
							<ul class="nav nav-pills nav-wizard setup-panel">
								<li class="active">
									<a href="#step-1" data-toggle="tab"><span>BUAT</span></a>
									<div class="nav-arrow"></div>
								</li>
								<li class="disabled">
									<div class="nav-wedge"></div>
									<a href="#step-2" data-toggle="tab"><span>CEK</span></a>
									<div class="nav-arrow"></div>
								</li>
								<li class="disabled">
									<div class="nav-wedge"></div>
									<a href="#step-3" data-toggle="tab"><span>BAYAR</span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="col-md-9 col-sm-9 col-xs-9">
						<div class="pull-right">
							<label>Tanggal Pengiriman: </label>
							<div class="form-group">
								<div class='input-group date' id='datepicker'>
									<input type="text" class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="checkbox checkbox-success">
	                        <input id="checkbox3" type="checkbox">
	                        <label for="checkbox3">
	                            <strong>&nbsp;Secepatnya</strong>
	                        </label>
	                    </div>
					</div>
				</div>
			</div>

			<div class="shipment-1">
				<div class="row shadow-bg">

					<!--Left-->
					<div class="col-sm-push-0 col-xs-push-0">
					<div class="placed-content col-pull">
						<div class="content">
							<div class="col-md-12 col-sm-12 col-xs-12">
								&nbsp;
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12">
								<select id="combo_multi" autofocus="autofocus" class="form-control">
									<option value="1">Satu ASAL, satu TUJUAN</option>
									<option value="2">Satu ASAL, banyak TUJUAN</option>
									<option value="3">Banyak ASAL, satu TUJUAN</option>
								</select>
								<img id="img_multi" class="img-responsive" src="<?= PARSELDAY_URL; ?>assets/img/create/001.jpg" /> <!-- 002,003 belum di ganti -->
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 ">
								<div class="form-group">
									<h3 id="lb_title">ASAL</h3>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 ">
								<div class="form-group">
									<input type="text" class="form-control" id="origin_search" name="origin_search" placeholder="Alamat Pengambilan, (PT. IMS Logistics)">
									<input type="hidden" id="origin_lat" name="origin_lat">
									<input type="hidden" id="origin_lng" name="origin_lng">
								</div>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12 pull-right">
								<div class="form-group">
									<input type="text" class="form-control" id="destination_address" name="destination_address" placeholder="Detail Alamat Pengambilan" required>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 pull-right">
								<div class="form-group">
									<input type="text" class="form-control" id="destination_name" name="destination_name" placeholder="Nama Lengkap Pengirim" required>
								</div>  
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6 pull-left">
										<div class="form-group">
											<input type="number" class="form-control" id="destination_phone" name="destination_phone" placeholder="Telepon" required>
										</div>  
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 pull-right">
										<div class="form-group">
											<input type="email" class="form-control" id="destination_email" name="destination_email" placeholder="Email" required>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 pull-right">
								<p class="tiny text-muted">&nbsp; * pilih jenis barang :</p>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center">
									<div class="btn btn-default btn-radio active">
										<img class="sizing" src="<?php echo PARSELDAY_URL; ?>assets/img/items/Barang-Dokumen.png"><br>
										<span class="font-sizing">Dokumen</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center">
									<div class="btn btn-default btn-radio">
										<img class="sizing" src="<?php echo PARSELDAY_URL; ?>assets/img/items/Barang-Kotak-Kecil.png"><br>
										<span class="font-sizing">Kotak</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center">
									<div class="btn btn-default btn-radio">
										<img class="sizing" src="<?php echo PARSELDAY_URL; ?>assets/img/items/Barang-Kotak-Besar.png"><br>
										<span class="font-sizing">Kardus</span>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 pad-mini-space">
								<div class="row">
									<div class="col-md-9 col-sm-9 col-xs-9">
										<div class="form-group">
											<input type="email" class="form-control" id="destination_caption" name="destination_caption" placeholder="" required>
										</div>
									</div>
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-group">
											<input type="text" class="form-control" id="destination_qty" name="destination_qty" placeholder="" required>
										</div>  
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12" id="">
								<div class="row">
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-group">
											<input type="panjang" class="form-control" id="panjang" name="panjang" placeholder="Panjang">
										</div>
									</div>
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-group">
											<input type="tinggi" class="form-control" id="tinggi" nama="tinggi" placeholder="Tinggi">
										</div>
									</div>
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-group">
											<input type="lebar" class="form-control" id="lebar" nama="lebar" placeholder="Lebar">
										</div>
									</div>
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-group">
											<input type="lebar" class="form-control" id="berat" nama="berat" placeholder="berat">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 ">
								<div class="text-center">
									<button class="btn btn-warning raised" id="addRow"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
								</div>
							</div>
						</div>
					</div>
					</div>


					<!--Right-->
					<div class="col-sm-pull-0 col-xs-pull-0">
						<div class="placed-maps" id="senderdata"></div>
					</div>
					
				</div>
			</div>

				<div class="row bg-submisison">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<div class="col-md-6 col-sm-12 col-xs-12">
									<h3 class="text-center">
										<input type="hidden" class="form-control" id="distance" name="distance" placeholder="Distance">
										<input type="hidden" min="1" class="form-control" id="cost" name="cost">Tarif : <strong id="cost-view"></strong>
									</h3>
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12 text-center">
									<button id="b_submit" type="button" class="btn btn-lg btn-primary raised">TAMBAHKAN KE KURIR</button>
								</div>
							</div>
							<div class="col-md-4"></div>
						</div>
					</div>
				</div>
			
		
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 table-bg" class="">
					<div id="list-pengiriman">
						<table id="tables" class="table" cellspacing="0" width="100%">
							<thead class="table-hover">
								<tr>
									<th>No</th>
									<th>Lat</th>
									<th>Lng</th>
									<th>Lokasi</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telpon</th>
									<th>Email</th>
									<th>Qty</th>
									<th>Keterangan</th>
									<th>Panjang</th>
									<th>Tinggi</th>
									<th>Lebar</th>
									<th>Berat</th>
									<th></th>
								</tr>
							</thead>
						 
							<tfoot>
								<tr>
									<th>No</th>
									<th>Lat</th>
									<th>Lng</th>
									<th>Lokasi</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telpon</th>
									<th>Email</th>
									<th>Qty</th>
									<th>Keterangan</th>
									<th>Panjang</th>
									<th>Tinggi</th>
									<th>Lebar</th>
									<th>Berat</th>
									<th></th>
								</tr>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

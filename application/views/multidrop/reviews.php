<div class="spacer-top"></div>

  <section class="shipment">
    <div class="pad-space bg-submisison container">

      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
          <h2 class="section-heading">Cek Pengiriman Anda</h2>
          <h3 class="section-subheading text-muted">Pastikan alamat dan tujuan anda sudah benar.</h3>
      </div>

    	<div class="col-md-12 col-sm-12 col-xs-12">
    		<p class="justify">Tabel Berikut ini merupakan daftar dari seluruh pengiriman yang telah anda buat disertai dengan estimasi harga. Anda harus melakukan pengecekan terlebih dahulu untuk memastikan kebenaran data pada tabel berikut agar terhindar dari kesalahan dalam pengiriman maupun penjemputan barang.<br></p>
    		<p class="justify">Anda juga dapat melakukan menghapus data pada kolom dengan menekan icon <i class="fa fa-trash-o"></i>.</p>
    	</div>

      <div class="col-md-12 col-sm-12 col-xs-12 pad-space">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="pull-left">
					<ul class="nav nav-pills nav-wizard setup-panel">
						<li class="active">
							<a href="#step-1" data-toggle="tab"><span>BUAT</span></a>
							<div class="nav-arrow"></div>
						</li>
						<li class="active">
							<div class="nav-wedge"></div>
							<a href="#step-2" data-toggle="tab"><span>CEK</span></a>
							<div class="nav-arrow"></div>
						</li>
						<li class="disabled">
							<div class="nav-wedge"></div>
							<a href="#step-3" data-toggle="tab"><span>BAYAR</span></a>
						</li>
					</ul>
                </div>
          	</div>

	        <div class="cost">
	          <div class="col-md-6 col-sm-12 col-xs-12">
	            <div class="bg-warning">
	              	<div class="row">
		                <div class="col-md-6 col-sm-6 col-xs-6">
		                    <h4>Total Harga</h3>
		                    <h4>Total Shipment</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6">
		                  	<h4>: &nbsp;Rp.&nbsp; <?= $this->cart->total(); ?> </h3>
		                  	<h4>: &nbsp; <?= $this->cart->total_items(); ?></h4>
		                </div>
	              	</div>
	            </div>
	          </div>
	        </div>

        </div>
      </div>

        <div id="list-pengiriman" class="col-md-12 col-sm-12 col-xs-12">

<?php 
  $i = 0;
  foreach ($this->cart->contents() as $items) {
  ++$i;
?>
          <hr>
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<p><strong>Tanggal Pembayaran : 16/12/2015<br>Estimasi Waktu : <i>Secepatnya</i></strong></p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="text-center">
						<h3>shipment <?= $i ?></h3>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<h4 class="pull-right">HARGA : Rp. <?= $items['price']; ?>&nbsp;&nbsp;</h4>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div style="table-bg">
						<table class="display dataTable" id="example<?= $i; ?>" style="width: 100%;">
							<thead>
								<tr role="row">
									<th>No</th>
									<th>Lokasi</th>
									<th>Nama</th>
									<th>Detail Lokasi</th>
									<th>Telpon</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>

<?php
	foreach($items['options'] as $row) {
?>
								<tr>
									<td><?= $row[0]; ?></td>
									<td><?= $row[3]; ?></td>
									<td><?= $row[4]; ?></td>
									<td><?= $row[5]; ?></td>
									<td><?= $row[6]; ?></td>
									<td><?= $row[7]; ?></td>
								</tr>
<?php
	}
?>
						</table>
					</div>
				</div>
			</div>
<?php 
  }
?>
          <div class="col-md-12 col-sm-12 col-xs-12 btn-space btm-space">
            <div class="text-center">
              <hr>
				<a class="btn btn-warning raised btn-inline-space" href="<?= PARSELDAY_URL;?>">
					<i class="fa fa-rotate-left"></i>&nbsp;Kembali
				</a>
				<a class="btn btn-warning raised btn-inline-space" href="<?= PARSELDAY_URL . 'hello/confirmCart';?>">
					<i class="fa fa-check"></i>&nbsp;Lanjutkan
				</a>
              <hr>
            </div>
          </div>

        </div>
    </div>
  </section>

    <!-- Page Section -->
    <section id="page">
        <div class="container">
            <h1 class="page-title">Order: <?php echo $order['OrderID']; ?></h1>

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Order #</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Billing Address</th>
                    <th>Payment Method</th>
                    <th>Total Shipments</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td><?php echo $order['OrderID']; ?></td>
                      <td><?php echo $order['Tanggal']; ?></td>
                      <td><?php echo $order['First_Name'].' '.$order['Last_Name'].'<br />'.$order['Phone']; ?></td>
                      <td><?php echo $order['Address'].'<br />'.$order['City'].', '.$order['Province'].' ('.$order['Zip'].')'; ?></td>
                      <td><?php if ($order['Payment_Method'] == 4) { echo 'Corp PIN'; } else if ($order['Payment_Method'] == 1) { echo 'Bank Transfer'; } else if ($order['Payment_Method'] == 3) { echo 'Credit Card'; } ?></td>
                      <td><?php echo $order['Total_Items'].' item(s)'; ?></td>
                      <td><?php echo $order['Amount_Total']; ?></td>
                      <td><?php switch($order['StatusID']) {
                      	case -1:
                      		echo 'Waiting for VeriTrans';
                      		break;
                      	case 0:
                      		echo 'Waiting for Payment Confirmation';
                      		break;
                      	case 1:
                      		echo 'To be Confirmed';
                      		break;
                      	case 2:
                      		echo 'Confirmed';
                      		break;
                        case 3:
                          echo 'Coorporate (Unpaid)';
                          break;
                      	default:
                      		echo 'ERROR';
                      	} ?></td>
                    </tr>
                </tbody>
              </table>
            </div>

            <?php if (isset($shipments)) { ?>
                <h2 class="page-title">Shipments</h2>

                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Shipment #</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Items</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($shipments as $shipment) { 
                        echo "<tr>";
                          echo "<td>".$shipment['Shipment']."</td>";
                          echo "<td>".$shipment['Dari_Kontak'].' - '.$shipment['Dari_Telpon']."<br />";
                          echo $shipment['Dari_Alamat']."<br />";
                          echo $shipment['Dari_Kelurahan'].", ".$shipment['Dari_Kecamatan']."<br />";
                          echo $shipment['Dari_Provinsi']." ".$shipment['Dari_Zip']."</td>";
                          echo "<td>".$shipment['Untuk_Kontak'].' - '.$shipment['Untuk_Telpon']."<br />";
                          echo $shipment['Untuk_Alamat']."<br />";
                          echo $shipment['Untuk_Kelurahan'].", ".$shipment['Untuk_Kecamatan']."<br />";
                          echo $shipment['Untuk_Provinsi']." ".$shipment['Untuk_Zip']."</td>";
                          echo "<td>".$shipment['Barang']."</td>";
                          echo "<td>";
                          switch($shipment['Status']) {
                            case 0:
                              echo "To be Confirmed";
                              break;
                            case 1:
                              echo "Awaiting Collection";
                              break;
                            case 2:
                              echo "Awaiting Collection";
                              break;
                            case 3:
                              echo "Onboard with Driver";
                              break;
                            case 4:
                              echo "Delivered";
                              break;
                            case 5:
                              echo "Delivered";
                              break;
                            default:
                              echo "Unknown";
                          }
                          echo "</td>";
                          echo "<td><a href='".$this->config->base_url()."hello/track/".$shipment['track_url']."'>TRACK!</a></td>";
                        echo "</tr>";
                      } ?>
                    </tbody>
                  </table>
                </div>
            <?php } ?>
        </div>
    </section>
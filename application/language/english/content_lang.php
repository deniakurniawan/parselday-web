<?php
/*
------------------
Language: English
------------------
*/

$lang['title'] = 'ParselDay - Jasa Kurir 3 Jam Sampai';

$lang['select_language'] = "Select language";

//mini top header
$lang['account_singin_header'] = "Already have an account? <a href='%s'>Sign In</a> or <a href='%s'>Register Here</a>";


//menu kecil dikanan
$lang['floating_cart_1'] = "Shipments";
$lang['floating_cart_2'] = "Checkout?";

//menu sesudah login
$lang['welcome_signin_user'] = "<a href='%s'>My Account</a> | <a href='%s'>Sign Out</a>";

// menu
$lang['menu_create_shipment'] = "CREATE SHIPMENT";
$lang['menu_track'] = "TRACK";
$lang['menu_how_it_works'] = "HOW IT WORKS";
$lang['menu_about_us'] = "ABOUT US";
$lang['menu_contact'] = "CONTACT";

//popup
$lang['popup_empty_heading'] = "NO SHIPMENT YET!";
$lang['popup_empty_sub_heading'] = "Your shipment cart is currently empty. You have not inserted data of delivery. Please return to the “Create Shipment” page.";

$lang['popup_heading'] = "SHIPMENT CART";
$lang['popup_items'] = "Items";
$lang['popup_shipment'] = "Shipment";
$lang['popup_qty'] = "Qty";
$lang['popup_tariff'] = "Tariff";

$lang['popup_button_view_detail_shipmens'] = "View Detail Shipments";
$lang['popup_button_checkout'] = "CHECK OUT";


// //placeholder track
$lang['placeholder_track'] = "Insert your Shipment Number here and hit 'Enter' to TRACK!";

// //slogan
$lang['slogan'] = 'SEND YOUR PARCEL TODAY!';

// //button
$lang['button_create_shipment'] = "CREATE SHIPMENT";

// //date and time
$lang['operational_hour'] = "Operational Hours 06.00 - 18.00";

// //Heading
$lang['heading_create_shipment'] = "CREATE SHIPMENT";
$lang['heading_how_it_works'] = "HOW IT WORKS";

// //Sub Heading
$lang['sub_heading_create_shipment'] = "Fill in the form below to start sending your parcel now.";
$lang['sub_heading_how_it_works'] = "Learn 3 easy steps to get your parcel delivered on-time and on-budget";

// //Feature
$lang['feature_create_shipment'] = "Create Shipment";
$lang['feature_pay'] = "Pay";
$lang['feature_we_pick_and_deliver'] = "We Pick and Deliver";

// //Sub Feature
$lang['sub_feature_create_shipment'] = "Fill in the online shipment order form at our website, and follow the instructions sent to your email. If you need any help in filling the form, our customer support team are ready to help 24/7.";
$lang['sub_feature_pay'] = "Make a payment using our various supported payment methods, including bank transfer, KlikPay BCA and Credit Cards. Don't forget to confirm your payment once you're done.";
$lang['sub_feature_we_pick_and_deliver'] = "Simply wait for our team to pickup your parsel and deliver it securely to your client. Don't worry, we offer money-back guarantee for every shipment we do.";


$lang['label_sender_details'] = "Sender Details";
$lang['label_recipient_details'] = "Recipient Details";
$lang['placeholder_details'] = "Type your contact here";
$lang['label_shipment_date'] = "Shipment Date";
$lang['label_origin'] = "Origin";
$lang['label_destination'] = "Destination";
$lang['placeholder_origin'] = "Type location, example: Jl. Pembangunan 3";
$lang['placeholder_destination'] = "Type location, example: Jl. Pembangunan 3";
$lang['help_origin'] = "Only cover Jakarta, Tangerang, Surabaya and Sidoarjo area";
$lang['help_destination'] = "Only cover Jakarta, Tangerang, Surabaya and Sidoarjo area";
$lang['placeholder_location_details'] = "Location details: House no/Floor/Landmark";
$lang['placeholder_village'] = "Village";
$lang['placeholder_sub_district'] = "Sub District";
$lang['placeholder_city'] = "City";
$lang['placeholder_province'] = "Province";
$lang['placeholder_country'] = "Country";
$lang['placeholder_zip'] = "Kode Zip / Postal Code";
$lang['label_contact_detail'] = "Contact Details";
$lang['placeholder_name'] = "Name";
$lang['placeholder_phone'] = "Phone";
$lang['placeholder_email'] = "Email Address";
$lang['label_item_description'] = "Item(s) Description";
$lang['placeholder_item'] = "Describe your item";
$lang['label_length'] = "Length (cm)";
$lang['placeholder_length'] = "Length (Max 50 cm)";
$lang['label_width'] = "Width (cm)";
$lang['placeholder_width'] = "Width (Max 50 cm)";
$lang['label_height'] = "Height (cm)";
$lang['placeholder_height'] = "Height (Max 50 cm)";
$lang['label_weight'] = "Weight (kg)";
$lang['placeholder_weight'] = "Weight (Max 25 kg)";
$lang['label_note'] = "Notes";
$lang['placeholder_note'] = "Write any specific instruction for your goods (if any)..";
$lang['label_please_note_that'] = "Please note that";
$lang['label_note_1'] = "Please make sure that you fill in the contact details for the receiver, so we can confirm the delivery";
$lang['label_note_2'] = "Please write detailed information such as tower and room number when sending to apartment";
$lang['label_tariff'] = "Tariff";
$lang['button_add_to_cart'] = "ADD TO CART";

$lang['label_track_your_shipment'] = "Track your shipment";

//form contact us
$lang['label_contact_us'] = "CONTACT US";
$lang['label_sub_contact_us'] = "Questions? Suggestions? Drop us a message and we'll get back to you ASAP!<br>You can contact us at <a href='tel:+6282221001009'>08 222 100 100 9</a> or <a href='mailto:info@parselday.com'>info@parselday.com</a>";
$lang['placeholder_contact_name'] = "Your Name *";
$lang['placeholder_contact_email'] = "Your Email *";
$lang['placeholder_contact_phone'] = "Your Phone *";
$lang['placeholder_contact_message'] = "Your Message *";
$lang['button_send_contact'] = "SEND MESSAGE";

//form about us

$lang['about_us_page_title'] = "ABOUT US";
$lang['about_us_page_sub_title_1'] = "<strong>PT. ParselDay Maju Internasional</strong> is a subsidiary company of PT IMS Logistik Indonesia which provide individuals or companies for delivery within the same day either its goods or documents in the city. It also be known as a service provider which fast, safe, reasonable and sensitive to the environment.";
$lang['about_us_page_sub_title_2'] = "In this regard, we are here to develop service with the concept of crowdsourcing. It will create a new jobs for those who want to earn extra income. Job opportunities as partners is open to freelancer, students, workers, and those who have turned 18 years and passed the background screening process.";

$lang['about_us_heading_milestone'] = "MILESTONE";
$lang['about_us_sub_heading_milestone'] = "A brief history about ParselDay.";

$lang['about_us_timeline_heading_march'] = "MARCH 2015";
$lang['about_us_timeline_sub_heading_march'] = "ParselDay is born";
$lang['about_us_timeline_body_march'] = "Implementation stage from idea into the real one.";

$lang['about_us_timeline_heading_april'] = "APRIL 2015";
$lang['about_us_timeline_sub_heading_april'] = "Grand Opening";
$lang['about_us_timeline_body_april'] = "Is a stage of pre-launch. Website can be used in accordance with the features offered";

$lang['about_us_timeline_heading_may'] = "MAY 2015";
$lang['about_us_timeline_sub_heading_may'] = "Addition of 100 Drivers";
$lang['about_us_timeline_body_may'] = "In this stage, we are looking to add more drivers to maximize our service.";

$lang['about_us_join_our_story_1'] = "Be Part";
$lang['about_us_join_our_story_2'] = "of Our";
$lang['about_us_join_our_story_3'] = "Story!";

//form view detail shipment

$lang['complete_title'] = "SHIPMENT CART";

$lang['complete_sub_title_1'] = "After you create the shipment it will be shown in the table below. The table is a summary of the shipment that you do along with the estimated price. You should check your data in this form whether the data is accurate or not. From the list you can also remove shipment that has been added to the chart by clicking trash button in the right side.";
$lang['complete_sub_title_2'] = "If you want to add a new shipment with different origin or destination then you should click the button “Create More Shipment“.";

$lang['complete_origin'] = "Origin";
$lang['complete_destination'] = "Destination";
$lang['complete_items'] = "Items";
$lang['complete_tariff'] = "Tariff";

$lang['complete_button_create_more_shipment'] = "CREATE MORE SHIPMENT";
$lang['complete_button_checkout'] = "CHECK OUT";

//form confirm cart
$lang['confirm_cart_title'] = "BILLING DETAIL";
$lang['confirm_cart_sub_title'] = "Please fill the below form accurately. We will not use the data you provided for any other purpose except for payment confirmation. Your data will be stored in our secure database";

$lang['confirm_cart_placeholder_first_name'] = "First Name";
$lang['confirm_cart_placeholder_last_name'] = "Last Name";
$lang['confirm_cart_placeholder_phone'] = "Your Phone Number";
$lang['confirm_cart_placeholder_email'] = "Your Email";
$lang['confirm_cart_placeholder_address'] = "Your Address";
$lang['confirm_cart_placeholder_city'] = "City";
$lang['confirm_cart_placeholder_province'] = "Province";
$lang['confirm_cart_placeholder_country'] = "Country";
$lang['confirm_cart_placeholder_zip'] = "ZIP / Postal Code";
$lang['confirm_cart_placeholder_username'] = "Username";
$lang['confirm_cart_placeholder_password'] = "Password";
$lang['confirm_cart_placeholder_retype_password'] = "Re-type Password";

$lang['confirm_cart_checkbox_create_an_account'] = "Create an account";

$lang['confirm_cart_heading'] = "DETAIL SHIPMENTS";

$lang['confirm_cart_label_origin'] = "Origin";
$lang['confirm_cart_label_destination'] = "Destination";
$lang['confirm_cart_label_items'] = "Items";
$lang['confirm_cart_label_tariff'] = "Tariff";

$lang['confirm_cart_button_confirm_order'] = "CONFIRM ORDER";

$lang['confirm_cart_label_payment_method'] = "Payment Method";

$lang['confirm_cart_dropdown_payment_method_1'] = "Bank Transfer";
$lang['confirm_cart_dropdown_payment_method_2'] = "Corporate PIN";
$lang['confirm_cart_dropdown_payment_method_3'] = "Credit Card";

$lang['confirm_cart_payment_method_info_1'] = "Please make a payment by transfer to one of the accounts listed below <strong>NOT LATER THAN 45 MINUTES</strong>";
$lang['confirm_cart_payment_method_info_2'] = "Account Name";
$lang['confirm_cart_payment_method_info_3'] = "Account Number";
$lang['confirm_cart_payment_method_info_4'] = "Bank Name & branch";
$lang['confirm_cart_payment_method_info_5'] = "Please include your shipment number in the transfer news. Your order will be cancelled <strong>WITHOUT</strong>confirmation if payment is <strong>NOT</strong> made in the next 45 minutes";


//page login & register

$lang['login_heading'] = "MEMBERS LOGIN";
$lang['login_sub_heading'] = "Please fill in the form below with your login details.";

$lang['login_label_username'] = "Username / Email address";
$lang['login_placeholder_username'] = "Enter Username / Email";
$lang['login_label_passwprd'] = "Password";
$lang['login_placeholder_password'] = "Password";
$lang['login_checkbox_remember_me'] = "Remember me";
$lang['login_button_login'] = "LOGIN";

$lang['register_heading'] = "DON'T HAVE AN ACCOUNT YET?";
$lang['register_sub_heading_1'] = "Here are several benefits of registering yourself or your company to ParselDay.";

$lang['register_info_1'] = "Addresses used will be automatically saved to your address book.";
$lang['register_info_2'] = "You only need to input the data once if the same address is repeated.";
$lang['register_info_3'] = "Be the first to know our updates or newest information.";
$lang['register_info_4'] = "Complete order history";
$lang['register_info_5'] = "And many more!";

$lang['register_sub_heading_2'] = "So, what are you waiting for? Simply fill the form below to register!";

$lang['register_label_name'] = "Name";
$lang['register_placeholder_name'] = "Your name / company";
$lang['register_label_username'] = "Username";
$lang['register_placeholder_username'] = "Enter username";
$lang['register_label_email'] = "Email address";
$lang['register_placeholder_email'] = "Enter email";
$lang['register_label_phone'] = "Phone";
$lang['register_placeholder_phone'] = "Enter your phone number";
$lang['register_label_password'] = "Password";
$lang['register_placeholder_password'] = "Enter password";
$lang['register_button_register'] = "REGISTER NOW";


//page thank you konfirmasi order
 
$lang['ty_order_title'] = "Thank You for Order";
$lang['ty_order_sub_title_1'] = "Your order has been received.";
$lang['ty_order_sub_title_2'] = "Please check your Shipment Order below before making payment";

$lang['ty_order_label_summary_order'] = "SUMMARY ORDER";
$lang['ty_order_table_shipment'] = "Shipment #";
$lang['ty_order_table_origin'] = "Origin";
$lang['ty_order_table_destination'] = "Destination";
$lang['ty_order_table_items'] = "Items";
$lang['ty_order_table_tariff'] = "Tariff";
$lang['ty_order_table_amount_total'] = "AMOUNT TOTAL";

$lang['ty_order_payment_info_1'] = "Please make a payment by transfer to one of the accounts listed below <strong>NOT LATER THAN 45 MINUTES</strong>:<br />
                Account Name: <strong>PT. Parsel Ekspress Indonesia</strong><br />
                Account Number: <strong>450 897 8888</strong><br />
                Bank Name & Branch: <strong>BCA Cabang Bidakara Jakarta</strong>";

$lang['ty_order_payment_info_2'] = "Please include your shipment number in the transfer news.</br> 
                Your order will be cancelled WITHOUT confirmation if payment is NOT made in the next 45 minutes.</br>
                Please <strong>CONFIRM</strong> after making a payment by clicking ";
$lang['ty_order_payment_info_3'] = "HERE";

$lang['ty_order_payment_info_4'] = "We also send your summary order to ";
$lang['ty_order_payment_info_5'] = "Please check your inbox or spam folder. If you have more question don't hesitate to contact us at info@parselday.com";


//page confirm payment

$lang['confirm_payment_title'] = "CONFIRM PAYMENT";
$lang['confirm_payment_sub_title'] = "You have to fill the form provided accurately. Please note that we will not use the data provided by you for any other purpose except payment checking. Your data will be stored in our database securely. You also have to upload proof of transaction by click “Choose File” in the payment slip. If you’ve filled the form completely, click “Send Confirmation” button in the lower section of the form. When you’ve finished all the steps above, just wait for a notification to your email for your order status update.";

$lang['confirm_payment_label_account_holder'] = "Account Holder";
$lang['confirm_payment_placeholder_account_holder'] = "Your bank account's name";
$lang['confirm_payment_label_bank'] = "Bank";
$lang['confirm_payment_placeholder_bank'] = "Your bank";
$lang['confirm_payment_label_account_number'] = "Account Number";
$lang['confirm_payment_placeholder_account_number'] = "Your bank account's number";
$lang['confirm_payment_label_paid_amount'] = "Paid Amount";
$lang['confirm_payment_placeholder_paid_amount'] = "Paid amount";
$lang['confirm_payment_label_payment_slip'] = "Payment Slip";
$lang['confirm_payment_label_info_upload'] = "Please upload in JPG/PNG/PDF only.";

$lang['confirm_payment_button_send_confirmation'] = "SEND CONFIRMATION";

$lang['confirm_payment_table_shipment'] = "Shipment #";
$lang['confirm_payment_table_item'] = "Item";
$lang['confirm_payment_table_route'] = "Route";
$lang['confirm_payment_table_jumlah'] = "Qty";
$lang['confirm_payment_table_tarif'] = "Tariff";
$lang['confirm_payment_table_amount_total'] = "AMOUNT TOTAL";










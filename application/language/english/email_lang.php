<?php

// Email subjects
$lang['email_subject_confirm_order'] = 'Your Order on %s!';
$lang['email_subject_order_confirmed'] = 'Your Payment on %s!';
$lang['email_subject_goods_delivered'] = 'Your Tracking Status on %s!';


/* End of file email_lang.php */
/* Location: ./application/language/english/email_lang.php */
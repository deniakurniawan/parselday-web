<?php
/*
------------------
Language: Indonesia
------------------
*/

$lang['title'] = 'ParselDay - Jasa Kurir 3 Jam Sampai';

$lang['select_language'] = "Pilih bahasa";

//mini top header
$lang['account_singin_header'] = "Sudah memiliki akun? <a href='%s'>Masuk</a> atau <a href='%s'>Daftar disini</a>";

//menu kecil dikanan
$lang['floating_cart_1'] = "Pengiriman";
$lang['floating_cart_2'] = "Selesai?";

//menu sesudah login
$lang['welcome_signin_user'] = "<a href='%s'>Akun saya</a> | <a href='%s'>Keluar</a>";

//menu
$lang['menu_create_shipment'] = "BUAT PENGIRIMAN";
$lang['menu_track'] = "LACAK";
$lang['menu_how_it_works'] = "BAGAIMANA CARANYA";
$lang['menu_about_us'] = "TENTANG KAMI";
$lang['menu_contact'] = "KONTAK";

//popup
$lang['popup_empty_heading'] = "TIDAK ADA PENGIRIMAN!";
$lang['popup_empty_sub_heading'] = "Keranjang pengiriman anda saat ini kosong. Anda belum memasukkan data pengiriman. Harap kembali ke halaman “Buat Pengiriman”.";

$lang['popup_heading'] = "Keranjang pengiriman";
$lang['popup_items'] = "Barang";
$lang['popup_shipment'] = "pengiriman";
$lang['popup_qty'] = "Banyak";
$lang['popup_tariff'] = "Tarif";

$lang['popup_button_view_detail_shipmens'] = "Lihat detil pengiriman";
$lang['popup_button_checkout'] = "SELESAI";

// //placeholder track
$lang['placeholder_track'] = "Masukan nomer pengiriman anda, dan tekan 'Enter' untuk LACAK!";

// //slogan
$lang['slogan'] = 'KIRIMKAN PAKET ANDA SEKARANG!';

// //button
$lang['button_create_shipment'] = "BUAT PENGIRIMAN";

// //date and time
$lang['operational_hour'] = "Jam operasional 06.00 - 18.00";

// //Heading
$lang['heading_create_shipment'] = "BUAT PENGIRIMAN";
$lang['heading_how_it_works'] = "BAGAIMANA CARANYA?";

// //Sub Heading
$lang['sub_heading_create_shipment'] = "Isi tabel dibawah ini untuk memulai pengiriman paket anda sekarang.";
$lang['sub_heading_how_it_works'] = "Berikut 3 langkah mudah untuk mendapatkan paket terkirim tepat waktu dan sesuai dengan budget anda";

// //Feature
$lang['feature_create_shipment'] = "Buat pengiriman";
$lang['feature_pay'] = "Bayar";
$lang['feature_we_pick_and_deliver'] = "Kami Ambil dan Kirim";

// //Sub Feature
$lang['sub_feature_create_shipment'] = "Isi tabel pengiriman online pada website, dan ikuti segala instruksi yang dikirimkan ke email anda. Jika anda butuh bantuan dalam mengisi tabel, customer support kami siap untuk membantu anda 24/7.";
$lang['sub_feature_pay'] = "Lakukan pembayaran dengan beberapa metode, seperti transfer, KlikPay BCA, dan Kartu Kredit. Jangan lupa untuk melakukan konfirmasi saat anda telah melakukan pembayaran.";
$lang['sub_feature_we_pick_and_deliver'] = "Silahkan tunggu tim kami untuk mengambil paket dan mengantarkannya dengan aman ke tujuan.";


$lang['label_sender_details'] = "Detil Pengirim";
$lang['label_recipient_details'] = "Detil Penerima";
$lang['placeholder_details'] = "Ketik kontak anda disini";
$lang['label_shipment_date'] = "Tanggal pengiriman";
$lang['label_origin'] = "Asal";
$lang['label_destination'] = "Tujuan";
$lang['placeholder_origin'] = "Ketik lokasi, contoh: Jl. Pembangunan 3";
$lang['placeholder_destination'] = "Ketik lokasi, contoh: Jl. Pembangunan 3";
$lang['help_origin'] = "Hanya untuk daerah Jakarta, Tangerang, Surabaya dan Sidoarjo";
$lang['help_destination'] = "Hanya untuk daerah Jakarta, Tangerang, Surabaya dan Sidoarjo";
$lang['placeholder_location_details'] = "Detail lokasi: Rumah no/Lantai/Landmark";
$lang['placeholder_village'] = "Kelurahan";
$lang['placeholder_sub_district'] = "Kecamatan";
$lang['placeholder_city'] = "Kota";
$lang['placeholder_province'] = "Provinsi";
$lang['placeholder_country'] = "Negara";
$lang['placeholder_zip'] = "Kode Pos";
$lang['label_contact_detail'] = "Detail Kontak";
$lang['placeholder_name'] = "Nama";
$lang['placeholder_phone'] = "Telepon Seluler";
$lang['placeholder_email'] = "Alamat Email";
$lang['label_item_description'] = "Deskripsi barang";
$lang['placeholder_item'] = "Deskripsi barang anda";
$lang['label_length'] = "Panjang (cm)";
$lang['placeholder_length'] = "Panjang (Max 50 cm)";
$lang['label_width'] = "Lebar (cm)";
$lang['placeholder_width'] = "Lebar (Max 50 cm)";
$lang['label_height'] = "Tinggi (cm)";
$lang['placeholder_height'] = "Tinggi (Max 50 cm)";
$lang['label_weight'] = "Berat (kg)";
$lang['placeholder_weight'] = "Berat (Max 25 kg)";
$lang['label_note'] = "Catatan";
$lang['placeholder_note'] = "Tulis instruksi spesifik mengenai barang anda";
$lang['label_please_note_that'] = "Tolong dicatat";
$lang['label_note_1'] = "Pastikan bahwa kamu mengisi detail kontak untuk penerima, agar kami bisa mengkonfirmasi pengiriman";
$lang['label_note_2'] = "Pastikan anda mencatat detail informasi seperti nama tower, dan nomer ruangan apabila mengirim ke apartemen";
$lang['label_tariff'] = "Tarif";
$lang['button_add_to_cart'] = "TAMBAHKAN KE KURIR";

$lang['label_track_your_shipment'] = "Lacak pengiriman anda";

//form contact us
$lang['label_contact_us'] = "KONTAK KAMI";
$lang['label_sub_contact_us'] = "Pertanyaan? Saran? Tinggalkan pesan kepada kami, dan kami akan memberitahu kamu secepatnya! <br/> atau kamu dapat menghubungi kami di <a href='tel:+6282221001009'>08 222 100 100 9</a> atau <a href='mailto:info@parselday.com'>info@parselday.com</a>";
$lang['placeholder_contact_name'] = "Nama anda *";
$lang['placeholder_contact_email'] = "Email anda *";
$lang['placeholder_contact_phone'] = "Telpon anda *";
$lang['placeholder_contact_message'] = "Pesan anda *";
$lang['button_send_contact'] = "KIRIM PESAN";

//form about us

$lang['about_us_page_title'] = "TENTANG KAMI";
$lang['about_us_page_sub_title_1'] = "<strong>PT. ParselDay Maju Internasional</strong> adalah bagian dari IMS Group yang menyediakan pengiriman dalam kota di hari yang sama untuk barang atau dokumen baik untuk perorangan maupun perusahaan.";
$lang['about_us_page_sub_title_2'] = "Dalam hal ini, ParselDay membuka lahan pekerjaan baru untuk mereka yang ingin memiliki penghasilan tambahan. Peluang kerja sebagai mitra terbuka untuk freelancer, pelajar, pekerja, dan mereka yang telah berusia 18 tahun dan telah melewati proses seleksi latar belakang.";

$lang['about_us_heading_milestone'] = "MILESTONE";
$lang['about_us_sub_heading_milestone'] = "Sejarah singkat tentang ParselDay.";

$lang['about_us_timeline_heading_march'] = "MARET 2015";
$lang['about_us_timeline_sub_heading_march'] = "ParselDay hadir";
$lang['about_us_timeline_body_march'] = "Tahap implementasi dari ide menjadi wujud nyata.";

$lang['about_us_timeline_heading_april'] = "APRIL 2015";
$lang['about_us_timeline_sub_heading_april'] = "Pembukaan Resmi";
$lang['about_us_timeline_body_april'] = "Merupakan tahap pra peluncuran. Website dapat digunakan sesuai dengan fitur yang di tawarkan";

$lang['about_us_timeline_heading_may'] = "MEI 2015";
$lang['about_us_timeline_sub_heading_may'] = "Penambahan 100 Pengendara";
$lang['about_us_timeline_body_may'] = "Dalam tahap ini, kami menambahkan lebih banyak partner kerja untuk memaksimalkan pelayanan.";

$lang['about_us_join_our_story_1'] = "Jadilah";
$lang['about_us_join_our_story_2'] = "Bagian dari";
$lang['about_us_join_our_story_3'] = "Cerita Kami!";

//form view detail shipment

$lang['complete_title'] = "KERANJANG PENGIRIMAN";

$lang['complete_sub_title_1'] = "Setelah anda membuat pengiriman maka detil pengiriman akan muncul tabel berikut ini. Tabel tersebut merupakan ringkasan pengiriman yang anda buat disertai dengan estimasi harga. Anda perlu untuk melakukan pengecekan terhadap data ini untuk memastikan keakurasiannya. Anda juga dapat menghapus daftar yang ditambahkan ke keranjang dengan menekan tombol keranjang sampah  di sisi kanan.";
$lang['complete_sub_title_2'] = "Jika anda ingin menambahkan pengiriman baru dengan asal atau destinasi yang berbeda, tekan tombol “Tambah Pengiriman Lagi”.";

$lang['complete_origin'] = "Asal";
$lang['complete_destination'] = "Tujuan";
$lang['complete_items'] = "Barang";
$lang['complete_tariff'] = "Tarif";

$lang['complete_button_create_more_shipment'] = "TAMBAH PENGIRIMAN";
$lang['complete_button_checkout'] = "SELESAI";

//form confirm cart
$lang['confirm_cart_title'] = "DETIL PENAGIHAN";
$lang['confirm_cart_sub_title'] = "Anda harus mengisi tabel berikut ini secara akurat. Kami tidak akan menggunakan data yang diberikan untuk segala tujuan lain kecuali pengecekan pembayaran. Data anda akan disimpan dalam database kami dengan aman.";

$lang['confirm_cart_placeholder_first_name'] = "Nama Depan";
$lang['confirm_cart_placeholder_last_name'] = "Nama Belakang";
$lang['confirm_cart_placeholder_phone'] = "Nomer Handphone Anda";
$lang['confirm_cart_placeholder_email'] = "Email Anda";
$lang['confirm_cart_placeholder_address'] = "Alamat Anda";
$lang['confirm_cart_placeholder_city'] = "Kota";
$lang['confirm_cart_placeholder_province'] = "Provinsi";
$lang['confirm_cart_placeholder_country'] = "Negara";
$lang['confirm_cart_placeholder_zip'] = "Kode Pos";
$lang['confirm_cart_placeholder_username'] = "Username";
$lang['confirm_cart_placeholder_password'] = "Kata Sandi";
$lang['confirm_cart_placeholder_retype_password'] = "Konfirmasi Kata Sandi";

$lang['confirm_cart_checkbox_create_an_account'] = "Buat akun";

$lang['confirm_cart_heading'] = "DETIL PENGIRIMAN";

$lang['confirm_cart_label_origin'] = "Asal";
$lang['confirm_cart_label_destination'] = "Tujuan";
$lang['confirm_cart_label_items'] = "Barang";
$lang['confirm_cart_label_tariff'] = "Tarif";

$lang['confirm_cart_button_confirm_order'] = "KONFIRMASI PEMBAYARAN";

$lang['confirm_cart_label_payment_method'] = "Metode Pembayaran";

$lang['confirm_cart_dropdown_payment_method_1'] = "Transfer Bank";
$lang['confirm_cart_dropdown_payment_method_2'] = "PIN Perusahaan";
$lang['confirm_cart_dropdown_payment_method_3'] = "Kartu Kredit";

$lang['confirm_cart_payment_method_info_1'] = "Harap lakukan pembayaran transfer pada rekening dibawah ini <strong>TIDAK LEBIH DARI 45 MENIT</strong>";
$lang['confirm_cart_payment_method_info_2'] = "Pemegang Rekening";
$lang['confirm_cart_payment_method_info_3'] = "Nomor Rekening";
$lang['confirm_cart_payment_method_info_4'] = "Nama Bank";

$lang['confirm_cart_payment_method_info_5'] = "Mohon mencantumkan nomor pengiriman anda pada berita transfer. Pesanan anda akan di batalkan <strong>TANPA</strong> konfirmasi jika pembayaran <strong>TIDAK</strong> dilakukan dalam waktu 45 menit.";

//page login & register

$lang['login_heading'] = "MASUK ANGGOTA";
$lang['login_sub_heading'] = "Harap masukkan detil informasi dibawah ini.";

$lang['login_label_username'] = "Username / Email address";
$lang['login_placeholder_username'] = "Masukan Username / Email";
$lang['login_label_passwprd'] = "Kata sandi";
$lang['login_placeholder_password'] = "Masukan kata sandi";
$lang['login_checkbox_remember_me'] = "Ingatkan saya";
$lang['login_button_login'] = "MASUK";

$lang['register_heading'] = "BELUM MEMILIKI AKUN?";
$lang['register_sub_heading_1'] = "Berikut ini beberapa kemudahan dengan mendaftarkan diri anda atau perusahaan ke ParselDay.";

$lang['register_info_1'] = "Alamat yang pernah dipakai akan otomatis tersimpan.";
$lang['register_info_2'] = "Anda hanya butuh untuk memasukkan data 1 kali apabila pengiriman ke alamat yang sama diulang kembali.";
$lang['register_info_3'] = "Menjadi orang pertama yang mengetahui informasi terbaru.";
$lang['register_info_4'] = "Semua data pengiriman tersimpan";
$lang['register_info_5'] = "dan masi banyak lagi.";

$lang['register_sub_heading_2'] = "jadi, tunggu apa lagi? Segera isi tabel berikut untuk daftar!";

$lang['register_label_name'] = "Nama";
$lang['register_placeholder_name'] = "Masukan nama / perusahaan";
$lang['register_label_username'] = "Username";
$lang['register_placeholder_username'] = "Masukan username";
$lang['register_label_email'] = "Email";
$lang['register_placeholder_email'] = "Masukan alamat email";
$lang['register_label_phone'] = "Telepon";
$lang['register_placeholder_phone'] = "Masukan nomer telepon";
$lang['register_label_password'] = "Kata sandi";
$lang['register_placeholder_password'] = "Masukan kata sandi";
$lang['register_button_register'] = "Daftar sekarang";

//page thank you konfirmasi order
 
$lang['ty_order_title'] = "Terima Kasih atas Pesanan Anda";
$lang['ty_order_sub_title_1'] = "Pesanan anda telah kami terima.";
$lang['ty_order_sub_title_2'] = "Harap periksa Pemesanan Pengiriman  anda dibawah ini sebelum melakukan pembayaran";

$lang['ty_order_label_summary_order'] = "RINGKASAN PEMESANAN";
$lang['ty_order_table_shipment'] = "Pengiriman #";
$lang['ty_order_table_origin'] = "Asal";
$lang['ty_order_table_destination'] = "Tujuan";
$lang['ty_order_table_items'] = "Barang";
$lang['ty_order_table_tariff'] = "Tarif";
$lang['ty_order_table_amount_total'] = "TOTAL PEMBAYARAN";

$lang['ty_order_payment_info_1'] = "Harap lakukan pembayaran transfer pada rekening dibawah ini  <strong>TIDAK LEBIH DARI 45 MENIT</strong>:<br />
                Pemegang Rekening: <strong>PT. Parsel Ekspress Indonesia</strong><br />
                Nomer Rekening: <strong>450 897 8888</strong><br />
                Nama Bank: <strong>BCA Cabang Bidakara Jakarta</strong>";

$lang['ty_order_payment_info_2'] = "Mohon mencantumkan nomor pengiriman anda pada berita transfer.</br> 
                Pesanan anda akan di batalkan TANPA konfirmasi jika pembayaran TIDAK dilakukan dalam waktu 45 menit.</br>
                Harap <strong>LAKUKAN KONFIRMASI</strong> setelah melakukan pembayaran dengan klik ";
$lang['ty_order_payment_info_3'] = "DISINI";

$lang['ty_order_payment_info_4'] = "Kami juga telah mengirimkan ringkasan pemesanan ke ";
$lang['ty_order_payment_info_5'] = "Harap periksa folder inbox atau spam anda. Jika anda memiliki pertanyaan silahkan hubungi kami di info@parselday.com";

//page confirm payment

$lang['confirm_payment_title'] = "KONFIRMASI PEMBAYARAN";
$lang['confirm_payment_sub_title'] = "Anda harus mengisi tabel berikut ini secara akurat. Kami tidak akan menggunakan data yang diberikan untuk segala tujuan lain kecuali pengecekan pembayaran. Data anda akan disimpan dalam database kami dengan aman. Anda juga perlu meng-upload bukti transaksi pembayaran dengan menekan tombol “Pilih File” pada bagian Slip Pembayaran. Apabila anda sudah mengisi data tersebut dengan lengkap, silahkan menekan tombol “Kirim Konfirmasi” pada bagian sisi bawah. Jika anda telah melewati tahap tersebut, silahkan tunggu notifikasi yang akan dikirimkan ke email untuk mengetahui status pengiriman.";

$lang['confirm_payment_label_account_holder'] = "Pemegang Rekening";
$lang['confirm_payment_placeholder_account_holder'] = "Nama di rekening bank";
$lang['confirm_payment_label_bank'] = "Bank";
$lang['confirm_payment_placeholder_bank'] = "Nama bank";
$lang['confirm_payment_label_account_number'] = "Nomer Rekening";
$lang['confirm_payment_placeholder_account_number'] = "Nomor rekening";
$lang['confirm_payment_label_paid_amount'] = "Jumlah Pembayaran";
$lang['confirm_payment_placeholder_paid_amount'] = "Jumlah pembayaran";
$lang['confirm_payment_label_payment_slip'] = "Bukti Pembayaran";
$lang['confirm_payment_label_info_upload'] = "Harap unduh hanya format JPG/PNG/PDF.";

$lang['confirm_payment_button_send_confirmation'] = "KIRIM KONFIRMASI";

$lang['confirm_payment_table_shipment'] = "Pengiriman #";
$lang['confirm_payment_table_item'] = "Barang";
$lang['confirm_payment_table_route'] = "Rute";
$lang['confirm_payment_table_jumlah'] = "Jumlah";
$lang['confirm_payment_table_tarif'] = "Tarif";
$lang['confirm_payment_table_amount_total'] = "TOTAL PEMBAYARAN";




